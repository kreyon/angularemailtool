﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AngularEmailTool.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AngularEmailTool
{
    [Route("api/[controller]")]
    public class DetailsController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public JsonResult Get()
        {
            int sent = 0;
            int draft = 0;
            int outbox = 0;
            int starred = 0;
            int trash = 0;

            int MasterID = 0;

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;

                       // String Decryptpassword = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

                        //smtphost.Value = record.SmtpServer;
                       // smtpport.Value = record.SmtpPort;
                       // password.Value = Decryptpassword;
                      //  username.Value = record.Username;
                      //  accesswithserver.Value = record.Access;
                      //  serverhost.Value = record.Host;
                      //  serverport.Value = record.Port;
                     //   signa.Value = record.Signature;
                      //  isreply.Value = record.IsReply.ToString();
                      //  isreplyall.Value = record.IsReplyAll.ToString();
                     //   iscompose.Value = record.IsCompose.ToString();
                     //   isforward.Value = record.IsForward.ToString();

                        //trash = record.TrashMailsTables.Where(a => a.IsDeleted == false).Count();
                        //sent = record.SendedMails.Count();
                        //draft = record.DraftEmailTables.Count(); ;
                        //outbox = record.OutBoxMails.Count();
                        //starred = record.StarredEmailTables.Count();

                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET

                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                       // HttpContext.Current.Session["ConfigId"] = record.Id;

                        //String Decryptpassword = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

                        //smtphost.Value = record.SmtpServer;
                        //smtpport.Value = record.SmtpPort;
                        //password.Value = Decryptpassword;
                        //username.Value = record.Username;
                        //accesswithserver.Value = record.Access;
                        //serverhost.Value = record.Host;
                        //serverport.Value = record.Port;
                        //signa.Value = record.Signature;
                        //isreply.Value = record.IsReply.ToString();
                        //isreplyall.Value = record.IsReplyAll.ToString();
                        //iscompose.Value = record.IsCompose.ToString();
                        //isforward.Value = record.IsForward.ToString();

                        //trash = record.TrashMailsTables.Count();
                        //sent = record.SendedMails.Count();
                        //draft = record.DraftEmailTables.Count(); ;
                        //outbox = record.OutBoxMails.Count();
                        //starred = record.StarredEmailTables.Count();
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
            }

            var mid = "{masterid:" + MasterID + "}";
            //trashcount.InnerHtml = trash.ToString();
            //sendcount.InnerHtml = sent.ToString();
            //draftcount.InnerHtml = draft.ToString();
            //outboxcount.InnerHtml = outbox.ToString();
            //starredcount.InnerHtml = starredcount.ToString();

            //starredcount.InnerHtml = starred.ToString();

            return new JsonResult(mid);

        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
