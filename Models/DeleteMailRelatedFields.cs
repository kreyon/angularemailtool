﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class DeleteMailRelatedFields
    {
        public int MasterID { get; set; }
        public int DeleteID { get; set; }
    }
}
