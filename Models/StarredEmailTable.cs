﻿using System;
using System.Collections.Generic;

namespace AngularEmailTool.Models
{
    public partial class StarredEmailTable
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string EmailId { get; set; }
        public bool? IsInbox { get; set; }
        public string XmlPath { get; set; }
        public string From1 { get; set; }

        public virtual EmailConfig User { get; set; }
    }
}
