﻿using System;
using System.Collections.Generic;

namespace AngularEmailTool.Models
{
    public partial class SendedMail
    {
        public SendedMail()
        {
            StarredEmail = new HashSet<StarredEmail>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string SendMailPath { get; set; }

        public virtual EmailConfig User { get; set; }
        public virtual ICollection<StarredEmail> StarredEmail { get; set; }
    }
}
