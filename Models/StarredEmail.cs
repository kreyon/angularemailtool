﻿using System;
using System.Collections.Generic;

namespace AngularEmailTool.Models
{
    public partial class StarredEmail
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? SentId { get; set; }
        public int? DraftId { get; set; }
        public int? OutboxId { get; set; }
        public int? TrashId { get; set; }
        public string XmlPath { get; set; }
        public DateTime StarredDate { get; set; }
        public string InboxId { get; set; }

        public virtual OutBoxMail Outbox { get; set; }
        public virtual SendedMail Sent { get; set; }
        public virtual TrashMailsTable Trash { get; set; }
        public virtual EmailConfig User { get; set; }
    }
}
