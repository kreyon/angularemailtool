﻿using System;
using System.Collections.Generic;

namespace AngularEmailTool.Models
{
    public partial class TrashMailsTable
    {
        public TrashMailsTable()
        {
            StarredEmail = new HashSet<StarredEmail>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Xmlpath { get; set; }
        public string EmailType { get; set; }
        public string InboxMailThreadId { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual EmailConfig User { get; set; }
        public virtual ICollection<StarredEmail> StarredEmail { get; set; }
    }
}
