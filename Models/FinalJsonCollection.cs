﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class FinalJsonCollection
    {
        public IEnumerable<MailData> _MailData { get; set; }
        public int TotalPages { get; set; }
    }
}
