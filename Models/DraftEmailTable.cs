﻿using System;
using System.Collections.Generic;

namespace AngularEmailTool.Models
{
    public partial class DraftEmailTable
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Draftpath { get; set; }

        public virtual EmailConfig User { get; set; }
    }
}
