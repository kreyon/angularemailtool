﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class MailData
    {
        public string Date { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Id { get; set; }
        public IEnumerable<AttachmentRecord> _AttachmentRecord { get; set; }
       // public string Attachment_Name { get; set; }

        public string[] Attachment_Name{ get; set; }
        public string Reply_All_Mails { get; set; }
        public string Depart_Name { get; set; }
        public string Display_Name { get; set; }
        public int skipnum { get; set; }
        public string searchval { get; set; }
        public string Attachment{ get; set; }
        public int starid { get; set; }

     

    }

    public class AttachmentRecord
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
