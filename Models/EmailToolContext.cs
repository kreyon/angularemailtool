﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AngularEmailTool.Models
{
    public partial class EmailToolContext : DbContext
    {
        public EmailToolContext()
        {
        }

        public EmailToolContext(DbContextOptions<EmailToolContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DraftEmailTable> DraftEmailTable { get; set; }
        public virtual DbSet<EmailConfig> EmailConfig { get; set; }
        public virtual DbSet<OutBoxMail> OutBoxMail { get; set; }
        public virtual DbSet<SendedMail> SendedMail { get; set; }
        public virtual DbSet<StarredEmail> StarredEmail { get; set; }
        public virtual DbSet<StarredEmailTable> StarredEmailTable { get; set; }
        public virtual DbSet<TrashMailsTable> TrashMailsTable { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=KREYON10-PC\\KREYON10;Initial Catalog=EmailTool;User ID=sa;Password=kreyonsys");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-preview3-35497");

            modelBuilder.Entity<DraftEmailTable>(entity =>
            {
                entity.Property(e => e.Draftpath)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DraftEmailTable)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_DraftEmailTable_EmailConfig");
            });

            modelBuilder.Entity<EmailConfig>(entity =>
            {
                entity.Property(e => e.Access)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.Port)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SmtpPort).HasMaxLength(50);

                entity.Property(e => e.SmtpServer)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<OutBoxMail>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.OutBoxMail)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_OutBoxMail_EmailConfig");
            });

            modelBuilder.Entity<SendedMail>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.SendedMail)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_SendedMail_EmailConfig");
            });

            modelBuilder.Entity<StarredEmail>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DraftId).HasColumnName("DraftID");

                entity.Property(e => e.InboxId)
                    .HasColumnName("InboxID")
                    .HasMaxLength(250);

                entity.Property(e => e.OutboxId).HasColumnName("OutboxID");

                entity.Property(e => e.SentId).HasColumnName("SentID");

                entity.Property(e => e.StarredDate).HasColumnType("datetime");

                entity.Property(e => e.TrashId).HasColumnName("TrashID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.XmlPath).HasMaxLength(150);

                entity.HasOne(d => d.Outbox)
                    .WithMany(p => p.StarredEmail)
                    .HasForeignKey(d => d.OutboxId)
                    .HasConstraintName("FK_StarredEmail_OutBoxMail");

                entity.HasOne(d => d.Sent)
                    .WithMany(p => p.StarredEmail)
                    .HasForeignKey(d => d.SentId)
                    .HasConstraintName("FK_StarredEmail_SendedMail");

                entity.HasOne(d => d.Trash)
                    .WithMany(p => p.StarredEmail)
                    .HasForeignKey(d => d.TrashId)
                    .HasConstraintName("FK_StarredEmail_TrashMailsTable");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.StarredEmail)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_StarredEmail_EmailConfig");
            });

            modelBuilder.Entity<StarredEmailTable>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmailId).HasMaxLength(200);

                entity.Property(e => e.From1).HasMaxLength(50);

                entity.Property(e => e.XmlPath).HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.StarredEmailTable)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StarredEmailTable_EmailConfig");
            });

            modelBuilder.Entity<TrashMailsTable>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmailType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.InboxMailThreadId)
                    .HasColumnName("InboxMailThreadID")
                    .HasMaxLength(200);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Xmlpath)
                    .HasColumnName("XMLPath")
                    .HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TrashMailsTable)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_TrashMailsTable_EmailConfig");
            });
        }
    }
}
