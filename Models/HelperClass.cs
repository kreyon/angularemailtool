﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class HelperClass
    {
        public static void createNode(System.Xml.XmlTextWriter writer, string StartElementName, string ElementValue)
        {
            writer.WriteStartElement(StartElementName);
            writer.WriteString(ElementValue);
            writer.WriteEndElement();
        }
    }
}
