﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class ConfigurationDetails
    {
        public int MasterID { get; set; }
        public string AccessWith { get; set; }
        public string SmtpHost { get; set; }
        public string SmtpPort { get; set; }
        public string IncomingHost { get; set; }
        public string IncomingPort { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Signature { get; set; }
        public Boolean? Reply { get; set; }
        public Boolean? ReplyAll { get; set; }
        public Boolean? Forward { get; set; }
        public Boolean? Compose { get; set; }
    }
}
