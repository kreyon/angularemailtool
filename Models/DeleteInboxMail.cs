﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class DeleteInboxMail
    {
        public int MasterID { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachment { get; set; }
        public string DeleteID { get; set; }
        public string ReplyAllEmail { get; set; }
        public string labeltxt { get; set; }
    }
}
