﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class EmailTabCounts
    {
        public int InboxCount { get; set; }
        public int SentCount { get; set; }
        public int DraftCount { get; set; }
        public int OutboxCount { get; set; }
        public int StarredCount { get; set; }
        public int TrashCount { get; set; }
    }
}
