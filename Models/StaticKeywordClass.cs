﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class StaticKeywordClass
    {
        public const string Inbox = "Inbox";
        public const string Sent = "Sent";
        public const string Draft = "Draft";
        public const string Outbox = "Outbox";
        public const string Starred = "Starred";
        public const string Trash = "Trash";
    }
   
}
