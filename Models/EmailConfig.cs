﻿using System;
using System.Collections.Generic;

namespace AngularEmailTool.Models
{
    public partial class EmailConfig
    {
        public EmailConfig()
        {
            DraftEmailTable = new HashSet<DraftEmailTable>();
            OutBoxMail = new HashSet<OutBoxMail>();
            SendedMail = new HashSet<SendedMail>();
            StarredEmail = new HashSet<StarredEmail>();
            StarredEmailTable = new HashSet<StarredEmailTable>();
            TrashMailsTable = new HashSet<TrashMailsTable>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Access { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SmtpServer { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? IsDeleted { get; set; }
        public string SmtpPort { get; set; }
        public string Signature { get; set; }
        public bool? IsReply { get; set; }
        public bool? IsReplyAll { get; set; }
        public bool? IsCompose { get; set; }
        public bool? IsForward { get; set; }

        public virtual ICollection<DraftEmailTable> DraftEmailTable { get; set; }
        public virtual ICollection<OutBoxMail> OutBoxMail { get; set; }
        public virtual ICollection<SendedMail> SendedMail { get; set; }
        public virtual ICollection<StarredEmail> StarredEmail { get; set; }
        public virtual ICollection<StarredEmailTable> StarredEmailTable { get; set; }
        public virtual ICollection<TrashMailsTable> TrashMailsTable { get; set; }
    }
}
