﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularEmailTool.Models
{
    public class ReturnResponseClass
    {
        public string type { get; set; }
        public string msg { get; set; }
        public int anyid { get; set; }
        public string anystring { get; set; }
    }
}
