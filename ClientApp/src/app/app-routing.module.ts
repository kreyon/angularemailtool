import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InboxmailComponent } from './inboxmail/inboxmail.component';
import { SendmailComponent } from './sendmail/sendmail.component';
import { ComposemailComponent } from './composemail/composemail.component';
import { DraftmailComponent } from './draftmail/draftmail.component';
import { TrashmailComponent } from './trashmail/trashmail.component';
import { OutboxmailComponent } from './outboxmail/outboxmail.component';
import { StarredmailComponent } from './starredmail/starredmail.component';

const routes: Routes = [
  {
    path: '',
    component: InboxmailComponent
  },
  {
    path: 'inbox',
    component: InboxmailComponent   
  },
  {
    path: 'sendmail',
    component: SendmailComponent
  }
  ,
  {
    path: 'composemail',
    component: ComposemailComponent
  }
  ,
  {
    path: 'draftmail',
    component: DraftmailComponent
  }
  ,
  {
    path: 'trashmail',
    component: TrashmailComponent
  }
  ,
  {
    path: 'outboxmail',
    component: OutboxmailComponent
  }
  ,
  {
    path: 'starredmail',
    component: StarredmailComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
