import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DataService } from '../data.service';

const endpoint = 'https://localhost:44365/api/values';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
export class Custom {
  icon: string;
  filename: string;
  newfilename: string;
}


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  signupdisplay = 'none';
  MasterID = 0;
  showsignature = false;
  theCheckbox = false;
  SignatureBody = "";
  ProgressBarStrip = 'none';
  completionPercent = 0;

  selectedFile: File;
  progress: number;

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    // uploadUrl: endpoint + '/SignatureFileUpload', 
    translate: 'no'
    //customClasses: [
    //  {
    //    name: "quote",
    //    class: "quote",
    //  },
    //  {
    //    name: 'redText',
    //    class: 'redText'
    //  },
    //  {
    //    name: "titleText",
    //    class: "titleText",
    //    tag: "h1",
    //  },
    //]
  }
  @Input() configureData = { AccessWith: '', SmtpHost: '', SmtpPort: '', IncomingHost: '', IncomingPort: '', EmailID: '', Password: '', Signature: '', Reply: false, ReplyAll: false, Forward: false, Compose: false };

  constructor(private _data: DataService, private http: HttpClient/*, private sanitizer: DomSanitizer*/, private _route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.GetDefaultConfigDetails();
  }

  // GET RECORD WITH INTIAL LOADING
  GetDefaultConfigDetails() {

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    this.http.get(endpoint + '/GetDefaultConfigDetails/' + _MasterID).subscribe(data => {

      this.MasterID = data[0].masterID;
      this.configureData.AccessWith = data[0].accessWith;
      this.configureData.SmtpHost = data[0].smtpHost;
      this.configureData.SmtpPort = data[0].smtpPort;
      this.configureData.IncomingHost = data[0].incomingHost;
      this.configureData.IncomingPort = data[0].incomingPort;
      this.configureData.EmailID = data[0].emailID;
      //this.configureData.Password = data[0].password;
      this.configureData.Signature = data[0].signature;
      this.configureData.Reply = data[0].reply;
      this.configureData.ReplyAll = data[0].replyAll;
      this.configureData.Compose = data[0].compose;
      this.configureData.Forward = data[0].forward;

      sessionStorage.setItem('currentUser', data[0].masterID);

      if (this.configureData.Signature != "" && this.configureData.Signature != "<br>") {
        this.showsignature = true;
      }

    });

  }

  /*** DROPDOWN CHANGE EVENT  *****/
  onAccessChange(deviceValue) {

    if (deviceValue == "select") {
      this.error_AccessWith = 'block';
    } else {
      this.error_AccessWith = 'none';
    }
  }

  error_AccessWith = 'none';
  error_SmtpHost = 'none';
  error_SmtpPort = 'none';
  error_IncomingHost = 'none';
  error_IncomingPort = 'none';
  error_EmailID = 'none';
  error_Password = 'none';


  /*** VALIDATION START *****/

  focusOutFunction() {
    this.validateFields();
  }

  validateEmail(emailField) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(emailField) == false) {
      return false;
    }

    return true;

  }

  validateFields() {

    var Result = true;

    var AccessWith = this.configureData.AccessWith;
    var SmtpHost = this.configureData.SmtpHost;
    var SmtpPort = this.configureData.SmtpPort;
    var IncomingHost = this.configureData.IncomingHost;
    var IncomingPort = this.configureData.IncomingPort;
    var EmailID = this.configureData.EmailID;
    var Password = this.configureData.Password;

    if (AccessWith == "") {
      this.error_AccessWith = 'block';
      Result = false;
    } else {
      this.error_AccessWith = 'none';
    }

    if (SmtpHost == "") {
      this.error_SmtpHost = 'block';
      Result = false;
    } else {
      this.error_SmtpHost = 'none';
    }

    if (SmtpPort == "") {
      this.error_SmtpPort = 'block';
      Result = false;
    } else {
      this.error_SmtpPort = 'none';
    }

    if (IncomingHost == "") {
      this.error_IncomingHost = 'block';
      Result = false;
    } else {
      this.error_IncomingHost = 'none';
    }

    if (IncomingPort == "") {
      this.error_IncomingPort = 'block';
      Result = false;
    } else {
      this.error_IncomingPort = 'none';
    }

    if (EmailID == "") {
      this.error_EmailID = 'block';
      Result = false;
    } else {
      Result = this.validateEmail(EmailID);
      if (!Result) {
        this.error_EmailID = 'block';
      } else {
        this.error_EmailID = 'none';
      }
    }

    if (Password == "") {
      this.error_Password = 'block';
      Result = false;
    } else {
      this.error_Password = 'none';
    }

    return Result;

  }
  /*** VALIDATION END *****/

  /*** START SAVE SUBMIT CLICK EVENT *****/
  CofigureSubmit() {

    var validateFields_fn = this.validateFields();

    if (!validateFields_fn) {
      return false;
    }

    this.SubmitCofigureDetails(this.configureData).subscribe((result) => {

      if (result.type == "success") {

        this.MasterID = result.anyid;
        sessionStorage.setItem('currentUser', this.MasterID.toString());

      } else {
        this.MasterID = 0;
        sessionStorage.setItem('currentUser', this.MasterID.toString());
      }

      this.closesignup();

      window.location.reload();

    }, (err) => {
      console.log(err);
    });

  }

  SubmitCofigureDetails(configureData): Observable<any> {
    return this.http.post<any>(endpoint + "/PostConfigurationDetails/", JSON.stringify(configureData), httpOptions).pipe(

    );
  }
  /*** END SAVE SUBMIT CLICK EVENT *****/

  /*** START RESET BUTTON CLICK EVENT *** */
  ResetSubmit(MasterID) {

    var _MasterID = 0;

    try {
      _MasterID = Number(sessionStorage.getItem('currentUser'));
    } catch (e) {
      _MasterID = 0;
    }

    this.http.get<any>(endpoint + '/GetResetSubmit/' + _MasterID).subscribe((result) => {

      if (result.type == "success") {

        this._data.MessageBoxShowHide({
          'Msg': result.msg, 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
          }
        });

      } else {

        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Error });
      }
    });

  }
  /*** END RESET BUTTON CLICK EVENT *** */

  openModal() {
    this.signupdisplay = 'block';
  }


  closesignup() {
    this.signupdisplay = 'none';
  }

  DisplaySignature(e) {
    this.showsignature = e.target.checked;
  }


  onSignatureChanged(event) {

    const fileGet = event.target.files[0];

    //console.log('size', fileGet.size);

    //console.log('type', fileGet.type);

    var sizefile = fileGet.size;
    // temp = temp + sizefile;

    var threeMb = 3 * 1024 * 1024;
    // var fivemb = 6 * 1024 * 1024;
    if (sizefile > threeMb) {

      //var cursize = parseFloat((event.files[0].size / 1048576)).toFixed(2);

      var cursize = (sizefile / 1048576).toFixed(2);

      this._data.MessageBoxShowHide({
        'Msg': "Your file size " + cursize + " MB. File size should not exceed 3 MB", 'Type': this._data.msg_Info
      });

      // $(event).val('');
      return false;
    }
    //if (temp > fivemb) {

    //  Message({
    //    "Msg": "Yor total file size is greater than 6 MB. File size should not exceed 6MB", "Type": msg_Info
    //  });

    //  $(this).val('');
    //  temp = temp - sizefile;
    //  return false;
    //}

    var validFiles = ["jpg", "jpeg", "png"];

    var source = fileGet.name;
    var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
    for (var i = 0; i < validFiles.length; i++) {
      if (validFiles[i] == ext) {
        break;
      }
    }
    if (i >= validFiles.length) {


      this._data.MessageBoxShowHide({ 'Msg': "Only following  file extensions are allowed : " + validFiles.join(","), 'Type': this._data.msg_Info });

      //this.ComposeMailData.Attachment = "";
      //$(event).val('');
      //$('#filetxt').val('');
      return false;
    }

    const file = event.target.files[0];

    //this.ComposeMailData.Attachment = event.target.files[0];

    this.selectedFile = event.target.files[0];

    // this.http is the injected HttpClient
    const uploadData = new FormData();

    uploadData.append('myFile', this.selectedFile, this.selectedFile.name);

    this.ProgressBarStrip = 'block';

    this.http.post(endpoint + '/SignatureFileUpload', uploadData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
        // debugger;
        if (event.type === HttpEventType.UploadProgress) {
          this.completionPercent = Math.round(100 * event.loaded / event.total);
        }
        else if (event.type === HttpEventType.Response) {
          this.ProgressBarStrip = 'none';
          this.completionPercent = 0;

          try {

            // if (event.body.action == "success") {

            var newfilename_ = "";

            for (var i in event.body) {
              newfilename_ = event.body[i];

            }

            this.configureData.Signature = this.configureData.Signature + "<br>" + "<img src='/SignatureAttacements/" + newfilename_ + "' />";

          }
          catch (e) { }
        }

      });
  }

  mob_menu_btn() {

    if ($('#slide-menu').hasClass('left-menu-open')) {
      $('#slide-menu').removeClass('left-menu-open');
      $('.mobilebg').fadeOut(300);
    }
    else {
      $('#slide-menu').addClass('left-menu-open');
      $('.mobilebg').fadeIn(300);
    }
  }

}
