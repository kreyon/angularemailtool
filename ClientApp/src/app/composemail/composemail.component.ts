import { Component, OnInit, NgModule, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DataService } from '../data.service';

export class Custom {
  icon: string;
  filename: string;
  newfilename: string;
}

const endpoint = 'https://localhost:44365/api/values';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-composemail',
  templateUrl: './composemail.component.html',
  styleUrls: ['./composemail.component.css']
})

export class ComposemailComponent implements OnInit {


  name = 'Angular 6';
  htmlContent = '';
  isreply: boolean;
  isreplyall: boolean;
  iscompose: boolean;
  isforward: boolean;
  signaturebody: string;

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    // placeholder: 'Enter text here...',
    translate: 'no'
    //customClasses: [
    //  {
    //    name: "quote",
    //    class: "quote",
    //  },
    //  {
    //    name: 'redText',
    //    class: 'redText'
    //  },
    //  {
    //    name: "titleText",
    //    class: "titleText",
    //    tag: "h1",
    //  },
    //]
  }

  @Input() ComposeMailData = { MasterID: 0, To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '' };
  @Input() configureData = { AccessWith: '', SmtpHost: '', SmtpPort: '', IncomingHost: '', IncomingPort: '', EmailID: '', Password: '', Signature: '', Reply: false, ReplyAll: false, Forward: false, Compose: false };
  referencename$: Object;
  transfermaildatas: any;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router, private _data: DataService) { }
  //constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    // 0.
    // THIS FUNCTION IS ONLY USING FOR MOBILE SIDE MENU CASE
    this._data.hideSidemenuifOpen();

    // 1.
    this.route.queryParams.subscribe(params => {
      this.referencename$ = params['name'];
    });

    // 2.
    this._data.transfermaildata.subscribe(res => this.transfermaildatas = res);

    // 3.
    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    this.http.get(endpoint + '/GetDefaultConfigDetails/' + _MasterID).subscribe(data => {

      this.configureData.Signature = data[0].signature;
      this.configureData.Reply = data[0].reply;
      this.configureData.ReplyAll = data[0].replyAll;
      this.configureData.Compose = data[0].compose;
      this.configureData.Forward = data[0].forward;

      this.CallAnotherRestFuction();

    });


    //console.log(this.transfermaildatas);

    //console.log(this.referencename$);

  }

  CallAnotherRestFuction() {

    if (this.transfermaildatas.IsChanged) {

      if (this.referencename$ == "InboxReply") {
        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.From;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;

        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAddInboxSignature();

      } else if (this.referencename$ == "InboxReplyAll") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAllAddInboxSignature();

      } else if (this.referencename$ == "InboxForward") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ForwardAllAddInboxSignature();

      } else if (this.referencename$ == "SentReply") {
        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.To;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAddInboxSignature();

      } else if (this.referencename$ == "SentReplyAll") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAllAddInboxSignature();

      } else if (this.referencename$ == "SentForward") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ForwardAllAddInboxSignature();
      } else if (this.referencename$ == "DraftSent") {
        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.To;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
      } else if (this.referencename$ == "TrashReply") {
        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.To;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAddInboxSignature();

      } else if (this.referencename$ == "TrashReplyAll") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAllAddInboxSignature();

      } else if (this.referencename$ == "TrashForward") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ForwardAllAddInboxSignature();
      } else if (this.referencename$ == "StarredReply") {
        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.To;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAddInboxSignature();

      } else if (this.referencename$ == "StarredReplyAll") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ReplyAllAddInboxSignature();

      } else if (this.referencename$ == "StarredForward") {

        this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
        //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
        this.ComposeMailData.Cc = this.transfermaildatas.Cc;
        this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
        this.ComposeMailData.Subject = this.transfermaildatas.Subject;
        this.ComposeMailData.Body = this.transfermaildatas.Body;
        //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;

        this.ForwardAllAddInboxSignature();
      }

      // IF CC VALUE COMES THEN BOX WILL BE OPEN
      if (this.transfermaildatas.Cc != "")
        this.CcButtonClick();

      // IF BCC VALUE COMES THEN BOX WILL BE OPEN
      if (this.transfermaildatas.Bcc != "")
        this.BccButtonClick();

    } else {

      if (this.configureData.Compose) {
        this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
      }
    }
  }

  ReplyAddInboxSignature() {
    if (this.configureData.Reply) {
      this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
    }
  }

  ReplyAllAddInboxSignature() {
    if (this.configureData.ReplyAll) {
      this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
    }
  }

  ForwardAllAddInboxSignature() {
    if (this.configureData.Forward) {
      this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
    }
  }

  CcInputBox = 'none';
  BccInputBox = 'none';
  ProgressBarStrip = 'none';
  completionPercent = 0;

  CcButtonClick() {
    this.CcInputBox = 'block';
  }

  BccButtonClick() {
    this.BccInputBox = 'block';
  }

  selectedFile: File;
  progress: number;
  message: string;
  //temp : 0;
  AttachedDiv = "";
  empList: Array<Custom> = [];

  onFileChanged(event) {

    const fileGet = event.target.files[0];

    console.log('size', fileGet.size);

    console.log('type', fileGet.type);

    var sizefile = fileGet.size;
    // temp = temp + sizefile;

    var threeMb = 3 * 1024 * 1024;
    // var fivemb = 6 * 1024 * 1024;
    if (sizefile > threeMb) {

      //var cursize = parseFloat((event.files[0].size / 1048576)).toFixed(2);

      var cursize = (sizefile / 1048576).toFixed(2);

      this._data.MessageBoxShowHide({
        'Msg': "Your file size " + cursize + " MB. File size should not exceed 3 MB", 'Type': this._data.msg_Info
      });

      // $(event).val('');
      return false;
    }
    //if (temp > fivemb) {

    //  Message({
    //    "Msg": "Yor total file size is greater than 6 MB. File size should not exceed 6MB", "Type": msg_Info
    //  });

    //  $(this).val('');
    //  temp = temp - sizefile;
    //  return false;
    //}

    var validFiles = ["jpg", "jpeg", "png", "doc", "pdf", "docx", "txt", "xlsm", "xltx", "xlsx", "xltm", "xls", "xlt", "ppt", "PDF", "pptx"];

    var source = fileGet.name;
    var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
    for (var i = 0; i < validFiles.length; i++) {
      if (validFiles[i] == ext) {
        break;
      }
    }
    if (i >= validFiles.length) {

      this._data.MessageBoxShowHide({
        'Msg': "Only following  file extensions are allowed : " + validFiles.join(","), 'Type': this._data.msg_Info
      });

      //this.ComposeMailData.Attachment = "";
      //$(event).val('');
      //$('#filetxt').val('');
      return false;
    }

    const file = event.target.files[0];

    //this.ComposeMailData.Attachment = event.target.files[0];

    this.selectedFile = event.target.files[0];

    // this.http is the injected HttpClient
    const uploadData = new FormData();

    uploadData.append('myFile', this.selectedFile, this.selectedFile.name);

    this.ProgressBarStrip = 'block';

    this.http.post<any>(endpoint + '/ComposeFileUpload', uploadData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
        // debugger;
        if (event.type === HttpEventType.UploadProgress) {
          this.completionPercent = Math.round(100 * event.loaded / event.total);
        }
        else if (event.type === HttpEventType.Response) {
          this.ProgressBarStrip = 'none';
          this.completionPercent = 0;

          try {

            if (event.body.type == "success") {

              let customObj = new Custom();
              customObj.filename = source;
              customObj.newfilename = event.body.anystring;

              if (ext == "doc" || ext == "docx") {
                customObj.icon = "word-icon.png";
              }
              else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {
                customObj.icon = "excel-icon.png";
              }
              else if (ext == "jpg" || ext == "jpeg" || ext == "png") {
                customObj.icon = "img-large-icon.png";
              }
              else if (ext == "pdf" || ext == "PDF") {
                customObj.icon = "pdf-large-icon.png";
              }
              else if (ext == "txt") {
                customObj.icon = "text-large-icon.png";
              }
              else if (ext == "pptx" || ext == "ppt") {
                customObj.icon = "PowerPoint-icon.png";
              }

              this.empList.push(customObj);

            } else {

              // IF FOUND ANY ERROR

              this._data.MessageBoxShowHide({
                'Msg': event.body.msg, 'Type': this._data.msg_Error
              });
            }
          } catch (e) { }
        }

      });
  }

  delpic(fileObj) {

    var file = fileObj.newfilename;

    this.http.get<any>(endpoint + '/GetDeleteAttachmentSubmit/' + file).subscribe((result) => {

      if (result.type == "success") {

        var tempEmpList: Array<Custom> = [];

        for (var i in this.empList) {

          if (this.empList[i].newfilename == file) {
            continue;
          }

          let customObj = new Custom();
          customObj.icon = this.empList[i].icon;
          customObj.filename = this.empList[i].filename;
          customObj.newfilename = this.empList[i].newfilename;

          tempEmpList.push(customObj);

        }

        this.empList = tempEmpList;

        this._data.MessageBoxShowHide({
          'Msg': "Attachment removed successfully", 'Type': this._data.msg_Success
        });
      } else {

        this._data.MessageBoxShowHide({
          'Msg': result.msg, 'Type': this._data.msg_Error
        });

      }
    });
  }

  /********* START SENT COMPOSE MAIL FUNCTIONS **********/
  finalSendBtn() {

    var Result = true;

    var RecipientTo = this.ComposeMailData.To;
    var RecipientCc = this.ComposeMailData.Cc;
    var RecipientBcc = this.ComposeMailData.Bcc;

    if (RecipientTo == "" && RecipientCc == "" && RecipientBcc == "") {
      this._data.MessageBoxShowHide({
        'Msg': "Please specify at least one recipient.", 'Type': this._data.msg_Info
      });

      return false;
    }

    var _MasterID = 0;

    try {
      _MasterID = Number(sessionStorage.getItem('currentUser'));
    } catch (e) {
      _MasterID = 0;
    }

    var tempEmpList: Array<string> = [];

    for (var i in this.empList) {
      tempEmpList.push(this.empList[i].newfilename);
    }

    this.ComposeMailData.Attachment = tempEmpList.join(",");

    this.ComposeMailData.MasterID = _MasterID;

    $("#mainbox_submitloader").show();

    this.SubmitComposeMailDetails(this.ComposeMailData).subscribe((result) => {

      $("#mainbox_submitloader").hide();

      if (result.type == "success") {

        this._data.MessageBoxShowHide({
          'Msg': "Mail sent successfully to recipient(s)", 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
            //this.router.navigate(['/composemail/']);
          }
        });


      } else {
        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Success });

      }
    }, (err) => {
      console.log(err);
    });
  }

  SubmitComposeMailDetails(ComposeMailData): Observable<any> {
    // console.log(configureData);
    return this.http.post<any>(endpoint + "/PostComposeMailDetails/", JSON.stringify(ComposeMailData), httpOptions).pipe(

    );
  }
  /********* END SENT COMPOSE MAIL FUNCTIONS **********/

  /********* START MAKE DRAFT COMPOSE MAIL FUNCTIONS **********/
  finalDraftBtn() {

    var Result = true;

    var RecipientTo = this.ComposeMailData.To;
    var RecipientCc = this.ComposeMailData.Cc;
    var RecipientBcc = this.ComposeMailData.Bcc;

    //if (RecipientTo == "" && RecipientCc == "" && RecipientBcc == "") {
    //  alert("Please specify at least one recipient.");
    //  return false;
    //}

    var _MasterID = 0;

    try {
      _MasterID = Number(sessionStorage.getItem('currentUser'));
    } catch (e) {
      _MasterID = 0;
    }

    var tempEmpList: Array<string> = [];

    for (var i in this.empList) {
      tempEmpList.push(this.empList[i].newfilename);
    }

    this.ComposeMailData.Attachment = tempEmpList.join(",");

    this.ComposeMailData.MasterID = _MasterID;

    if ((RecipientTo == "" && RecipientCc == "" && RecipientBcc == "") && (this.ComposeMailData.Subject == "") && (this.ComposeMailData.Body == "")) {
      this._data.MessageBoxShowHide({ 'Msg': "Please specify at least one recipient, subject or body", 'Type': this._data.msg_Info });

      return false;
    }

    $("#mainbox_submitloader").show();

    this.SubmitDraftMailDetails(this.ComposeMailData).subscribe((result) => {

      $("#mainbox_submitloader").hide();

      if (result.type == "success") {

        this._data.MessageBoxShowHide({
          'Msg': "Mail saved as a draft successfully", 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
            //this.router.navigate(['/composemail/']);
          }
        });

      } else {
        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Error });

      }
    }, (err) => {
      console.log(err);
    });
  }

  SubmitDraftMailDetails(ComposeMailData): Observable<any> {

    return this.http.post<any>(endpoint + "/PostDraftMailDetails/", JSON.stringify(ComposeMailData), httpOptions).pipe(

    );
  }
  /********* END MAKE DRAFT COMPOSE MAIL FUNCTIONS **********/

  finalDiscardBtn() {

    this.router.navigate(['/inbox/']);
    //window.location.reload();
  }
}


