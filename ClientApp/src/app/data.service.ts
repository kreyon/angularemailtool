import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  private transfermaildatas = new BehaviorSubject<any>({ IsChanged: false, MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', AnyId: '', ReplyAllEmail: '' });
  transfermaildata = this.transfermaildatas.asObservable();

  // MAKING TAB COUNTS GLOBAL VARIABLE
  private _mailcountss = new BehaviorSubject<any>({ inboxmailcount: 0, sentmailcount: 0, draftmailcount: 0, outboxmailcount: 0, starredmailcount: 0, trashmailcount: 0 });
  _mailcounts = this._mailcountss.asObservable();

  constructor() { }
  
  changeGoal(transfermaildata) {
    this.transfermaildatas.next(transfermaildata)
  }

  changeinboxcounts(_mailcounts) {
    this._mailcountss.next(_mailcounts)
  }

  // THIS IS SIDE MENU OF MOBILE CASE
  hideSidemenuifOpen() {
    $('#slide-menu').removeClass('left-menu-open');
    $('.mobilebg').fadeOut(300);
  }

  msg_Error = 3;
  msg_Success = 4;
  msg_Info = 5;

  MsgData = function (data) {
    this.Msg = data.Msg,
      this.Heading = data.Heading,
      this.IsConfirm = data.IsConfirm,
      this.Type = data.Type,
      this.Event1 = data.Event1,
      this.Event2 = data.Event2
  };

  MessageBoxShowHide(content) {

    content.IsConfirm = content.IsConfirm === undefined ? false : content.IsConfirm;
    content.Heading = content.Heading === undefined ? 'Mail Box' : content.Heading;
    content.Type = content.Type === undefined ? this.msg_Info : content.Type;
    content.Msg = content.Msg === undefined ? 'Welcome to Mail Box' : content.Msg;

    if (content.IsConfirm) {

      $('#conf_msgpopup').find(".content_body").html("<p>" + content.Msg + "</p>");
      $('#conf_msgpopup').show();

    } else if (content.Type == this.msg_Success) {

      $('#success_msgpopup').find(".content_body").html("<p>" + content.Msg + "</p>");
      $('#success_msgpopup').show();

    } else if (content.Type == this.msg_Error) {

      $('#error_msgpopup').find(".content_body").html("<p>" + content.Msg + "</p>");
      $('#error_msgpopup').show();

    } else if (content.Type == this.msg_Info) {

      $('#info_msgpopup').find(".content_body").html("<p>" + content.Msg + "</p>");
      $('#info_msgpopup').show();

    }

    $('#overall_lightbox').show();

    // OK CLICK EVENT
    $('#overall_lightbox').find(".Message_box:visible").find('.popupClick').unbind();
    $('#overall_lightbox').find(".Message_box:visible").find('.popupClick').click(function () {
      $(this).parent().parent().hide();
      $(this).parent().parent().parent().hide();

      if (content.Event1 && typeof content.Event1 == 'function') {
        content.Event1();
      }
    });

    // CANCEL CLICK EVENT
    $('#overall_lightbox').find(".Message_box:visible").find('.cancelClick').unbind();
    $('#overall_lightbox').find(".Message_box:visible").find('.cancelClick').click(function () {

      $(this).parent().parent().hide();
      $(this).parent().parent().parent().hide();

      if (content.Event2 && typeof content.Event2 == 'function') {
        content.Event2();
      }
    });
  }

}

//<!--EXAMPLE
//  * HOW TO USE THIS ALERT BOXES

//    // FOR SUCCESS
//    * Message({ 'Msg': "Updated successfully", 'Type': msg_Success });

//    // FOR ERROR
//   * Message({ 'Msg': "Updated successfully", 'Type': msg_Error });

//    // FOR ALERT
//   * Message({ 'Msg': "Updated successfully", 'Type': msg_Info });

//    // FOR CONFIRMATION
//   * Message({
//  "Msg": "Do you really want to delete this", "IsConfirm": true, "Event1": function () {

//    // YES BUTTON EXCUTION
//    alert("yes click");
//  }
//});
//-->
