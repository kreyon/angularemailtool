import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarredmailComponent } from './starredmail.component';

describe('StarredmailComponent', () => {
  let component: StarredmailComponent;
  let fixture: ComponentFixture<StarredmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarredmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarredmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
