import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppComponent } from '../app.component';
//import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { DataService } from '../data.service';

export class attachmentimage {
  icon: string;
  filename: string;

}

const endpoint = 'https://localhost:44365/api/values';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-starredmail',
  templateUrl: './starredmail.component.html',
  styleUrls: ['./starredmail.component.css']
})

export class StarredmailComponent implements OnInit {

  apps$: Object;
  recipientdisplay = 'none';

  recipient_from_div = 'none';
  recipient_to_div = 'none';
  recipient_cc_div = 'none';
  recipient_bcc_div = 'none';

  from_emails_array: Array<string> = [];
  to_emails_array: Array<string> = [];
  cc_emails_array: Array<string> = [];
  bcc_emails_array: Array<string> = [];

  DeleteSentMailData = { MasterID: 0, DeleteID: 0 };

  MailBody: any;
  MailSub: any;
  MailFrom: any;
  MailDate: any;
  skip: any;
  search: any;
  skipnum: any;
  skiparray: Object;
  attachment: Object;
  fileUrl;
  PageClick = 1;
  _visiblePages = 5;
  SampleCounter = 1;
  empList: Array<attachmentimage> = [];
  pageStart: number = 1;
  pages: number = 4;

  transfermaildatas = { IsChanged: false, MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', AnyId: '', ReplyAllEmail: '' };

  constructor(private _data: DataService, private http: HttpClient/*, private sanitizer: DomSanitizer*/, private _route: ActivatedRoute, private _router: Router) { }

  //constructor(private http: HttpClient/*, private sanitizer: DomSanitizer*/) { }
  ngOnInit(): void {

    // THIS FUNCTION IS ONLY USING FOR MOBILE SIDE MENU CASE
    this._data.hideSidemenuifOpen();

    this.skip = 1;
    this.search = "";

    // INITIALIZE THE REQUEST OF GETTING ALL INBOX RECORDS
    this.GetSentEachRecords(this.skip);

    this._data.transfermaildata.subscribe(res => this.transfermaildatas = res);
    this._data.changeGoal(this.transfermaildatas);
  }

  // START METHOD FOR GETTING Sent RECORDS
  GetSentEachRecords(skip) {

    var searchval = "";
    if (this.search == "") {
      searchval = "blank";
    }
    else {
      searchval = this.search;
    }


    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;


    $("#loader").show();

    this.http.get<any>(endpoint + '/GetStarredRecords/' + skip + '/' + searchval + '/' + _MasterID).subscribe(response => {

      $("#loader").hide();

      // START BELOW CODE IF FOUND ANY ERROR
      try {

        if (response.type == "success") {

          this._data.MessageBoxShowHide({ 'Msg': response.msg, 'Type': this._data.msg_Success });

        } else if (response.type == "error") {
          this._data.MessageBoxShowHide({ 'Msg': response.msg, 'Type': this._data.msg_Error });

          return false;

        }
      } catch (e) { }
      // END

      var data = response[0]._MailData;

      if (Number(data.length) == 0) {
        this.resetvalues();
        this._data.MessageBoxShowHide({ 'Msg':"No sent mails found", 'Type': this._data.msg_Info });
        
        return false;
      }

      this.apps$ = data;

      // START THIS IS WILL NEED FOR DELETE
      this.DeleteSentMailData.MasterID = _MasterID;
      this.DeleteSentMailData.DeleteID = data[0].id;
      // END THIS IS WILL NEED FOR DELETE

      // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
      this.transfermaildatas.IsChanged = true;
      this.transfermaildatas.MasterID = _MasterID;
      //this.transfermaildatas.From = data[0].from;
      this.transfermaildatas.To = data[0].to;
      this.transfermaildatas.Cc = data[0].cc;
      this.transfermaildatas.Bcc = data[0].bcc;
      this.transfermaildatas.Subject = data[0].subject;
      this.transfermaildatas.Body = data[0].body;
      //this.transfermaildatas.Attachment = data[0]._AttachmentRecord;
      //this.transfermaildatas.AnyId = data[0].id;
      this.transfermaildatas.ReplyAllEmail = data[0].reply_All_Mails;
      // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY

      this.MailBody = data[0].body;
      this.MailSub = data[0].subject;
      this.MailFrom = (data[0].from == "" ? data[0].to : data[0].from);
      this.skipnum = response[0].totalPages;
      this.MailDate = data[0].date;

      this.attachment = data[0].attachment_Name;
      this.PageClick = skip;

      try {

        var SplitFrom = (data[0].from).split(",");
        var SplitTo = (data[0].to).split(",");
        var SplitCc = (data[0].cc).split(",");
        var SplitBcc = (data[0].bcc).split(",");

        for (var k in SplitFrom) {
          this.from_emails_array.push(SplitFrom[k]);
        }

        for (var k in SplitTo) {
          this.to_emails_array.push(SplitTo[k]);
        }

        for (var k in SplitCc) {
          this.cc_emails_array.push(SplitCc[k]);
        }

        for (var k in SplitBcc) {
          this.bcc_emails_array.push(SplitBcc[k]);
        }

        if (Number(SplitFrom) == 0)
          this.recipient_from_div = 'none';
        else
          this.recipient_from_div = 'block';

        if (Number(SplitTo) == 0)
          this.recipient_to_div = 'none';
        else
          this.recipient_to_div = 'block';

        if (Number(SplitCc) == 0)
          this.recipient_cc_div = 'none';
        else
          this.recipient_cc_div = 'block';

        if (Number(SplitBcc) == 0)
          this.recipient_bcc_div = 'none';
        else
          this.recipient_bcc_div = 'block';

      } catch (e) { }
      // START HERE CODE START FOR ATTACHMENTS
      this.empList = [];
      for (var item in this.attachment) {

        var GetFileName = this.attachment[item];

        let customObj = new attachmentimage();

        customObj.filename = GetFileName;

        var SplitExt = GetFileName.split('.');

        var ext = SplitExt[1];

        if (ext == "doc" || ext == "docx") {
          customObj.icon = "word-icon.png";

        }
        else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {

          customObj.icon = "excel-icon.png";// this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/excel-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "jpg" || ext == "jpeg" || ext == "png") {

          customObj.icon = "img-large-icon.png";//  this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/img-large-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "pdf" || ext == "PDF") {

          customObj.icon = "pdf-large-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/pdf-large-icon.png" /><span class="file-attachted-name">' + source + '</span><span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "txt") {

          customObj.icon = "text-large-icon.png"; //   this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/text-large-icon.png" /><span class="file-attachted-name">' + source + '</span><span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "pptx" || ext == "ppt") {

          customObj.icon = "PowerPoint-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/PowerPoint-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }

        this.empList.push(customObj);

      }
      // END

      // START HERE CODE FOR PAGINATION NUMBERS
      var items: number[] = [];
      for (var i = 1; i <= this.skipnum; i++) {
        items.push(i);
      }
      this.skiparray = items;
      this.refreshItems();
      // END

    });
  }
  // END METHOD FOR GETTING INBOX RECORDS

  // START CLICKING EVENTS OF SUBJECT BOX THEN IT WILL SHOW THE IT'S RELATED BODY
  GetInboxEmaildetails(dis): void {



    var WindowsWidth = window.innerWidth;

    if (WindowsWidth < 768) {
      $(".mob_contnrinboxlist").css('margin-left', '-100%');
      $(".mob_contnr-inbox").css('margin-left', '0');

    }

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    // START THIS IS WILL NEED FOR DELETE
    this.DeleteSentMailData.MasterID = _MasterID;
    this.DeleteSentMailData.DeleteID = dis.id;
    // END THIS IS WILL NEED FOR DELETE

    // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
    this.transfermaildatas.IsChanged = true;
    this.transfermaildatas.MasterID = _MasterID;
    //this.transfermaildatas.From = dis.from;
    this.transfermaildatas.To = dis.to;
    this.transfermaildatas.Cc = dis.cc;
    this.transfermaildatas.Bcc = dis.bcc;
    this.transfermaildatas.Subject = dis.subject;
    this.transfermaildatas.Body = dis.body;
    //this.transfermaildatas.Attachment = dis._AttachmentRecord;
    //this.transfermaildatas.AnyId = data[0].id;
    this.transfermaildatas.ReplyAllEmail = dis.reply_All_Mails;
    // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY

    this.MailBody = dis.body;
    this.MailSub = dis.subject;
    this.MailFrom = (dis.from == "" ? dis.to : dis.from);
    this.attachment = dis.attachment_Name;
    this.MailDate = dis.date;


    // START HERE CODE START FOR ATTACHMENTS
    this.empList = [];
    for (var item in this.attachment) {

      var GetFileName = this.attachment[item];

      let customObj = new attachmentimage();

      customObj.filename = GetFileName;

      var SplitExt = GetFileName.split('.');

      var ext = SplitExt[1];

      if (ext == "doc" || ext == "docx") {
        customObj.icon = "word-icon.png";
      }
      else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {
        customObj.icon = "excel-icon.png";
      }
      else if (ext == "jpg" || ext == "jpeg" || ext == "png") {
        customObj.icon = "img-large-icon.png";
      }
      else if (ext == "pdf" || ext == "PDF") {
        customObj.icon = "pdf-large-icon.png";
      }
      else if (ext == "txt") {
        customObj.icon = "text-large-icon.png";
      }
      else if (ext == "pptx" || ext == "ppt") {
        customObj.icon = "PowerPoint-icon.png";
      }

      this.empList.push(customObj);

    }
    // END
    try {

      this.recipientdisplay = 'none';

      var SplitFrom = (dis.from).split(",");
      var SplitTo = (dis.to).split(",");
      var SplitCc = (dis.cc).split(",");
      var SplitBcc = (dis.bcc).split(",");

      this.from_emails_array = [];
      this.to_emails_array = [];
      this.cc_emails_array = [];
      this.bcc_emails_array = [];

      for (var i in SplitFrom) {
        this.from_emails_array.push(SplitFrom[i]);
      }

      for (var i in SplitTo) {
        this.to_emails_array.push(SplitTo[i]);
      }

      for (var i in SplitCc) {
        this.cc_emails_array.push(SplitCc[i]);
      }

      for (var i in SplitBcc) {
        this.bcc_emails_array.push(SplitBcc[i]);
      }

      if (Number(SplitFrom) == 0)
        this.recipient_from_div = 'none';
      else
        this.recipient_from_div = 'block';

      if (Number(SplitTo) == 0)
        this.recipient_to_div = 'none';
      else
        this.recipient_to_div = 'block';

      if (Number(SplitCc) == 0)
        this.recipient_cc_div = 'none';
      else
        this.recipient_cc_div = 'block';

      if (Number(SplitBcc) == 0)
        this.recipient_bcc_div = 'none';
      else
        this.recipient_bcc_div = 'block';

    } catch (e) { }
  }
  // END

  // CLICK EVENT OF DOWNLOADING
  AttachmentDownloadClick(name) {


    let link = document.createElement("a");

    link.href = endpoint + '/DownloadAttachment/' + name;

    link.setAttribute('visibility', 'hidden');
    link.download = name;

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  // END


  //// DELETE SINGLE EMAIL
  //Deletemail() {

  //  this.SubmitDeleteSentMail(this.DeleteSentMailData).subscribe((result) => {

  //    if (result.type == "success") {

  //      alert("Mail moved into trash successfully");
  //      window.location.reload();
  //      //this.GetSentEachRecords(1);

  //    } else {

  //      alert(result.msg);

  //    }

  //  }, (err) => {
  //    console.log(err);
  //  });

  //}

  //SubmitDeleteSentMail(DeleteSentMailData): Observable<any> {

  //  return this.http.post<any>(endpoint + "/PostDeleteSentMail/", JSON.stringify(DeleteSentMailData), httpOptions).pipe(

  //  );
  //}


  Replyemail() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "StarredReply" } });
  }


  ReplyAllemail() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "StarredReplyAll" } });
  }


  Forwardemail() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "StarredForward" } });
  }

  // SEARCH ENTER EVENT
  eventHandler(event) {

    if (event.keyCode == 13)
      this.GetSentEachRecords(1);

  }

  dropdownMenuLink() {

    if (this.recipientdisplay == 'block') {
      this.recipientdisplay = 'none';
    } else {
      this.recipientdisplay = 'block';
    }

  }


  prevPage() {
    if (this.PageClick > 1) {
      this.PageClick--;
    }
    if (this.PageClick < this.pageStart) {
      this.pageStart = this.PageClick;
    }
    this.GetSentEachRecords(this.PageClick);
    //this.refreshItems();
  }

  nextPage() {
   
    if (this.PageClick < this.skipnum) {
      this.PageClick++;
    }
    if (this.PageClick >= (this.pageStart + this.pages)) {
      this.pageStart = this.PageClick - this.pages + 1;
    }
    this.GetSentEachRecords(this.PageClick);
    /// this.refreshItems();
  }


  fillArray(): any {
    
    var obj = new Array();
    if (this.skipnum < this.pages) {
      this.pages = this.skipnum;
    }
    for (var index = this.pageStart; index < this.pageStart + this.pages; index++) {
      obj.push(index);
    }
    return obj;
  }

  refreshItems() {
   
    //this.skiparray = this.skiparray.slice((this.PageClick - 1) * this.pageSize, (this.currentIndex) * this.pageSize);
    this.skiparray = this.fillArray();
  }


  resetvalues() {
    this.apps$ = [];
    this.MailBody = "";
    this.MailSub = "";
    this.MailFrom = "";
    this.skipnum = "";
    this.MailDate = "";
    this.DeleteSentMailData.MasterID = 0;
    this.DeleteSentMailData.DeleteID = 0;
    this.transfermaildatas.IsChanged = true;
    this.transfermaildatas.MasterID = 0;
    this.transfermaildatas.To = "";
    this.transfermaildatas.Cc = "";
    this.transfermaildatas.Bcc = "";
    this.transfermaildatas.Subject = "";
    this.transfermaildatas.Body = "";
    this.transfermaildatas.ReplyAllEmail = "";
    this.attachment = [];
    this.skiparray = [];
  }


  /********* start make star & revoke mails ************/
  //SaveStarClick(item) {

  //  var _MasterID = Number(sessionStorage.getItem('currentUser'));

  //  if (isNaN(_MasterID))
  //    _MasterID = 0;

  //  var SaveInboxMailData = { MasterID: _MasterID, DeleteID: item.id, labeltxt: "Sent" };

  //  this.SubmitSaveStarMail(SaveInboxMailData).subscribe((result) => {

  //    window.location.reload();

  //  }, (err) => {
  //    console.log(err);
  //  });

  //}

  //SubmitSaveStarMail(SaveInboxMailData): Observable<any> {
  //  // console.log(configureData);
  //  return this.http.post<any>(endpoint + "/PostSaveStarMailOther/", JSON.stringify(SaveInboxMailData), httpOptions).pipe(
  //    //tap((product) => /*console.log('added product w/ id=${product.id}')*/),
  //    //catchError(this.handleError<any>('addProduct'))
  //  );
  //}

  RemoveStarClick(ID) {

    this.http.get<any>(endpoint + '/GetRemoveStarEvent/' + ID).subscribe(data => {

      window.location.reload();

    });

  }
  /********* end *******************************************/


  closedetail() {

    $(".mob_contnrinboxlist").css('margin-left', '0');
    $(".mob_contnr-inbox").css('margin-left', '-100%');
  }

}
