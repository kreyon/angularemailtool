import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent  {

  ngOnInit(): void{


  }

  fn_mobilebgclick() {
    $('#slide-menu').removeClass('left-menu-open');
    $('.mobilebg').fadeOut(300);
  }

  
}
