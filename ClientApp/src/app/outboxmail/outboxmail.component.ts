import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppComponent } from '../app.component';
//import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { DataService } from '../data.service';

export class attachmentimage {
  icon: string;
  filename: string;

}

const endpoint = 'https://localhost:44365/api/values';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Component({
  selector: 'app-outboxmail',
  templateUrl: './outboxmail.component.html',
  styleUrls: ['./outboxmail.component.css']
})
export class OutboxmailComponent implements OnInit {

  apps$: Object;
  recipientdisplay = 'none';

  recipient_to_div = 'none';
  recipient_cc_div = 'none';
  recipient_bcc_div = 'none';

  to_emails_array: Array<string> = [];
  cc_emails_array: Array<string> = [];
  bcc_emails_array: Array<string> = [];

  DeleteOutboxMailData = { MasterID: 0, DeleteID: 0 };

  MailBody: any;
  MailSub: any;
  MailFrom: any;
  MailDate: any;
  skip: any;
  search: any;
  skipnum: any;
  skiparray: Object;
  attachment: Object;
  fileUrl;
  PageClick = 1;
  _visiblePages = 5;
  SampleCounter = 1;
  empList: Array<attachmentimage> = [];
  pageStart: number = 1;
  pages: number = 4;

  isSelected = false;
  multipleDeleteContainer: Array<number> = [];

  transfermaildatas = { IsChanged: false, MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', AnyId: '', ReplyAllEmail: '' };

  constructor(private _data: DataService, private http: HttpClient/*, private sanitizer: DomSanitizer*/, private _route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {

    // THIS FUNCTION IS ONLY USING FOR MOBILE SIDE MENU CASE
    this._data.hideSidemenuifOpen();

    this.skip = 1;
    this.search = "";

    // INITIALIZE THE REQUEST OF GETTING ALL INBOX RECORDS
    this.GetOutboxEachRecords(this.skip);

    this._data.transfermaildata.subscribe(res => this.transfermaildatas = res);
    this._data.changeGoal(this.transfermaildatas);

  }

  // start checkbox change event
  CheckboxChildChange(event) {

    var value = event.target.value;

    if (event.target.checked) {

      //ADDING
      this.multipleDeleteContainer.push(value);

    } else {

      var tempMultipleDeleteContainer: Array<number> = [];

      // REMOVE
      for (var i in this.multipleDeleteContainer) {

        if (this.multipleDeleteContainer[i] != value) {
          tempMultipleDeleteContainer.push(this.multipleDeleteContainer[i]);
        }
      }

      // RE ASSIGNING TEMPORARY ARRAY
      this.multipleDeleteContainer = tempMultipleDeleteContainer;
    }

    console.log(this.multipleDeleteContainer)
  }
  // end

  GetOutboxEachRecords(skip) {

    var searchval = "";
    if (this.search == "") {
      searchval = "blank";
    }
    else {
      searchval = this.search;
    }


    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;


    $("#loader").show();

    this.http.get<any>(endpoint + '/GetOutboxRecords/' + skip + '/' + searchval + '/' + _MasterID).subscribe(response => {

      $("#loader").hide();

      // START BELOW CODE IF FOUND ANY ERROR
      try {

        if (response.type == "success") {
          this._data.MessageBoxShowHide({ 'Msg': response.msg, 'Type': this._data.msg_Success });
      
        } else if (response.type == "error") {
          this.apps$ = [];
          this._data.MessageBoxShowHide({ 'Msg': response.msg, 'Type': this._data.msg_Error });
       
          return false;

        }
      } catch (e) { }
      // END

      var data = response[0]._MailData;

      if (Number(data.length) == 0) {
        this.resetvalues();
        this._data.MessageBoxShowHide({ 'Msg': "No draft mails found", 'Type': this._data.msg_Info });
        return false;
      }

      this.apps$ = data;

      // START THIS IS WILL NEED FOR DELETE
      this.DeleteOutboxMailData.MasterID = _MasterID;
      this.DeleteOutboxMailData.DeleteID = data[0].id;
      // END THIS IS WILL NEED FOR DELETE

      // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
      this.transfermaildatas.IsChanged = true;
      this.transfermaildatas.MasterID = _MasterID;
      //this.transfermaildatas.From = data[0].from;
      this.transfermaildatas.To = data[0].to;
      this.transfermaildatas.Cc = data[0].cc;
      this.transfermaildatas.Bcc = data[0].bcc;
      this.transfermaildatas.Subject = data[0].subject;
      this.transfermaildatas.Body = data[0].body;
      //this.transfermaildatas.Attachment = data[0]._AttachmentRecord;
      //this.transfermaildatas.AnyId = data[0].id;
      this.transfermaildatas.ReplyAllEmail = data[0].reply_All_Mails;
      // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY

      this.MailBody = data[0].body;
      this.MailSub = data[0].subject;
      this.MailFrom = data[0].to;
      this.skipnum = response[0].totalPages;
      this.MailDate = data[0].date;

      this.attachment = data[0].attachment_Name;
      this.PageClick = skip;

      try {

        var SplitTo = (data[0].to).split(",");
        var SplitCc = (data[0].cc).split(",");
        var SplitBcc = (data[0].bcc).split(",");

        for (var k in SplitTo) {
          this.to_emails_array.push(SplitTo[k]);
        }

        for (var k in SplitCc) {
          this.cc_emails_array.push(SplitCc[k]);
        }

        for (var k in SplitBcc) {
          this.bcc_emails_array.push(SplitBcc[k]);
        }

        if (Number(SplitTo) == 0)
          this.recipient_to_div = 'none';
        else
          this.recipient_to_div = 'block';

        if (Number(SplitCc) == 0)
          this.recipient_cc_div = 'none';
        else
          this.recipient_cc_div = 'block';

        if (Number(SplitBcc) == 0)
          this.recipient_bcc_div = 'none';
        else
          this.recipient_bcc_div = 'block';

      } catch (e) { }

      // START HERE CODE START FOR ATTACHMENTS
      this.empList = [];
      for (var item in this.attachment) {

        var GetFileName = this.attachment[item];

        let customObj = new attachmentimage();

        customObj.filename = GetFileName;

        var SplitExt = GetFileName.split('.');

        var ext = SplitExt[1];

        if (ext == "doc" || ext == "docx") {
          customObj.icon = "word-icon.png";

        }
        else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {

          customObj.icon = "excel-icon.png";// this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/excel-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "jpg" || ext == "jpeg" || ext == "png") {

          customObj.icon = "img-large-icon.png";//  this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/img-large-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "pdf" || ext == "PDF") {

          customObj.icon = "pdf-large-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/pdf-large-icon.png" /><span class="file-attachted-name">' + source + '</span><span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "txt") {

          customObj.icon = "text-large-icon.png"; //   this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/text-large-icon.png" /><span class="file-attachted-name">' + source + '</span><span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }
        else if (ext == "pptx" || ext == "ppt") {

          customObj.icon = "PowerPoint-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/PowerPoint-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
        }

        this.empList.push(customObj);

      }
      // END

      // START HERE CODE FOR PAGINATION NUMBERS
      var items: number[] = [];
      for (var i = 1; i <= this.skipnum; i++) {
        items.push(i);
      }
      this.skiparray = items;
      this.refreshItems();
      // END

    });
  }
  // END METHOD FOR GETTING INBOX RECORDS

  // START CLICKING EVENTS OF SUBJECT BOX THEN IT WILL SHOW THE IT'S RELATED BODY
  GetOutboxEmaildetails(dis): void {


    var WindowsWidth = window.innerWidth;

    if (WindowsWidth < 768) {
      $(".mob_contnrinboxlist").css('margin-left', '-100%');
      $(".mob_contnr-inbox").css('margin-left', '0');

    }

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    // START THIS IS WILL NEED FOR DELETE
    this.DeleteOutboxMailData.MasterID = _MasterID;
    this.DeleteOutboxMailData.DeleteID = dis.id;
    // END THIS IS WILL NEED FOR DELETE

    // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
    this.transfermaildatas.IsChanged = true;
    this.transfermaildatas.MasterID = _MasterID;
    //this.transfermaildatas.From = dis.from;
    this.transfermaildatas.To = dis.to;
    this.transfermaildatas.Cc = dis.cc;
    this.transfermaildatas.Bcc = dis.bcc;
    this.transfermaildatas.Subject = dis.subject;
    this.transfermaildatas.Body = dis.body;
    //this.transfermaildatas.Attachment = dis._AttachmentRecord;
    //this.transfermaildatas.AnyId = data[0].id;
    this.transfermaildatas.ReplyAllEmail = dis.reply_All_Mails;
    // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY

    this.MailBody = dis.body;
    this.MailSub = dis.subject;
    this.MailFrom = dis.to;
    this.attachment = dis.attachment_Name;
    this.MailDate = dis.date;


    // START HERE CODE START FOR ATTACHMENTS
    this.empList = [];
    for (var item in this.attachment) {

      var GetFileName = this.attachment[item];

      let customObj = new attachmentimage();

      customObj.filename = GetFileName;

      var SplitExt = GetFileName.split('.');

      var ext = SplitExt[1];

      if (ext == "doc" || ext == "docx") {
        customObj.icon = "word-icon.png";
      }
      else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {
        customObj.icon = "excel-icon.png";
      }
      else if (ext == "jpg" || ext == "jpeg" || ext == "png") {
        customObj.icon = "img-large-icon.png";
      }
      else if (ext == "pdf" || ext == "PDF") {
        customObj.icon = "pdf-large-icon.png";
      }
      else if (ext == "txt") {
        customObj.icon = "text-large-icon.png";
      }
      else if (ext == "pptx" || ext == "ppt") {
        customObj.icon = "PowerPoint-icon.png";
      }

      this.empList.push(customObj);

    }
    // END

    try {

      this.recipientdisplay = 'none';

      var SplitTo = (dis.to).split(",");
      var SplitCc = (dis.cc).split(",");
      var SplitBcc = (dis.bcc).split(",");

      this.to_emails_array = [];
      this.cc_emails_array = [];
      this.bcc_emails_array = [];

      for (var i in SplitTo) {
        this.to_emails_array.push(SplitTo[i]);
      }

      for (var i in SplitCc) {
        this.cc_emails_array.push(SplitCc[i]);
      }

      for (var i in SplitBcc) {
        this.bcc_emails_array.push(SplitBcc[i]);
      }

      if (Number(SplitTo) == 0)
        this.recipient_to_div = 'none';
      else
        this.recipient_to_div = 'block';

      if (Number(SplitCc) == 0)
        this.recipient_cc_div = 'none';
      else
        this.recipient_cc_div = 'block';

      if (Number(SplitBcc) == 0)
        this.recipient_bcc_div = 'none';
      else
        this.recipient_bcc_div = 'block';

    } catch (e) { }
  }
  // END

  // CLICK EVENT OF DOWNLOADING
  AttachmentDownloadClick(name) {


    let link = document.createElement("a");

    link.href = endpoint + '/DownloadAttachment/' + name;

    link.setAttribute('visibility', 'hidden');
    link.download = name;

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  // END


  // DELETE SINGLE EMAIL
  Deletemail() {

    this.SubmitDeleteSentMail(this.DeleteOutboxMailData).subscribe((result) => {

      if (result.type == "success") {
        this._data.MessageBoxShowHide({
          'Msg': "Mail moved into trash successfully", 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
            //this.router.navigate(['/composemail/']);
          }
        });
        
      } else {

        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Error });

      }

    }, (err) => {
      console.log(err);
    });

  }

  SubmitDeleteSentMail(DeleteOutboxMailData): Observable<any> {

    return this.http.post<any>(endpoint + "/PostDeleteOutboxMail/", JSON.stringify(DeleteOutboxMailData), httpOptions).pipe(

    );
  }


  SentMailToCompose() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "DraftSent" } });
  }

  // SEARCH ENTER EVENT
  eventHandler(event) {

    if (event.keyCode == 13)
      this.GetOutboxEachRecords(1);

  }

  dropdownMenuLink() {

    if (this.recipientdisplay == 'block') {
      this.recipientdisplay = 'none';
    } else {
      this.recipientdisplay = 'block';
    }

  }

  prevPage() {
    if (this.PageClick > 1) {
      this.PageClick--;
    }
    if (this.PageClick < this.pageStart) {
      this.pageStart = this.PageClick;
    }
    this.GetOutboxEachRecords(this.PageClick);
    //this.refreshItems();
  }

  nextPage() {
   
    if (this.PageClick < this.skipnum) {
      this.PageClick++;
    }
    if (this.PageClick >= (this.pageStart + this.pages)) {
      this.pageStart = this.PageClick - this.pages + 1;
    }
    this.GetOutboxEachRecords(this.PageClick);
    /// this.refreshItems();
  }


  fillArray(): any {
 
    var obj = new Array();
    if (this.skipnum < this.pages) {
      this.pages = this.skipnum;
    }
    for (var index = this.pageStart; index < this.pageStart + this.pages; index++) {
      obj.push(index);
    }
    return obj;
  }

  refreshItems() {
   
    //this.skiparray = this.skiparray.slice((this.PageClick - 1) * this.pageSize, (this.currentIndex) * this.pageSize);
    this.skiparray = this.fillArray();
  }


  resetvalues() {
    this.apps$ = [];
    this.MailBody = "";
    this.MailSub = "";
    this.MailFrom = "";
    this.skipnum = "";
    this.MailDate = "";
    this.DeleteOutboxMailData.MasterID = 0;
    this.DeleteOutboxMailData.DeleteID = 0;
    this.transfermaildatas.IsChanged = true;
    this.transfermaildatas.MasterID = 0;
    this.transfermaildatas.To = "";
    this.transfermaildatas.Cc = "";
    this.transfermaildatas.Bcc = "";
    this.transfermaildatas.Subject = "";
    this.transfermaildatas.Body = "";
    this.transfermaildatas.ReplyAllEmail = "";
    this.attachment = [];
    this.skiparray = [];
  }



  /********* start make star & revoke mails ************/
  SaveStarClick(item) {

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    var SaveInboxMailData = { MasterID: _MasterID, DeleteID: item.id, labeltxt: "Outbox" };

    this.SubmitSaveStarMail(SaveInboxMailData).subscribe((result) => {

      window.location.reload();

    }, (err) => {
      console.log(err);
    });

  }

  SubmitSaveStarMail(SaveInboxMailData): Observable<any> {
    // console.log(configureData);
    return this.http.post<any>(endpoint + "/PostSaveStarMailOther/", JSON.stringify(SaveInboxMailData), httpOptions).pipe(
      //tap((product) => /*console.log('added product w/ id=${product.id}')*/),
      //catchError(this.handleError<any>('addProduct'))
    );
  }

  RemoveStarClick(ID) {

    this.http.get<any>(endpoint + '/GetRemoveStarEvent/' + ID).subscribe(data => {

      window.location.reload();

    });

  }
  /********* end *******************************************/

  CheckboxHeadSent(event) {
    if (event.target.checked) {
      this.isSelected = true;
    } else {
      this.isSelected = false;
    }
  }

  SentBulkDelete() {

    var DeleteList = this.multipleDeleteContainer;

    if (Number(DeleteList.length) == 0) {
      this._data.MessageBoxShowHide({ 'Msg': "Please select at least one record to delete", 'Type': this._data.msg_Info });
      
      return false;
    }

    this.SubmitBulkDeleteMail(this.multipleDeleteContainer).subscribe((result) => {
      if (result.type == "success") {
        this._data.MessageBoxShowHide({
          'Msg': "Mail moved into trash successfully", 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
            //this.router.navigate(['/composemail/']);
          }
        });

      } else {

        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Error });

      }
    }, (err) => {
      console.log(err);
    });
  }

  SubmitBulkDeleteMail(multipleDeleteContainer): Observable<any> {

    return this.http.post<any>(endpoint + "/PostOutboxBulkDeleteMail/" + multipleDeleteContainer.join(","), httpOptions).pipe(

    );
  }


  closedetail() {

    $(".mob_contnrinboxlist").css('margin-left', '0');
    $(".mob_contnr-inbox").css('margin-left', '-100%');
  }
}
