import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutboxmailComponent } from './outboxmail.component';

describe('OutboxmailComponent', () => {
  let component: OutboxmailComponent;
  let fixture: ComponentFixture<OutboxmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutboxmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutboxmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
