import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppComponent } from '../app.component';
//import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { DataService } from '../data.service';

export class MailReletedObject {
  MasterID: number;
  From: string;
  To: string;
  Cc: string;
  Bcc: string;
  Subject: string;
  Body: string;
  Attachment: string;
  DeleteID: string;
  ReplyAllEmail: string;
}

const endpoint = 'https://localhost:44365/api/values';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-inboxmail',
  templateUrl: './inboxmail.component.html',
  styleUrls: ['./inboxmail.component.css']
})


export class InboxmailComponent implements OnInit {

  apps$: Object;
  recipientdisplay = 'none';

  recipient_to_div = 'none';
  recipient_cc_div = 'none';
  recipient_bcc_div = 'none';

  to_emails_array: Array<string> = [];
  cc_emails_array: Array<string> = [];
  bcc_emails_array: Array<string> = [];

  __MailReletedObject: Array<MailReletedObject> = [];

  DeleteInboxMailData = { MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', DeleteID: '', ReplyAllEmail: '' };

  MailBody: any;
  MailSub: any;
  MailFrom: any;
  MailDate: any;
  skip: any;
  search: any;
  skipnum: any;
  skiparray: Object;
  attachment: Object;
  fileUrl;
  PageClick = 1;
  _visiblePages = 5;
  SampleCounter = 1;
  pageStart: number = 1;
  pages: number = 4;

  isSelected = false;
  multipleDeleteContainer: Array<string> = [];

  transfermaildatas = { IsChanged: false, MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', AnyId: '', ReplyAllEmail: '' };

  constructor(private _data: DataService, private http: HttpClient/*, private sanitizer: DomSanitizer*/, private _route: ActivatedRoute, private _router: Router) { }
  ngOnInit(): void {

    // THIS FUNCTION IS ONLY USING FOR MOBILE SIDE MENU CASE
    this._data.hideSidemenuifOpen();

    this.skip = 1;
    this.search = "";

    // INITIALIZE THE REQUEST OF GETTING ALL INBOX RECORDS
    this.GetInboxEachRecords(this.skip);


    this._data.transfermaildata.subscribe(res => this.transfermaildatas = res);
    this._data.changeGoal(this.transfermaildatas);
  }

  // START METHOD FOR GETTING INBOX RECORDS
  GetInboxEachRecords(skip) {

    var searchval = "";
    if (this.search == "") {
      searchval = "blank";
    }
    else {
      searchval = this.search;
    }

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    $("#loader").show();

    this.http.get(endpoint + '/GetInboxRecords/' + skip + '/' + searchval + '/' + _MasterID).subscribe(data => {
      $("#loader").hide();
      if (data == "error") {
        $("#loader").hide();
        this._data.MessageBoxShowHide({ 'Msg': "Invalid or no credential found", 'Type': this._data.msg_Error });
      
        return false;
      }
      if (data == "nomessage") {
        this.resetvalues();
        this._data.MessageBoxShowHide({ 'Msg': "No inbox mails found", 'Type': this._data.msg_Error });
        return false;
      }

      this.apps$ = data;

      // START THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
      this.DeleteInboxMailData.MasterID = _MasterID;
      this.DeleteInboxMailData.From = data[0].from;
      this.DeleteInboxMailData.To = data[0].to;
      this.DeleteInboxMailData.Cc = data[0].cc;
      this.DeleteInboxMailData.Bcc = data[0].bcc;
      this.DeleteInboxMailData.Subject = data[0].subject;
      this.DeleteInboxMailData.Body = data[0].body;
      //this.DeleteInboxMailData.Attachment = data[0];
      this.DeleteInboxMailData.DeleteID = data[0].id;
      this.DeleteInboxMailData.ReplyAllEmail = data[0].reply_All_Mails;
      // END THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY

      // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
      this.transfermaildatas.IsChanged = true;
      this.transfermaildatas.MasterID = _MasterID;
      this.transfermaildatas.From = data[0].from;
      this.transfermaildatas.To = data[0].to;
      this.transfermaildatas.Cc = data[0].cc;
      this.transfermaildatas.Bcc = data[0].bcc;
      this.transfermaildatas.Subject = data[0].subject;
      this.transfermaildatas.Body = data[0].body;
      this.transfermaildatas.Attachment = data[0]._AttachmentRecord;
      //this.transfermaildatas.AnyId = data[0].id;
      this.transfermaildatas.ReplyAllEmail = data[0].reply_All_Mails;
      // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY

      this.MailBody = data[0].body;
      this.MailSub = data[0].subject;
      this.MailFrom = data[0].from;
      this.skipnum = data[0].skipnum;
      this.MailDate = data[0].date;



      this.attachment = data[0]._AttachmentRecord;
      this.PageClick = skip;

      try {

        var SplitTo = (data[0].to).split(",");
        var SplitCc = (data[0].cc).split(",");
        var SplitBcc = (data[0].bcc).split(",");

        for (var k in SplitTo) {
          this.to_emails_array.push(SplitTo[k]);
        }

        for (var k in SplitCc) {
          this.cc_emails_array.push(SplitCc[k]);
        }

        for (var k in SplitBcc) {
          this.bcc_emails_array.push(SplitBcc[k]);
        }

        if (Number(SplitTo) == 0)
          this.recipient_to_div = 'none';
        else
          this.recipient_to_div = 'block';

        if (Number(SplitCc) == 0)
          this.recipient_cc_div = 'none';
        else
          this.recipient_cc_div = 'block';

        if (Number(SplitBcc) == 0)
          this.recipient_bcc_div = 'none';
        else
          this.recipient_bcc_div = 'block';

      } catch (e) { }


      // this.attachname = data[0].attachment_Name;

      var items: number[] = [];
      for (var i = 1; i <= this.skipnum; i++) {
        items.push(i);
      }
      this.skiparray = items;

      /**/
      this.refreshItems();


      /**/
    });
  }
  // END METHOD FOR GETTING INBOX RECORDS

  // START CLICKING EVENTS OF SUBJECT BOX THEN IT WILL SHOW THE IT'S RELATED BODY
  GetInboxEmaildetails(dis): void {

    var WindowsWidth = window.innerWidth;

    if (WindowsWidth < 768) {
      $(".mob_contnrinboxlist").css('margin-left', '-100%');
      $(".mob_contnr-inbox").css('margin-left', '0');

    }


    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    // START THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
    this.DeleteInboxMailData.MasterID = _MasterID;
    this.DeleteInboxMailData.From = dis.from;
    this.DeleteInboxMailData.To = dis.to;
    this.DeleteInboxMailData.Cc = dis.cc;
    this.DeleteInboxMailData.Bcc = dis.bcc;
    this.DeleteInboxMailData.Subject = dis.subject;
    this.DeleteInboxMailData.Body = dis.body;
    //  this.DeleteInboxMailData.Attachment = dis. 
    this.DeleteInboxMailData.DeleteID = dis.id;
    this.DeleteInboxMailData.ReplyAllEmail = dis.reply_All_Mails;
    // END THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY

    // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
    this.transfermaildatas.IsChanged = true;
    this.transfermaildatas.MasterID = _MasterID;
    this.transfermaildatas.From = dis.from;
    this.transfermaildatas.To = dis.to;
    this.transfermaildatas.Cc = dis.cc;
    this.transfermaildatas.Bcc = dis.bcc;
    this.transfermaildatas.Subject = dis.subject;
    this.transfermaildatas.Body = dis.body;
    this.transfermaildatas.Attachment = dis._AttachmentRecord;
    //this.transfermaildatas.AnyId = data[0].id;
    this.transfermaildatas.ReplyAllEmail = dis.reply_All_Mails;
    // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY

    this.MailBody = dis.body;
    this.MailSub = dis.subject;
    this.MailFrom = dis.from;
    this.attachment = dis._AttachmentRecord;
    ///this.attachname = dis.attachment_Name;

    try {

      this.recipientdisplay = 'none';

      var SplitTo = (dis.to).split(",");
      var SplitCc = (dis.cc).split(",");
      var SplitBcc = (dis.bcc).split(",");

      this.to_emails_array = [];
      this.cc_emails_array = [];
      this.bcc_emails_array = [];

      for (var i in SplitTo) {
        this.to_emails_array.push(SplitTo[i]);
      }

      for (var i in SplitCc) {
        this.cc_emails_array.push(SplitCc[i]);
      }

      for (var i in SplitBcc) {
        this.bcc_emails_array.push(SplitBcc[i]);
      }

      if (Number(SplitTo) == 0)
        this.recipient_to_div = 'none';
      else
        this.recipient_to_div = 'block';

      if (Number(SplitCc) == 0)
        this.recipient_cc_div = 'none';
      else
        this.recipient_cc_div = 'block';

      if (Number(SplitBcc) == 0)
        this.recipient_bcc_div = 'none';
      else
        this.recipient_bcc_div = 'block';

    } catch (e) { }
  }
  // END

  // CLICK EVENT OF DOWNLOADING
  AttachmentDownloadClick(name, value) {

    //var blob = new Blob([value], { type: 'application/octet-stream' });
    //this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));

    let byteCharacters = atob(value);

    let byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    let byteArray = new Uint8Array(byteNumbers);
    let blob = new Blob([byteArray], { "type": "application/octet-stream" });
    let link = document.createElement("a");

    link.href = URL.createObjectURL(blob);;

    link.setAttribute('visibility', 'hidden');
    link.download = name;

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  // END

  // DELETE SINGLE EMAIL
  Deletemail() {

    this.SubmitDeleteInboxMail(this.DeleteInboxMailData).subscribe((result) => {

      if (result.type == "success") {
        this._data.MessageBoxShowHide({
          'Msg': "Mail moved into trash successfully", 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
            //this.router.navigate(['/composemail/']);
          }
        });
      } else {

        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Error });

      }

    }, (err) => {
      console.log(err);
    });

  }

  SubmitDeleteInboxMail(DeleteInboxMailData): Observable<any> {

    return this.http.post<any>(endpoint + "/PostDeleteInboxMail/", JSON.stringify(DeleteInboxMailData), httpOptions).pipe(

    );
  }

  Replyemail() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "InboxReply" } });
  }


  ReplyAllemail() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "InboxReplyAll" } });
  }


  Forwardemail() {

    this.transfermaildatas = this.transfermaildatas;

    this._data.changeGoal(this.transfermaildatas);

    this._router.navigate(['/composemail'], { queryParams: { name: "InboxForward" } });
  }

  // SEARCH ENTER EVENT
  eventHandler(event) {

    if (event.keyCode == 13)
      this.GetInboxEachRecords(1);

  }

  dropdownMenuLink() {

    if (this.recipientdisplay == 'block') {
      this.recipientdisplay = 'none';
    } else {
      this.recipientdisplay = 'block';
    }

  }

  prevPage() {
    if (this.PageClick > 1) {
      this.PageClick--;
    }
    if (this.PageClick < this.pageStart) {
      this.pageStart = this.PageClick;
    }
    this.GetInboxEachRecords(this.PageClick);
    //this.refreshItems();
  }

  nextPage() {

    if (this.PageClick < this.skipnum) {
      this.PageClick++;
    }
    if (this.PageClick >= (this.pageStart + this.pages)) {
      this.pageStart = this.PageClick - this.pages + 1;
    }
    this.GetInboxEachRecords(this.PageClick);
    /// this.refreshItems();
  }

  fillArray(): any {

    var obj = new Array();
    if (this.skipnum < this.pages) {
      this.pages = this.skipnum;
    }
    for (var index = this.pageStart; index < this.pageStart + this.pages; index++) {
      obj.push(index);
    }
    return obj;
  }

  refreshItems() {

    //this.skiparray = this.skiparray.slice((this.PageClick - 1) * this.pageSize, (this.currentIndex) * this.pageSize);
    this.skiparray = this.fillArray();
  }

  resetvalues() {
    this.apps$ = [];
    this.MailBody = "";
    this.MailSub = "";
    this.MailFrom = "";
    this.skipnum = "";
    this.MailDate = "";
    this.DeleteInboxMailData.MasterID = 0;
    this.DeleteInboxMailData.From = "";
    this.DeleteInboxMailData.To = "";
    this.DeleteInboxMailData.Cc = "";
    this.DeleteInboxMailData.Bcc = "";
    this.DeleteInboxMailData.Subject = "";
    this.DeleteInboxMailData.Body = "";
    //this.DeleteInboxMailData.Attachment = data[0];
    this.DeleteInboxMailData.DeleteID = "";
    this.DeleteInboxMailData.ReplyAllEmail = "";


    this.transfermaildatas.IsChanged = true;
    this.transfermaildatas.MasterID = 0;
    this.transfermaildatas.From = "";
    this.transfermaildatas.To = "";
    this.transfermaildatas.Cc = "";
    this.transfermaildatas.Bcc = "";
    this.transfermaildatas.Subject = "";
    this.transfermaildatas.Body = "";
    this.transfermaildatas.ReplyAllEmail = "";
    this.transfermaildatas.Attachment = "";
    this.attachment = [];
    this.skiparray = [];
  }

  /********* start make star & revoke mails ************/
  SaveStarClick(item) {

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    var SaveInboxMailData = { MasterID: _MasterID, From: item.from, To: item.to, Cc: item.cc, Bcc: item.bcc, Subject: item.subject, Body: item.body, Attachment: '', DeleteID: item.id, ReplyAllEmail: item.reply_All_Mails };

    $("#mainbox_submitloader").show();

    this.SubmitSaveStarMail(SaveInboxMailData).subscribe((result) => {

      $("#mainbox_submitloader").hide();

      window.location.reload();

    }, (err) => {
      console.log(err);
    });

  }

  SubmitSaveStarMail(SaveInboxMailData): Observable<any> {
    // console.log(configureData);
    return this.http.post<any>(endpoint + "/PostSaveStarMail/", JSON.stringify(SaveInboxMailData), httpOptions).pipe(
      //tap((product) => /*console.log('added product w/ id=${product.id}')*/),
      //catchError(this.handleError<any>('addProduct'))
    );
  }

  RemoveStarClick(ID) {

    $("#mainbox_submitloader").show();

    this.http.get<any>(endpoint + '/GetRemoveStarEvent/' + ID).subscribe(data => {

      $("#mainbox_submitloader").hide();

      window.location.reload();

    });

  }
  /********* end *******************************************/

  // start checkbox change event
  CheckboxChildChange(event, dis) {

    var _MasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_MasterID))
      _MasterID = 0;

    var value = event.target.value;

    if (event.target.checked) {

      //ADDING
      this.multipleDeleteContainer.push(value);

      // START THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
      let customObj = new MailReletedObject();

      customObj.MasterID = _MasterID;
      customObj.From = dis.from;
      customObj.To = dis.to;
      customObj.Cc = dis.cc;
      customObj.Bcc = dis.bcc;
      customObj.Subject = dis.subject;
      customObj.Body = dis.body;
      //customObj.Attachment =  dis.;
      customObj.DeleteID = dis.id;
      customObj.ReplyAllEmail = dis.reply_All_Mails;

      // END THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
      this.__MailReletedObject.push(customObj);

    } else {

      /**************** 1st START JUST HOLDING INBOX ID *********************/

      var tempMultipleDeleteContainer: Array<string> = [];

      // REMOVE
      for (var i in this.multipleDeleteContainer) {

        if (this.multipleDeleteContainer[i] != value) {
          tempMultipleDeleteContainer.push(this.multipleDeleteContainer[i]);
        }
      }

      // RE ASSIGNING TEMPORARY ARRAY
      this.multipleDeleteContainer = tempMultipleDeleteContainer;

      /**************** 1st END JUST HOLDING INBOX ID *********************/

      /**************** 2nd START JUST HOLDING INBOX ID *********************/

      // START JUST HOLDING INBOX ID
      var temp__MailReletedObject: Array<MailReletedObject> = [];

      for (var i in this.__MailReletedObject) {

        if (this.__MailReletedObject[i].DeleteID != value) {

          let customObj = new MailReletedObject();

          customObj.MasterID = _MasterID;
          customObj.From = this.__MailReletedObject[i].From;
          customObj.To = this.__MailReletedObject[i].To;
          customObj.Cc = this.__MailReletedObject[i].Cc;
          customObj.Bcc = this.__MailReletedObject[i].Bcc;
          customObj.Subject = this.__MailReletedObject[i].Subject;
          customObj.Body = this.__MailReletedObject[i].Body;
          //customObj.Attachment =  this.__MailReletedObject[i].;
          customObj.DeleteID = this.__MailReletedObject[i].DeleteID;
          customObj.ReplyAllEmail = this.__MailReletedObject[i].ReplyAllEmail;

          temp__MailReletedObject.push(customObj);
        }
      }

      // RE ASSIGNING TEMPORARY ARRAY
      this.__MailReletedObject = temp__MailReletedObject;

      /**************** 2ndEND JUST HOLDING INBOX ID *********************/

    }

    //console.log(this.multipleDeleteContainer);
    // console.log(this.__MailReletedObject);
  }
  // end


  CheckboxHeadSent(event) {
    if (event.target.checked) {
      this.isSelected = true;
    } else {
      this.isSelected = false;
    }
  }

  SentBulkDelete() {

    var DeleteList = this.multipleDeleteContainer;

    if (Number(DeleteList.length) == 0) {
      this._data.MessageBoxShowHide({ 'Msg': "Please select at least one record to delete", 'Type': this._data.msg_Info });
    
      return false;
    }

    this.SubmitBulkDeleteMail(this.__MailReletedObject).subscribe((result) => {
      if (result.type == "success") {
        this._data.MessageBoxShowHide({
          'Msg': "Mail(s) moved into trash successfully", 'Type': this._data.msg_Success, 'Event1': function () {
            window.location.reload();
            //this.router.navigate(['/composemail/']);
          }
        });
      } else {

        this._data.MessageBoxShowHide({ 'Msg': result.msg, 'Type': this._data.msg_Error });

      }
    }, (err) => {
      console.log(err);
    });
  }

  SubmitBulkDeleteMail(__MailReletedObject): Observable<any> {

    return this.http.post<any>(endpoint + "/PostInboxBulkDeleteMail/", JSON.stringify(__MailReletedObject), httpOptions).pipe(

    );

  }


  closedetail() {

    $(".mob_contnrinboxlist").css('margin-left', '0');
    $(".mob_contnr-inbox").css('margin-left', '-100%');
  }


}
