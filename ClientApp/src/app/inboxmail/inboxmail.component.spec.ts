import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxmailComponent } from './inboxmail.component';

describe('InboxmailComponent', () => {
  let component: InboxmailComponent;
  let fixture: ComponentFixture<InboxmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboxmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
