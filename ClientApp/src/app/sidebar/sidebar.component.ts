import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DataService } from '../data.service';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  _mailcounts = { inboxmailcount: 0, sentmailcount: 0, draftmailcount: 0, outboxmailcount: 0, starredmailcount: 0, trashmailcount: 0 };

  constructor(private _data: DataService, private http: HttpClient) { }

  ngOnInit() {

    this._data._mailcounts.subscribe(res => this._mailcounts = res);
    this._data.changeinboxcounts(this._mailcounts);

    var _GlobalMasterID = Number(sessionStorage.getItem('currentUser'));

    if (isNaN(_GlobalMasterID))
      _GlobalMasterID = 0;

    this.http.get<any>('https://localhost:44365/api/values/GetTabCount/' + _GlobalMasterID).subscribe(data => {
      
      this._mailcounts.inboxmailcount = data[0].inboxCount;
      this._mailcounts.sentmailcount = data[0].sentCount;
      this._mailcounts.draftmailcount = data[0].draftCount;
      this._mailcounts.outboxmailcount = data[0].outboxCount;
      this._mailcounts.starredmailcount = data[0].starredCount;
      this._mailcounts.trashmailcount = data[0].trashCount;
    });
  }


}
