import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { SendmailComponent } from './sendmail/sendmail.component';
import { InboxmailComponent } from './inboxmail/inboxmail.component';
import { AppRoutingModule } from './app-routing.module';
import { ComposemailComponent } from './composemail/composemail.component';
import { DraftmailComponent } from './draftmail/draftmail.component';
import { TrashmailComponent } from './trashmail/trashmail.component';
import { DataService } from './data.service';
import { OutboxmailComponent } from './outboxmail/outboxmail.component';
import { StarredmailComponent } from './starredmail/starredmail.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    SendmailComponent,
    InboxmailComponent,
    ComposemailComponent,
    DraftmailComponent,
    TrashmailComponent,
    OutboxmailComponent,
    StarredmailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularEditorModule,
  ],
 
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule {
 
 
}

