var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InboxmailComponent } from './inboxmail/inboxmail.component';
import { SendmailComponent } from './sendmail/sendmail.component';
import { ComposemailComponent } from './composemail/composemail.component';
import { DraftmailComponent } from './draftmail/draftmail.component';
import { TrashmailComponent } from './trashmail/trashmail.component';
import { OutboxmailComponent } from './outboxmail/outboxmail.component';
import { StarredmailComponent } from './starredmail/starredmail.component';
var routes = [
    {
        path: '',
        component: InboxmailComponent
    },
    {
        path: 'inbox',
        component: InboxmailComponent
    },
    {
        path: 'sendmail',
        component: SendmailComponent
    },
    {
        path: 'composemail',
        component: ComposemailComponent
    },
    {
        path: 'draftmail',
        component: DraftmailComponent
    },
    {
        path: 'trashmail',
        component: TrashmailComponent
    },
    {
        path: 'outboxmail',
        component: OutboxmailComponent
    },
    {
        path: 'starredmail',
        component: StarredmailComponent
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map