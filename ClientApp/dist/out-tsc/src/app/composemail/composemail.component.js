var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
var Custom = /** @class */ (function () {
    function Custom() {
    }
    return Custom;
}());
export { Custom };
var endpoint = 'https://localhost:44365/api/values';
var httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
var ComposemailComponent = /** @class */ (function () {
    function ComposemailComponent(route, http, router, _data) {
        this.route = route;
        this.http = http;
        this.router = router;
        this._data = _data;
        this.name = 'Angular 6';
        this.htmlContent = '';
        this.config = {
            editable: true,
            spellcheck: true,
            height: '15rem',
            minHeight: '5rem',
            // placeholder: 'Enter text here...',
            translate: 'no'
            //customClasses: [
            //  {
            //    name: "quote",
            //    class: "quote",
            //  },
            //  {
            //    name: 'redText',
            //    class: 'redText'
            //  },
            //  {
            //    name: "titleText",
            //    class: "titleText",
            //    tag: "h1",
            //  },
            //]
        };
        this.ComposeMailData = { MasterID: 0, To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '' };
        this.configureData = { AccessWith: '', SmtpHost: '', SmtpPort: '', IncomingHost: '', IncomingPort: '', EmailID: '', Password: '', Signature: '', Reply: false, ReplyAll: false, Forward: false, Compose: false };
        this.CcInputBox = 'none';
        this.BccInputBox = 'none';
        this.ProgressBarStrip = 'none';
        this.completionPercent = 0;
        //temp : 0;
        this.AttachedDiv = "";
        this.empList = [];
    }
    //constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }
    ComposemailComponent.prototype.ngOnInit = function () {
        var _this = this;
        // 1.
        this.route.queryParams.subscribe(function (params) {
            _this.referencename$ = params['name'];
        });
        // 2.
        this._data.transfermaildata.subscribe(function (res) { return _this.transfermaildatas = res; });
        // 3.
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        this.http.get(endpoint + '/GetDefaultConfigDetails/' + _MasterID).subscribe(function (data) {
            _this.configureData.Signature = data[0].signature;
            _this.configureData.Reply = data[0].reply;
            _this.configureData.ReplyAll = data[0].replyAll;
            _this.configureData.Compose = data[0].compose;
            _this.configureData.Forward = data[0].forward;
            _this.CallAnotherRestFuction();
        });
        //console.log(this.transfermaildatas);
        //console.log(this.referencename$);
    };
    ComposemailComponent.prototype.CallAnotherRestFuction = function () {
        if (this.transfermaildatas.IsChanged) {
            if (this.referencename$ == "InboxReply") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.From;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAddInboxSignature();
            }
            else if (this.referencename$ == "InboxReplyAll") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAllAddInboxSignature();
            }
            else if (this.referencename$ == "InboxForward") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ForwardAllAddInboxSignature();
            }
            else if (this.referencename$ == "SentReply") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.To;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAddInboxSignature();
            }
            else if (this.referencename$ == "SentReplyAll") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAllAddInboxSignature();
            }
            else if (this.referencename$ == "SentForward") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ForwardAllAddInboxSignature();
            }
            else if (this.referencename$ == "DraftSent") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.To;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
            }
            else if (this.referencename$ == "TrashReply") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.To;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAddInboxSignature();
            }
            else if (this.referencename$ == "TrashReplyAll") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAllAddInboxSignature();
            }
            else if (this.referencename$ == "TrashForward") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ForwardAllAddInboxSignature();
            }
            else if (this.referencename$ == "StarredReply") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.To;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAddInboxSignature();
            }
            else if (this.referencename$ == "StarredReplyAll") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ReplyAllAddInboxSignature();
            }
            else if (this.referencename$ == "StarredForward") {
                this.ComposeMailData.MasterID = this.transfermaildatas.MasterID;
                //this.ComposeMailData.To = this.transfermaildatas.ReplyAllEmail;
                this.ComposeMailData.Cc = this.transfermaildatas.Cc;
                this.ComposeMailData.Bcc = this.transfermaildatas.Bcc;
                this.ComposeMailData.Subject = this.transfermaildatas.Subject;
                this.ComposeMailData.Body = this.transfermaildatas.Body;
                //this.ComposeMailData.Attachment = this.transfermaildatas.Attachment;
                this.ForwardAllAddInboxSignature();
            }
        }
        else {
           
            if (this.configureData.Compose) {
                this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
            }
        }
    };
    ComposemailComponent.prototype.ReplyAddInboxSignature = function () {
        if (this.configureData.Reply) {
            this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
        }
    };
    ComposemailComponent.prototype.ReplyAllAddInboxSignature = function () {
        if (this.configureData.ReplyAll) {
            this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
        }
    };
    ComposemailComponent.prototype.ForwardAllAddInboxSignature = function () {
        if (this.configureData.Forward) {
            this.ComposeMailData.Body = this.ComposeMailData.Body + "<br> " + this.configureData.Signature;
        }
    };
    ComposemailComponent.prototype.CcButtonClick = function () {
        this.CcInputBox = 'block';
    };
    ComposemailComponent.prototype.BccButtonClick = function () {
        this.BccInputBox = 'block';
    };
    ComposemailComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        var fileGet = event.target.files[0];
        console.log('size', fileGet.size);
        console.log('type', fileGet.type);
        var sizefile = fileGet.size;
        // temp = temp + sizefile;
        var threeMb = 3 * 1024 * 1024;
        // var fivemb = 6 * 1024 * 1024;
        if (sizefile > threeMb) {
            //var cursize = parseFloat((event.files[0].size / 1048576)).toFixed(2);
            var cursize = (sizefile / 1048576).toFixed(2);
            alert("Your file size " + cursize + " MB. File size should not exceed 3 MB");
            // $(event).val('');
            return false;
        }
        //if (temp > fivemb) {
        //  Message({
        //    "Msg": "Yor total file size is greater than 6 MB. File size should not exceed 6MB", "Type": msg_Info
        //  });
        //  $(this).val('');
        //  temp = temp - sizefile;
        //  return false;
        //}
        var validFiles = ["jpg", "jpeg", "png", "doc", "pdf", "docx", "txt", "xlsm", "xltx", "xlsx", "xltm", "xls", "xlt", "ppt", "PDF", "pptx"];
        var source = fileGet.name;
        var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
        for (var i = 0; i < validFiles.length; i++) {
            if (validFiles[i] == ext) {
                break;
            }
        }
        if (i >= validFiles.length) {
            alert("Only following  file extensions are allowed : " + validFiles.join(","));
            //this.ComposeMailData.Attachment = "";
            //$(event).val('');
            //$('#filetxt').val('');
            return false;
        }
        var file = event.target.files[0];
        //this.ComposeMailData.Attachment = event.target.files[0];
        this.selectedFile = event.target.files[0];
        // this.http is the injected HttpClient
        var uploadData = new FormData();
        uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
        this.ProgressBarStrip = 'block';
        this.http.post(endpoint + '/ComposeFileUpload', uploadData, {
            reportProgress: true,
            observe: 'events'
        })
            .subscribe(function (event) {
            // debugger;
            if (event.type === HttpEventType.UploadProgress) {
                _this.completionPercent = Math.round(100 * event.loaded / event.total);
            }
            else if (event.type === HttpEventType.Response) {
                _this.ProgressBarStrip = 'none';
                _this.completionPercent = 0;
                try {
                    if (event.body.type == "success") {
                        var customObj = new Custom();
                        customObj.filename = source;
                        customObj.newfilename = event.body.anystring;
                        if (ext == "doc" || ext == "docx") {
                            customObj.icon = "word-icon.png";
                        }
                        else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {
                            customObj.icon = "excel-icon.png";
                        }
                        else if (ext == "jpg" || ext == "jpeg" || ext == "png") {
                            customObj.icon = "img-large-icon.png";
                        }
                        else if (ext == "pdf" || ext == "PDF") {
                            customObj.icon = "pdf-large-icon.png";
                        }
                        else if (ext == "txt") {
                            customObj.icon = "text-large-icon.png";
                        }
                        else if (ext == "pptx" || ext == "ppt") {
                            customObj.icon = "PowerPoint-icon.png";
                        }
                        _this.empList.push(customObj);
                    }
                    else {
                        // IF FOUND ANY ERROR
                        alert(event.body.msg);
                    }
                }
                catch (e) { }
            }
        });
    };
    ComposemailComponent.prototype.delpic = function (fileObj) {
        var _this = this;
        var file = fileObj.newfilename;
        this.http.get(endpoint + '/GetDeleteAttachmentSubmit/' + file).subscribe(function (result) {
            if (result.type == "success") {
                var tempEmpList = [];
                for (var i in _this.empList) {
                    if (_this.empList[i].newfilename == file) {
                        continue;
                    }
                    var customObj = new Custom();
                    customObj.icon = _this.empList[i].icon;
                    customObj.filename = _this.empList[i].filename;
                    customObj.newfilename = _this.empList[i].newfilename;
                    tempEmpList.push(customObj);
                }
                _this.empList = tempEmpList;
                alert("Attachment removed successfully");
            }
            else {
                alert(result.msg);
            }
        });
    };
    /********* START SENT COMPOSE MAIL FUNCTIONS **********/
    ComposemailComponent.prototype.finalSendBtn = function () {
        var Result = true;
        var RecipientTo = this.ComposeMailData.To;
        var RecipientCc = this.ComposeMailData.Cc;
        var RecipientBcc = this.ComposeMailData.Bcc;
        if (RecipientTo == "" && RecipientCc == "" && RecipientBcc == "") {
            alert("Please specify at least one recipient.");
            return false;
        }
        var _MasterID = 0;
        try {
            _MasterID = Number(sessionStorage.getItem('currentUser'));
        }
        catch (e) {
            _MasterID = 0;
        }
        var tempEmpList = [];
        for (var i in this.empList) {
            tempEmpList.push(this.empList[i].newfilename);
        }
        this.ComposeMailData.Attachment = tempEmpList.join(",");
        this.ComposeMailData.MasterID = _MasterID;
        this.SubmitComposeMailDetails(this.ComposeMailData).subscribe(function (result) {
            if (result.type == "success") {
                alert("Mail sent successfully to recipient(s)");
                //alert(result.msg);
                window.location.reload();
                //this.router.navigate(['/composemail/']);
            }
            else {
                alert(result.msg);
            }
        }, function (err) {
            console.log(err);
        });
    };
    ComposemailComponent.prototype.SubmitComposeMailDetails = function (ComposeMailData) {
        // console.log(configureData);
        return this.http.post(endpoint + "/PostComposeMailDetails/", JSON.stringify(ComposeMailData), httpOptions).pipe();
    };
    /********* END SENT COMPOSE MAIL FUNCTIONS **********/
    /********* START MAKE DRAFT COMPOSE MAIL FUNCTIONS **********/
    ComposemailComponent.prototype.finalDraftBtn = function () {
        var Result = true;
        var RecipientTo = this.ComposeMailData.To;
        var RecipientCc = this.ComposeMailData.Cc;
        var RecipientBcc = this.ComposeMailData.Bcc;
        //if (RecipientTo == "" && RecipientCc == "" && RecipientBcc == "") {
        //  alert("Please specify at least one recipient.");
        //  return false;
        //}
        var _MasterID = 0;
        try {
            _MasterID = Number(sessionStorage.getItem('currentUser'));
        }
        catch (e) {
            _MasterID = 0;
        }
        var tempEmpList = [];
        for (var i in this.empList) {
            tempEmpList.push(this.empList[i].newfilename);
        }
        this.ComposeMailData.Attachment = tempEmpList.join(",");
        this.ComposeMailData.MasterID = _MasterID;
        if ((RecipientTo == "" && RecipientCc == "" && RecipientBcc == "") && (this.ComposeMailData.Subject == "") && (this.ComposeMailData.Body == "")) {
            alert("Please specify at least one recipient, subject or body");
            return false;
        }
        this.SubmitDraftMailDetails(this.ComposeMailData).subscribe(function (result) {
            if (result.type == "success") {
                alert("Mail saved as a draft successfully");
                window.location.reload();
                //this.router.navigate(['/composemail/']);
            }
            else {
                alert(result.msg);
            }
        }, function (err) {
            console.log(err);
        });
    };
    ComposemailComponent.prototype.SubmitDraftMailDetails = function (ComposeMailData) {
        return this.http.post(endpoint + "/PostDraftMailDetails/", JSON.stringify(ComposeMailData), httpOptions).pipe();
    };
    /********* END MAKE DRAFT COMPOSE MAIL FUNCTIONS **********/
    ComposemailComponent.prototype.finalDiscardBtn = function () {
        this.router.navigate(['/inbox/']);
        //window.location.reload();
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], ComposemailComponent.prototype, "ComposeMailData", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], ComposemailComponent.prototype, "configureData", void 0);
    ComposemailComponent = __decorate([
        Component({
            selector: 'app-composemail',
            templateUrl: './composemail.component.html',
            styleUrls: ['./composemail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute, HttpClient, Router, DataService])
    ], ComposemailComponent);
    return ComposemailComponent;
}());
export { ComposemailComponent };
//# sourceMappingURL=composemail.component.js.map
