var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
var attachmentimage = /** @class */ (function () {
    function attachmentimage() {
    }
    return attachmentimage;
}());
export { attachmentimage };
var endpoint = 'https://localhost:44365/api/values';
var httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
var SendmailComponent = /** @class */ (function () {
    function SendmailComponent(_data, http /*, private sanitizer: DomSanitizer*/, _route, _router) {
        this._data = _data;
        this.http = http;
        this._route = _route;
        this._router = _router;
        this.recipientdisplay = 'none';
        this.recipient_to_div = 'none';
        this.recipient_cc_div = 'none';
        this.recipient_bcc_div = 'none';
        this.to_emails_array = [];
        this.cc_emails_array = [];
        this.bcc_emails_array = [];
        this.DeleteSentMailData = { MasterID: 0, DeleteID: 0 };
        this.PageClick = 1;
        this._visiblePages = 5;
        this.SampleCounter = 1;
        this.empList = [];
        this.pageStart = 1;
        this.pages = 4;
        this.isSelected = false;
        this.multipleDeleteContainer = [];
        this.transfermaildatas = { IsChanged: false, MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', AnyId: '', ReplyAllEmail: '' };
    }
    //constructor(private http: HttpClient/*, private sanitizer: DomSanitizer*/) { }
    SendmailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.skip = 1;
        this.search = "";
        // INITIALIZE THE REQUEST OF GETTING ALL INBOX RECORDS
        this.GetSentEachRecords(this.skip);
        this._data.transfermaildata.subscribe(function (res) { return _this.transfermaildatas = res; });
        this._data.changeGoal(this.transfermaildatas);
    };
    // start checkbox change event
    SendmailComponent.prototype.CheckboxChildChange = function (event) {
        var value = event.target.value;
        if (event.target.checked) {
            //ADDING
            this.multipleDeleteContainer.push(value);
        }
        else {
            var tempMultipleDeleteContainer = [];
            // REMOVE
            for (var i in this.multipleDeleteContainer) {
                if (this.multipleDeleteContainer[i] != value) {
                    tempMultipleDeleteContainer.push(this.multipleDeleteContainer[i]);
                }
            }
            // RE ASSIGNING TEMPORARY ARRAY
            this.multipleDeleteContainer = tempMultipleDeleteContainer;
        }
        console.log(this.multipleDeleteContainer);
    };
    // end
    // START METHOD FOR GETTING Sent RECORDS
    SendmailComponent.prototype.GetSentEachRecords = function (skip) {
        var _this = this;
        var searchval = "";
        if (this.search == "") {
            searchval = "blank";
        }
        else {
            searchval = this.search;
        }
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        $("#loader").show();
        this.http.get(endpoint + '/GetSentRecords/' + skip + '/' + searchval + '/' + _MasterID).subscribe(function (response) {
            $("#loader").hide();
            // START BELOW CODE IF FOUND ANY ERROR
            try {
                if (response.type == "success") {
                    alert(response.msg);
                }
                else if (response.type == "error") {
                    alert(response.msg);
                    return false;
                }
            }
            catch (e) { }
            // END
            var data = response[0]._MailData;
            if (Number(data.length) == 0) {
                _this.resetvalues();
                alert("No sent mails found");
                return false;
            }
            _this.apps$ = data;
            // START THIS IS WILL NEED FOR DELETE
            _this.DeleteSentMailData.MasterID = _MasterID;
            _this.DeleteSentMailData.DeleteID = data[0].id;
            // END THIS IS WILL NEED FOR DELETE
            // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
            _this.transfermaildatas.IsChanged = true;
            _this.transfermaildatas.MasterID = _MasterID;
            //this.transfermaildatas.From = data[0].from;
            _this.transfermaildatas.To = data[0].to;
            _this.transfermaildatas.Cc = data[0].cc;
            _this.transfermaildatas.Bcc = data[0].bcc;
            _this.transfermaildatas.Subject = data[0].subject;
            _this.transfermaildatas.Body = data[0].body;
            //this.transfermaildatas.Attachment = data[0]._AttachmentRecord;
            //this.transfermaildatas.AnyId = data[0].id;
            _this.transfermaildatas.ReplyAllEmail = data[0].reply_All_Mails;
            // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
            _this.MailBody = data[0].body;
            _this.MailSub = data[0].subject;
            _this.MailFrom = data[0].to;
            _this.skipnum = response[0].totalPages;
            _this.MailDate = data[0].date;
            _this.attachment = data[0].attachment_Name;
            _this.PageClick = skip;
            try {
                var SplitTo = (data[0].to).split(",");
                var SplitCc = (data[0].cc).split(",");
                var SplitBcc = (data[0].bcc).split(",");
                for (var k in SplitTo) {
                    _this.to_emails_array.push(SplitTo[k]);
                }
                for (var k in SplitCc) {
                    _this.cc_emails_array.push(SplitCc[k]);
                }
                for (var k in SplitBcc) {
                    _this.bcc_emails_array.push(SplitBcc[k]);
                }
                if (Number(SplitTo) == 0)
                    _this.recipient_to_div = 'none';
                else
                    _this.recipient_to_div = 'block';
                if (Number(SplitCc) == 0)
                    _this.recipient_cc_div = 'none';
                else
                    _this.recipient_cc_div = 'block';
                if (Number(SplitBcc) == 0)
                    _this.recipient_bcc_div = 'none';
                else
                    _this.recipient_bcc_div = 'block';
            }
            catch (e) { }
            // START HERE CODE START FOR ATTACHMENTS
            _this.empList = [];
            for (var item in _this.attachment) {
                var GetFileName = _this.attachment[item];
                var customObj = new attachmentimage();
                customObj.filename = GetFileName;
                var SplitExt = GetFileName.split('.');
                var ext = SplitExt[1];
                if (ext == "doc" || ext == "docx") {
                    customObj.icon = "word-icon.png";
                }
                else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {
                    customObj.icon = "excel-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/excel-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
                }
                else if (ext == "jpg" || ext == "jpeg" || ext == "png") {
                    customObj.icon = "img-large-icon.png"; //  this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/img-large-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
                }
                else if (ext == "pdf" || ext == "PDF") {
                    customObj.icon = "pdf-large-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/pdf-large-icon.png" /><span class="file-attachted-name">' + source + '</span><span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
                }
                else if (ext == "txt") {
                    customObj.icon = "text-large-icon.png"; //   this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/text-large-icon.png" /><span class="file-attachted-name">' + source + '</span><span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
                }
                else if (ext == "pptx" || ext == "ppt") {
                    customObj.icon = "PowerPoint-icon.png"; // this.AttachedDiv += '<div class="img-thumbnail img-attachment"><img src="../assets/images/PowerPoint-icon.png" /><span class="file-attachted-name">' + source + '</span> <span class="file-attachted-size"></span><div class="doc_downlod_hovr"><i class="mdi mdi-close" (click)=\'delpic(event,"' + newlyCreatedFileName + '");\'></i></div></div>';
                }
                _this.empList.push(customObj);
            }
            // END
            // START HERE CODE FOR PAGINATION NUMBERS
            var items = [];
            for (var i = 1; i <= _this.skipnum; i++) {
                items.push(i);
            }
            _this.skiparray = items;
            _this.refreshItems();
            // END
        });
    };
    // END METHOD FOR GETTING INBOX RECORDS
    // START CLICKING EVENTS OF SUBJECT BOX THEN IT WILL SHOW THE IT'S RELATED BODY
    SendmailComponent.prototype.GetInboxEmaildetails = function (dis) {
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        // START THIS IS WILL NEED FOR DELETE
        this.DeleteSentMailData.MasterID = _MasterID;
        this.DeleteSentMailData.DeleteID = dis.id;
        // END THIS IS WILL NEED FOR DELETE
        // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
        this.transfermaildatas.IsChanged = true;
        this.transfermaildatas.MasterID = _MasterID;
        //this.transfermaildatas.From = dis.from;
        this.transfermaildatas.To = dis.to;
        this.transfermaildatas.Cc = dis.cc;
        this.transfermaildatas.Bcc = dis.bcc;
        this.transfermaildatas.Subject = dis.subject;
        this.transfermaildatas.Body = dis.body;
        //this.transfermaildatas.Attachment = dis._AttachmentRecord;
        //this.transfermaildatas.AnyId = data[0].id;
        this.transfermaildatas.ReplyAllEmail = dis.reply_All_Mails;
        // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
        this.MailBody = dis.body;
        this.MailSub = dis.subject;
        this.MailFrom = dis.to;
        this.attachment = dis.attachment_Name;
        this.MailDate = dis.date;
        // START HERE CODE START FOR ATTACHMENTS
        this.empList = [];
        for (var item in this.attachment) {
            var GetFileName = this.attachment[item];
            var customObj = new attachmentimage();
            customObj.filename = GetFileName;
            var SplitExt = GetFileName.split('.');
            var ext = SplitExt[1];
            if (ext == "doc" || ext == "docx") {
                customObj.icon = "word-icon.png";
            }
            else if (ext == "xlsx" || ext == "xlsm" || ext == "xltx" || ext == "xltm" || ext == "xls" || ext == "xlt") {
                customObj.icon = "excel-icon.png";
            }
            else if (ext == "jpg" || ext == "jpeg" || ext == "png") {
                customObj.icon = "img-large-icon.png";
            }
            else if (ext == "pdf" || ext == "PDF") {
                customObj.icon = "pdf-large-icon.png";
            }
            else if (ext == "txt") {
                customObj.icon = "text-large-icon.png";
            }
            else if (ext == "pptx" || ext == "ppt") {
                customObj.icon = "PowerPoint-icon.png";
            }
            this.empList.push(customObj);
        }
        // END
        try {
            this.recipientdisplay = 'none';
            var SplitTo = (dis.to).split(",");
            var SplitCc = (dis.cc).split(",");
            var SplitBcc = (dis.bcc).split(",");
            this.to_emails_array = [];
            this.cc_emails_array = [];
            this.bcc_emails_array = [];
            for (var i in SplitTo) {
                this.to_emails_array.push(SplitTo[i]);
            }
            for (var i in SplitCc) {
                this.cc_emails_array.push(SplitCc[i]);
            }
            for (var i in SplitBcc) {
                this.bcc_emails_array.push(SplitBcc[i]);
            }
            if (Number(SplitTo) == 0)
                this.recipient_to_div = 'none';
            else
                this.recipient_to_div = 'block';
            if (Number(SplitCc) == 0)
                this.recipient_cc_div = 'none';
            else
                this.recipient_cc_div = 'block';
            if (Number(SplitBcc) == 0)
                this.recipient_bcc_div = 'none';
            else
                this.recipient_bcc_div = 'block';
        }
        catch (e) { }
    };
    // END
    // CLICK EVENT OF DOWNLOADING
    SendmailComponent.prototype.AttachmentDownloadClick = function (name) {
        var link = document.createElement("a");
        link.href = endpoint + '/DownloadAttachment/' + name;
        link.setAttribute('visibility', 'hidden');
        link.download = name;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };
    // END
    // DELETE SINGLE EMAIL
    SendmailComponent.prototype.Deletemail = function () {
        this.SubmitDeleteSentMail(this.DeleteSentMailData).subscribe(function (result) {
            if (result.type == "success") {
                alert("Mail moved into trash successfully");
                window.location.reload();
                //this.GetSentEachRecords(1);
            }
            else {
                alert(result.msg);
            }
        }, function (err) {
            console.log(err);
        });
    };
    SendmailComponent.prototype.SubmitDeleteSentMail = function (DeleteSentMailData) {
        return this.http.post(endpoint + "/PostDeleteSentMail/", JSON.stringify(DeleteSentMailData), httpOptions).pipe();
    };
    SendmailComponent.prototype.Replyemail = function () {
        this.transfermaildatas = this.transfermaildatas;
        this._data.changeGoal(this.transfermaildatas);
        this._router.navigate(['/composemail'], { queryParams: { name: "SentReply" } });
    };
    SendmailComponent.prototype.ReplyAllemail = function () {
        this.transfermaildatas = this.transfermaildatas;
        this._data.changeGoal(this.transfermaildatas);
        this._router.navigate(['/composemail'], { queryParams: { name: "SentReplyAll" } });
    };
    SendmailComponent.prototype.Forwardemail = function () {
        this.transfermaildatas = this.transfermaildatas;
        this._data.changeGoal(this.transfermaildatas);
        this._router.navigate(['/composemail'], { queryParams: { name: "SentForward" } });
    };
    // SEARCH ENTER EVENT
    SendmailComponent.prototype.eventHandler = function (event) {
        if (event.keyCode == 13)
            this.GetSentEachRecords(1);
    };
    SendmailComponent.prototype.dropdownMenuLink = function () {
        if (this.recipientdisplay == 'block') {
            this.recipientdisplay = 'none';
        }
        else {
            this.recipientdisplay = 'block';
        }
    };
    SendmailComponent.prototype.prevPage = function () {
        if (this.PageClick > 1) {
            this.PageClick--;
        }
        if (this.PageClick < this.pageStart) {
            this.pageStart = this.PageClick;
        }
        this.GetSentEachRecords(this.PageClick);
        //this.refreshItems();
    };
    SendmailComponent.prototype.nextPage = function () {
       
        if (this.PageClick < this.skipnum) {
            this.PageClick++;
        }
        if (this.PageClick >= (this.pageStart + this.pages)) {
            this.pageStart = this.PageClick - this.pages + 1;
        }
        this.GetSentEachRecords(this.PageClick);
        /// this.refreshItems();
    };
    SendmailComponent.prototype.fillArray = function () {
      
        var obj = new Array();
        if (this.skipnum < this.pages) {
            this.pages = this.skipnum;
        }
        for (var index = this.pageStart; index < this.pageStart + this.pages; index++) {
            obj.push(index);
        }
        return obj;
    };
    SendmailComponent.prototype.refreshItems = function () {
       
        //this.skiparray = this.skiparray.slice((this.PageClick - 1) * this.pageSize, (this.currentIndex) * this.pageSize);
        this.skiparray = this.fillArray();
    };
    SendmailComponent.prototype.resetvalues = function () {
        this.apps$ = [];
        this.MailBody = "";
        this.MailSub = "";
        this.MailFrom = "";
        this.skipnum = "";
        this.MailDate = "";
        this.DeleteSentMailData.MasterID = 0;
        this.DeleteSentMailData.DeleteID = 0;
        this.transfermaildatas.IsChanged = true;
        this.transfermaildatas.MasterID = 0;
        this.transfermaildatas.To = "";
        this.transfermaildatas.Cc = "";
        this.transfermaildatas.Bcc = "";
        this.transfermaildatas.Subject = "";
        this.transfermaildatas.Body = "";
        this.transfermaildatas.ReplyAllEmail = "";
        this.attachment = [];
        this.skiparray = [];
    };
    /********* start make star & revoke mails ************/
    SendmailComponent.prototype.SaveStarClick = function (item) {
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        var SaveInboxMailData = { MasterID: _MasterID, DeleteID: item.id, labeltxt: "Sent" };
        this.SubmitSaveStarMail(SaveInboxMailData).subscribe(function (result) {
            window.location.reload();
        }, function (err) {
            console.log(err);
        });
    };
    SendmailComponent.prototype.SubmitSaveStarMail = function (SaveInboxMailData) {
        // console.log(configureData);
        return this.http.post(endpoint + "/PostSaveStarMailOther/", JSON.stringify(SaveInboxMailData), httpOptions).pipe(
        //tap((product) => /*console.log('added product w/ id=${product.id}')*/),
        //catchError(this.handleError<any>('addProduct'))
        );
    };
    SendmailComponent.prototype.RemoveStarClick = function (ID) {
        this.http.get(endpoint + '/GetRemoveStarEvent/' + ID).subscribe(function (data) {
            window.location.reload();
        });
    };
    /********* end *******************************************/
    SendmailComponent.prototype.CheckboxHeadSent = function (event) {
        if (event.target.checked) {
            this.isSelected = true;
        }
        else {
            this.isSelected = false;
        }
    };
    SendmailComponent.prototype.SentBulkDelete = function () {
        var DeleteList = this.multipleDeleteContainer;
        if (Number(DeleteList.length) == 0) {
            alert("Please select at least one record to delete");
            return false;
        }
        this.SubmitBulkDeleteMail(this.multipleDeleteContainer).subscribe(function (result) {
            if (result.type == "success") {
                alert("Mail(s) moved into trash successfully");
                window.location.reload();
                // this.GetInboxEachRecords(1);
            }
            else {
                alert(result.msg);
            }
        }, function (err) {
            console.log(err);
        });
    };
    SendmailComponent.prototype.SubmitBulkDeleteMail = function (multipleDeleteContainer) {
        return this.http.post(endpoint + "/PostSentBulkDeleteMail/" + multipleDeleteContainer.join(","), httpOptions).pipe();
    };
    SendmailComponent = __decorate([
        Component({
            selector: 'app-sendmail',
            templateUrl: './sendmail.component.html',
            styleUrls: ['./sendmail.component.css']
        }),
        __metadata("design:paramtypes", [DataService, HttpClient /*, private sanitizer: DomSanitizer*/, ActivatedRoute, Router])
    ], SendmailComponent);
    return SendmailComponent;
}());
export { SendmailComponent };
//# sourceMappingURL=sendmail.component.js.map
