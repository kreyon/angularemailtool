var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
var MailReletedObject = /** @class */ (function () {
    function MailReletedObject() {
    }
    return MailReletedObject;
}());
export { MailReletedObject };
var endpoint = 'https://localhost:44365/api/values';
var httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
var InboxmailComponent = /** @class */ (function () {
    function InboxmailComponent(_data, http /*, private sanitizer: DomSanitizer*/, _route, _router) {
        this._data = _data;
        this.http = http;
        this._route = _route;
        this._router = _router;
        this.recipientdisplay = 'none';
        this.recipient_to_div = 'none';
        this.recipient_cc_div = 'none';
        this.recipient_bcc_div = 'none';
        this.to_emails_array = [];
        this.cc_emails_array = [];
        this.bcc_emails_array = [];
        this.__MailReletedObject = [];
        this.DeleteInboxMailData = { MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', DeleteID: '', ReplyAllEmail: '' };
        this.PageClick = 1;
        this._visiblePages = 5;
        this.SampleCounter = 1;
        this.pageStart = 1;
        this.pages = 4;
        this.isSelected = false;
        this.multipleDeleteContainer = [];
        this.transfermaildatas = { IsChanged: false, MasterID: 0, From: '', To: '', Cc: '', Bcc: '', Subject: '', Body: '', Attachment: '', AnyId: '', ReplyAllEmail: '' };
    }
    InboxmailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.skip = 1;
        this.search = "";
        // INITIALIZE THE REQUEST OF GETTING ALL INBOX RECORDS
        this.GetInboxEachRecords(this.skip);
        this._data.transfermaildata.subscribe(function (res) { return _this.transfermaildatas = res; });
        this._data.changeGoal(this.transfermaildatas);
    };
    // START METHOD FOR GETTING INBOX RECORDS
    InboxmailComponent.prototype.GetInboxEachRecords = function (skip) {
        var _this = this;
        var searchval = "";
        if (this.search == "") {
            searchval = "blank";
        }
        else {
            searchval = this.search;
        }
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        $("#loader").show();
        this.http.get(endpoint + '/GetInboxRecords/' + skip + '/' + searchval + '/' + _MasterID).subscribe(function (data) {
            $("#loader").hide();
            if (data == "error") {
                $("#loader").hide();
                alert("Invalid or no credential found");
                return false;
            }
            if (data == "nomessage") {
                _this.resetvalues();
                alert("No inbox mails found");
                return false;
            }
            _this.apps$ = data;
            // START THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
            _this.DeleteInboxMailData.MasterID = _MasterID;
            _this.DeleteInboxMailData.From = data[0].from;
            _this.DeleteInboxMailData.To = data[0].to;
            _this.DeleteInboxMailData.Cc = data[0].cc;
            _this.DeleteInboxMailData.Bcc = data[0].bcc;
            _this.DeleteInboxMailData.Subject = data[0].subject;
            _this.DeleteInboxMailData.Body = data[0].body;
            //this.DeleteInboxMailData.Attachment = data[0];
            _this.DeleteInboxMailData.DeleteID = data[0].id;
            _this.DeleteInboxMailData.ReplyAllEmail = data[0].reply_All_Mails;
            // END THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
            // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
            _this.transfermaildatas.IsChanged = true;
            _this.transfermaildatas.MasterID = _MasterID;
            _this.transfermaildatas.From = data[0].from;
            _this.transfermaildatas.To = data[0].to;
            _this.transfermaildatas.Cc = data[0].cc;
            _this.transfermaildatas.Bcc = data[0].bcc;
            _this.transfermaildatas.Subject = data[0].subject;
            _this.transfermaildatas.Body = data[0].body;
            _this.transfermaildatas.Attachment = data[0]._AttachmentRecord;
            //this.transfermaildatas.AnyId = data[0].id;
            _this.transfermaildatas.ReplyAllEmail = data[0].reply_All_Mails;
            // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
            _this.MailBody = data[0].body;
            _this.MailSub = data[0].subject;
            _this.MailFrom = data[0].from;
            _this.skipnum = data[0].skipnum;
            _this.MailDate = data[0].date;
            _this.attachment = data[0]._AttachmentRecord;
            _this.PageClick = skip;
            try {
                var SplitTo = (data[0].to).split(",");
                var SplitCc = (data[0].cc).split(",");
                var SplitBcc = (data[0].bcc).split(",");
                for (var k in SplitTo) {
                    _this.to_emails_array.push(SplitTo[k]);
                }
                for (var k in SplitCc) {
                    _this.cc_emails_array.push(SplitCc[k]);
                }
                for (var k in SplitBcc) {
                    _this.bcc_emails_array.push(SplitBcc[k]);
                }
                if (Number(SplitTo) == 0)
                    _this.recipient_to_div = 'none';
                else
                    _this.recipient_to_div = 'block';
                if (Number(SplitCc) == 0)
                    _this.recipient_cc_div = 'none';
                else
                    _this.recipient_cc_div = 'block';
                if (Number(SplitBcc) == 0)
                    _this.recipient_bcc_div = 'none';
                else
                    _this.recipient_bcc_div = 'block';
            }
            catch (e) { }
            // this.attachname = data[0].attachment_Name;
            var items = [];
            for (var i = 1; i <= _this.skipnum; i++) {
                items.push(i);
            }
            _this.skiparray = items;
            /**/
            _this.refreshItems();
            /**/
        });
    };
    // END METHOD FOR GETTING INBOX RECORDS
    // START CLICKING EVENTS OF SUBJECT BOX THEN IT WILL SHOW THE IT'S RELATED BODY
    InboxmailComponent.prototype.GetInboxEmaildetails = function (dis) {
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        // START THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
        this.DeleteInboxMailData.MasterID = _MasterID;
        this.DeleteInboxMailData.From = dis.from;
        this.DeleteInboxMailData.To = dis.to;
        this.DeleteInboxMailData.Cc = dis.cc;
        this.DeleteInboxMailData.Bcc = dis.bcc;
        this.DeleteInboxMailData.Subject = dis.subject;
        this.DeleteInboxMailData.Body = dis.body;
        //  this.DeleteInboxMailData.Attachment = dis. 
        this.DeleteInboxMailData.DeleteID = dis.id;
        this.DeleteInboxMailData.ReplyAllEmail = dis.reply_All_Mails;
        // END THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
        // START THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
        this.transfermaildatas.IsChanged = true;
        this.transfermaildatas.MasterID = _MasterID;
        this.transfermaildatas.From = dis.from;
        this.transfermaildatas.To = dis.to;
        this.transfermaildatas.Cc = dis.cc;
        this.transfermaildatas.Bcc = dis.bcc;
        this.transfermaildatas.Subject = dis.subject;
        this.transfermaildatas.Body = dis.body;
        this.transfermaildatas.Attachment = dis._AttachmentRecord;
        //this.transfermaildatas.AnyId = data[0].id;
        this.transfermaildatas.ReplyAllEmail = dis.reply_All_Mails;
        // END  THESE OBJECT VARIABLES WILL USE IN SEND TO COMPOSE MAIL FUNCTIONALITY
        this.MailBody = dis.body;
        this.MailSub = dis.subject;
        this.MailFrom = dis.from;
        this.attachment = dis._AttachmentRecord;
        ///this.attachname = dis.attachment_Name;
        try {
            this.recipientdisplay = 'none';
            var SplitTo = (dis.to).split(",");
            var SplitCc = (dis.cc).split(",");
            var SplitBcc = (dis.bcc).split(",");
            this.to_emails_array = [];
            this.cc_emails_array = [];
            this.bcc_emails_array = [];
            for (var i in SplitTo) {
                this.to_emails_array.push(SplitTo[i]);
            }
            for (var i in SplitCc) {
                this.cc_emails_array.push(SplitCc[i]);
            }
            for (var i in SplitBcc) {
                this.bcc_emails_array.push(SplitBcc[i]);
            }
            if (Number(SplitTo) == 0)
                this.recipient_to_div = 'none';
            else
                this.recipient_to_div = 'block';
            if (Number(SplitCc) == 0)
                this.recipient_cc_div = 'none';
            else
                this.recipient_cc_div = 'block';
            if (Number(SplitBcc) == 0)
                this.recipient_bcc_div = 'none';
            else
                this.recipient_bcc_div = 'block';
        }
        catch (e) { }
    };
    // END
    // CLICK EVENT OF DOWNLOADING
    InboxmailComponent.prototype.AttachmentDownloadClick = function (name, value) {
        //var blob = new Blob([value], { type: 'application/octet-stream' });
        //this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
        var byteCharacters = atob(value);
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        var blob = new Blob([byteArray], { "type": "application/octet-stream" });
        var link = document.createElement("a");
        link.href = URL.createObjectURL(blob);
        ;
        link.setAttribute('visibility', 'hidden');
        link.download = name;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };
    // END
    // DELETE SINGLE EMAIL
    InboxmailComponent.prototype.Deletemail = function () {
        this.SubmitDeleteInboxMail(this.DeleteInboxMailData).subscribe(function (result) {
            if (result.type == "success") {
                alert("Mail moved into trash successfully");
                window.location.reload();
                // this.GetInboxEachRecords(1);
            }
            else {
                alert(result.msg);
            }
        }, function (err) {
            console.log(err);
        });
    };
    InboxmailComponent.prototype.SubmitDeleteInboxMail = function (DeleteInboxMailData) {
        return this.http.post(endpoint + "/PostDeleteInboxMail/", JSON.stringify(DeleteInboxMailData), httpOptions).pipe();
    };
    InboxmailComponent.prototype.Replyemail = function () {
        this.transfermaildatas = this.transfermaildatas;
        this._data.changeGoal(this.transfermaildatas);
        this._router.navigate(['/composemail'], { queryParams: { name: "InboxReply" } });
    };
    InboxmailComponent.prototype.ReplyAllemail = function () {
        this.transfermaildatas = this.transfermaildatas;
        this._data.changeGoal(this.transfermaildatas);
        this._router.navigate(['/composemail'], { queryParams: { name: "InboxReplyAll" } });
    };
    InboxmailComponent.prototype.Forwardemail = function () {
        this.transfermaildatas = this.transfermaildatas;
        this._data.changeGoal(this.transfermaildatas);
        this._router.navigate(['/composemail'], { queryParams: { name: "InboxForward" } });
    };
    // SEARCH ENTER EVENT
    InboxmailComponent.prototype.eventHandler = function (event) {
        if (event.keyCode == 13)
            this.GetInboxEachRecords(1);
    };
    InboxmailComponent.prototype.dropdownMenuLink = function () {
        if (this.recipientdisplay == 'block') {
            this.recipientdisplay = 'none';
        }
        else {
            this.recipientdisplay = 'block';
        }
    };
    InboxmailComponent.prototype.prevPage = function () {
        if (this.PageClick > 1) {
            this.PageClick--;
        }
        if (this.PageClick < this.pageStart) {
            this.pageStart = this.PageClick;
        }
        this.GetInboxEachRecords(this.PageClick);
        //this.refreshItems();
    };
    InboxmailComponent.prototype.nextPage = function () {
        if (this.PageClick < this.skipnum) {
            this.PageClick++;
        }
        if (this.PageClick >= (this.pageStart + this.pages)) {
            this.pageStart = this.PageClick - this.pages + 1;
        }
        this.GetInboxEachRecords(this.PageClick);
        /// this.refreshItems();
    };
    InboxmailComponent.prototype.fillArray = function () {
        var obj = new Array();
        if (this.skipnum < this.pages) {
            this.pages = this.skipnum;
        }
        for (var index = this.pageStart; index < this.pageStart + this.pages; index++) {
            obj.push(index);
        }
        return obj;
    };
    InboxmailComponent.prototype.refreshItems = function () {
        //this.skiparray = this.skiparray.slice((this.PageClick - 1) * this.pageSize, (this.currentIndex) * this.pageSize);
        this.skiparray = this.fillArray();
    };
    InboxmailComponent.prototype.resetvalues = function () {
        this.apps$ = [];
        this.MailBody = "";
        this.MailSub = "";
        this.MailFrom = "";
        this.skipnum = "";
        this.MailDate = "";
        this.DeleteInboxMailData.MasterID = 0;
        this.DeleteInboxMailData.From = "";
        this.DeleteInboxMailData.To = "";
        this.DeleteInboxMailData.Cc = "";
        this.DeleteInboxMailData.Bcc = "";
        this.DeleteInboxMailData.Subject = "";
        this.DeleteInboxMailData.Body = "";
        //this.DeleteInboxMailData.Attachment = data[0];
        this.DeleteInboxMailData.DeleteID = "";
        this.DeleteInboxMailData.ReplyAllEmail = "";
        this.transfermaildatas.IsChanged = true;
        this.transfermaildatas.MasterID = 0;
        this.transfermaildatas.From = "";
        this.transfermaildatas.To = "";
        this.transfermaildatas.Cc = "";
        this.transfermaildatas.Bcc = "";
        this.transfermaildatas.Subject = "";
        this.transfermaildatas.Body = "";
        this.transfermaildatas.ReplyAllEmail = "";
        this.transfermaildatas.Attachment = "";
        this.attachment = [];
        this.skiparray = [];
    };
    /********* start make star & revoke mails ************/
    InboxmailComponent.prototype.SaveStarClick = function (item) {
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        var SaveInboxMailData = { MasterID: _MasterID, From: item.from, To: item.to, Cc: item.cc, Bcc: item.bcc, Subject: item.subject, Body: item.body, Attachment: '', DeleteID: item.id, ReplyAllEmail: item.reply_All_Mails };
        this.SubmitSaveStarMail(SaveInboxMailData).subscribe(function (result) {
            window.location.reload();
        }, function (err) {
            console.log(err);
        });
    };
    InboxmailComponent.prototype.SubmitSaveStarMail = function (SaveInboxMailData) {
        // console.log(configureData);
        return this.http.post(endpoint + "/PostSaveStarMail/", JSON.stringify(SaveInboxMailData), httpOptions).pipe(
        //tap((product) => /*console.log('added product w/ id=${product.id}')*/),
        //catchError(this.handleError<any>('addProduct'))
        );
    };
    InboxmailComponent.prototype.RemoveStarClick = function (ID) {
        this.http.get(endpoint + '/GetRemoveStarEvent/' + ID).subscribe(function (data) {
            window.location.reload();
        });
    };
    /********* end *******************************************/
    // start checkbox change event
    InboxmailComponent.prototype.CheckboxChildChange = function (event, dis) {
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        var value = event.target.value;
        if (event.target.checked) {
            //ADDING
            this.multipleDeleteContainer.push(value);
            // START THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
            var customObj = new MailReletedObject();
            customObj.MasterID = _MasterID;
            customObj.From = dis.from;
            customObj.To = dis.to;
            customObj.Cc = dis.cc;
            customObj.Bcc = dis.bcc;
            customObj.Subject = dis.subject;
            customObj.Body = dis.body;
            //customObj.Attachment =  dis.;
            customObj.DeleteID = dis.id;
            customObj.ReplyAllEmail = dis.reply_All_Mails;
            // END THESE OBJECT VARIABLES WILL USE IN DELETE MAILS FUNCTIONALITY
            this.__MailReletedObject.push(customObj);
        }
        else {
            /**************** 1st START JUST HOLDING INBOX ID *********************/
            var tempMultipleDeleteContainer = [];
            // REMOVE
            for (var i in this.multipleDeleteContainer) {
                if (this.multipleDeleteContainer[i] != value) {
                    tempMultipleDeleteContainer.push(this.multipleDeleteContainer[i]);
                }
            }
            // RE ASSIGNING TEMPORARY ARRAY
            this.multipleDeleteContainer = tempMultipleDeleteContainer;
            /**************** 1st END JUST HOLDING INBOX ID *********************/
            /**************** 2nd START JUST HOLDING INBOX ID *********************/
            // START JUST HOLDING INBOX ID
            var temp__MailReletedObject = [];
            for (var i in this.__MailReletedObject) {
                if (this.__MailReletedObject[i].DeleteID != value) {
                    var customObj = new MailReletedObject();
                    customObj.MasterID = _MasterID;
                    customObj.From = this.__MailReletedObject[i].From;
                    customObj.To = this.__MailReletedObject[i].To;
                    customObj.Cc = this.__MailReletedObject[i].Cc;
                    customObj.Bcc = this.__MailReletedObject[i].Bcc;
                    customObj.Subject = this.__MailReletedObject[i].Subject;
                    customObj.Body = this.__MailReletedObject[i].Body;
                    //customObj.Attachment =  this.__MailReletedObject[i].;
                    customObj.DeleteID = this.__MailReletedObject[i].DeleteID;
                    customObj.ReplyAllEmail = this.__MailReletedObject[i].ReplyAllEmail;
                    temp__MailReletedObject.push(customObj);
                }
            }
            // RE ASSIGNING TEMPORARY ARRAY
            this.__MailReletedObject = temp__MailReletedObject;
            /**************** 2ndEND JUST HOLDING INBOX ID *********************/
        }
        //console.log(this.multipleDeleteContainer);
        // console.log(this.__MailReletedObject);
    };
    // end
    InboxmailComponent.prototype.CheckboxHeadSent = function (event) {
        if (event.target.checked) {
            this.isSelected = true;
        }
        else {
            this.isSelected = false;
        }
    };
    InboxmailComponent.prototype.SentBulkDelete = function () {
        var DeleteList = this.multipleDeleteContainer;
        if (Number(DeleteList.length) == 0) {
            alert("Please select at least one record to delete");
            return false;
        }
        this.SubmitBulkDeleteMail(this.__MailReletedObject).subscribe(function (result) {
            if (result.type == "success") {
                alert("Mail(s) moved into trash successfully");
                window.location.reload();
                // this.GetInboxEachRecords(1);
            }
            else {
                alert(result.msg);
            }
        }, function (err) {
            console.log(err);
        });
    };
    InboxmailComponent.prototype.SubmitBulkDeleteMail = function (__MailReletedObject) {
        return this.http.post(endpoint + "/PostInboxBulkDeleteMail/", JSON.stringify(__MailReletedObject), httpOptions).pipe();
    };
    InboxmailComponent = __decorate([
        Component({
            selector: 'app-inboxmail',
            templateUrl: './inboxmail.component.html',
            styleUrls: ['./inboxmail.component.css']
        }),
        __metadata("design:paramtypes", [DataService, HttpClient /*, private sanitizer: DomSanitizer*/, ActivatedRoute, Router])
    ], InboxmailComponent);
    return InboxmailComponent;
}());
export { InboxmailComponent };
//# sourceMappingURL=inboxmail.component.js.map