var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
var endpoint = 'https://localhost:44365/api/values';
var httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
var Custom = /** @class */ (function () {
    function Custom() {
    }
    return Custom;
}());
export { Custom };
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
        //testimage = require("../../../../wwwroot/SignatureAttacements/" + this.heroImage);
        this.signupdisplay = 'none';
        this.MasterID = 0;
        this.showsignature = false;
        this.theCheckbox = false;
        this.SignatureBody = "";
        this.ProgressBarStrip = 'none';
        this.completionPercent = 0;
        this.config = {
            editable: true,
            spellcheck: true,
            height: '15rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            // uploadUrl: endpoint + '/SignatureFileUpload', 
            translate: 'no'
            //customClasses: [
            //  {
            //    name: "quote",
            //    class: "quote",
            //  },
            //  {
            //    name: 'redText',
            //    class: 'redText'
            //  },
            //  {
            //    name: "titleText",
            //    class: "titleText",
            //    tag: "h1",
            //  },
            //]
        };
        this.configureData = { AccessWith: '', SmtpHost: '', SmtpPort: '', IncomingHost: '', IncomingPort: '', EmailID: '', Password: '', Signature: '', Reply: false, ReplyAll: false, Forward: false, Compose: false };
        this.error_AccessWith = 'none';
        this.error_SmtpHost = 'none';
        this.error_SmtpPort = 'none';
        this.error_IncomingHost = 'none';
        this.error_IncomingPort = 'none';
        this.error_EmailID = 'none';
        this.error_Password = 'none';
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.GetDefaultConfigDetails();
    };
    // GET RECORD WITH INTIAL LOADING
    HeaderComponent.prototype.GetDefaultConfigDetails = function () {
        var _this = this;
        var _MasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_MasterID))
            _MasterID = 0;
        this.http.get(endpoint + '/GetDefaultConfigDetails/' + _MasterID).subscribe(function (data) {
            _this.MasterID = data[0].masterID;
            _this.configureData.AccessWith = data[0].accessWith;
            _this.configureData.SmtpHost = data[0].smtpHost;
            _this.configureData.SmtpPort = data[0].smtpPort;
            _this.configureData.IncomingHost = data[0].incomingHost;
            _this.configureData.IncomingPort = data[0].incomingPort;
            _this.configureData.EmailID = data[0].emailID;
            //this.configureData.Password = data[0].password;
            _this.configureData.Signature = data[0].signature;
            _this.configureData.Reply = data[0].reply;
            _this.configureData.ReplyAll = data[0].replyAll;
            _this.configureData.Compose = data[0].compose;
            _this.configureData.Forward = data[0].forward;
            sessionStorage.setItem('currentUser', data[0].masterID);
           
            if (_this.configureData.Signature != "" && _this.configureData.Signature != "<br>") {
                _this.showsignature = true;
            }
        });
    };
    /*** DROPDOWN CHANGE EVENT  *****/
    HeaderComponent.prototype.onAccessChange = function (deviceValue) {
        if (deviceValue == "select") {
            this.error_AccessWith = 'block';
        }
        else {
            this.error_AccessWith = 'none';
        }
    };
    /*** VALIDATION START *****/
    HeaderComponent.prototype.focusOutFunction = function () {
        this.validateFields();
    };
    HeaderComponent.prototype.validateEmail = function (emailField) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField) == false) {
            return false;
        }
        return true;
    };
    HeaderComponent.prototype.validateFields = function () {
        var Result = true;
        var AccessWith = this.configureData.AccessWith;
        var SmtpHost = this.configureData.SmtpHost;
        var SmtpPort = this.configureData.SmtpPort;
        var IncomingHost = this.configureData.IncomingHost;
        var IncomingPort = this.configureData.IncomingPort;
        var EmailID = this.configureData.EmailID;
        var Password = this.configureData.Password;
        if (AccessWith == "") {
            this.error_AccessWith = 'block';
            Result = false;
        }
        else {
            this.error_AccessWith = 'none';
        }
        if (SmtpHost == "") {
            this.error_SmtpHost = 'block';
            Result = false;
        }
        else {
            this.error_SmtpHost = 'none';
        }
        if (SmtpPort == "") {
            this.error_SmtpPort = 'block';
            Result = false;
        }
        else {
            this.error_SmtpPort = 'none';
        }
        if (IncomingHost == "") {
            this.error_IncomingHost = 'block';
            Result = false;
        }
        else {
            this.error_IncomingHost = 'none';
        }
        if (IncomingPort == "") {
            this.error_IncomingPort = 'block';
            Result = false;
        }
        else {
            this.error_IncomingPort = 'none';
        }
        if (EmailID == "") {
            this.error_EmailID = 'block';
            Result = false;
        }
        else {
            Result = this.validateEmail(EmailID);
            if (!Result) {
                this.error_EmailID = 'block';
            }
            else {
                this.error_EmailID = 'none';
            }
        }
        if (Password == "") {
            this.error_Password = 'block';
            Result = false;
        }
        else {
            this.error_Password = 'none';
        }
        return Result;
    };
    /*** VALIDATION END *****/
    /*** START SAVE SUBMIT CLICK EVENT *****/
    HeaderComponent.prototype.CofigureSubmit = function () {
        var _this = this;
        var validateFields_fn = this.validateFields();
        if (!validateFields_fn) {
            return false;
        }
        this.SubmitCofigureDetails(this.configureData).subscribe(function (result) {
            if (result.type == "success") {
                _this.MasterID = result.anyid;
                sessionStorage.setItem('currentUser', _this.MasterID.toString());
            }
            else {
                _this.MasterID = 0;
                sessionStorage.setItem('currentUser', _this.MasterID.toString());
            }
            _this.closesignup();
            window.location.reload();
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.SubmitCofigureDetails = function (configureData) {
        return this.http.post(endpoint + "/PostConfigurationDetails/", JSON.stringify(configureData), httpOptions).pipe();
    };
    /*** END SAVE SUBMIT CLICK EVENT *****/
    /*** START RESET BUTTON CLICK EVENT *** */
    HeaderComponent.prototype.ResetSubmit = function (MasterID) {
        var _MasterID = 0;
        try {
            _MasterID = Number(sessionStorage.getItem('currentUser'));
        }
        catch (e) {
            _MasterID = 0;
        }
        this.http.get(endpoint + '/GetResetSubmit/' + _MasterID).subscribe(function (result) {
            if (result.type == "success") {
                alert(result.msg);
                window.location.reload();
            }
            else {
                alert(result.msg);
            }
        });
    };
    /*** END RESET BUTTON CLICK EVENT *** */
    HeaderComponent.prototype.openModal = function () {
        this.signupdisplay = 'block';
    };
    HeaderComponent.prototype.closesignup = function () {
        this.signupdisplay = 'none';
    };
    HeaderComponent.prototype.DisplaySignature = function (e) {
        this.showsignature = e.target.checked;
    };
    HeaderComponent.prototype.onSignatureChanged = function (event) {
        var _this = this;
        var fileGet = event.target.files[0];
        //console.log('size', fileGet.size);
        //console.log('type', fileGet.type);
        var sizefile = fileGet.size;
        // temp = temp + sizefile;
        var threeMb = 3 * 1024 * 1024;
        // var fivemb = 6 * 1024 * 1024;
        if (sizefile > threeMb) {
            //var cursize = parseFloat((event.files[0].size / 1048576)).toFixed(2);
            var cursize = (sizefile / 1048576).toFixed(2);
            alert("Your file size " + cursize + " MB. File size should not exceed 3 MB");
            // $(event).val('');
            return false;
        }
        //if (temp > fivemb) {
        //  Message({
        //    "Msg": "Yor total file size is greater than 6 MB. File size should not exceed 6MB", "Type": msg_Info
        //  });
        //  $(this).val('');
        //  temp = temp - sizefile;
        //  return false;
        //}
        var validFiles = ["jpg", "jpeg", "png"];
        var source = fileGet.name;
        var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
        for (var i = 0; i < validFiles.length; i++) {
            if (validFiles[i] == ext) {
                break;
            }
        }
        if (i >= validFiles.length) {
            alert("Only following  file extensions are allowed : " + validFiles.join(","));
            //this.ComposeMailData.Attachment = "";
            //$(event).val('');
            //$('#filetxt').val('');
            return false;
        }
        var file = event.target.files[0];
        //this.ComposeMailData.Attachment = event.target.files[0];
        this.selectedFile = event.target.files[0];
        // this.http is the injected HttpClient
        var uploadData = new FormData();
        uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
        this.ProgressBarStrip = 'block';
        this.http.post(endpoint + '/SignatureFileUpload', uploadData, {
            reportProgress: true,
            observe: 'events'
        })
            .subscribe(function (event) {
            // debugger;
            if (event.type === HttpEventType.UploadProgress) {
                _this.completionPercent = Math.round(100 * event.loaded / event.total);
            }
            else if (event.type === HttpEventType.Response) {
                _this.ProgressBarStrip = 'none';
                _this.completionPercent = 0;
                try {
                 
                    // if (event.body.action == "success") {
                    var newfilename_ = "";
                    for (var i in event.body) {
                        newfilename_ = event.body[i];
                    }
                    //var testimg = "../../SignatureAttacements/" + newfilename_ ;
                    //this.heroImage = require(testimg);
                    _this.configureData.Signature = _this.configureData.Signature + "<br>" + "<img src='/SignatureAttacements/" + newfilename_ + "' />";
                    //this.configureData.Signature = "<img src='https://localhost:44325/SignatureAttacements/" + newfilename_ + "' />"
                }
                catch (e) {
                }
            }
        });
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "configureData", void 0);
    HeaderComponent = __decorate([
        Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, ActivatedRoute, Router])
    ], HeaderComponent);
    return HeaderComponent;
}());
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map
