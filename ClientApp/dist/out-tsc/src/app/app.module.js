var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { SendmailComponent } from './sendmail/sendmail.component';
import { InboxmailComponent } from './inboxmail/inboxmail.component';
import { AppRoutingModule } from './app-routing.module';
import { ComposemailComponent } from './composemail/composemail.component';
import { DraftmailComponent } from './draftmail/draftmail.component';
import { TrashmailComponent } from './trashmail/trashmail.component';
import { DataService } from './data.service';
import { OutboxmailComponent } from './outboxmail/outboxmail.component';
import { StarredmailComponent } from './starredmail/starredmail.component';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                SidebarComponent,
                HeaderComponent,
                SendmailComponent,
                InboxmailComponent,
                ComposemailComponent,
                DraftmailComponent,
                TrashmailComponent,
                OutboxmailComponent,
                StarredmailComponent
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                AppRoutingModule,
                AngularEditorModule,
            ],
            providers: [DataService],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map