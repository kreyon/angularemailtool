var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(_data, http) {
        this._data = _data;
        this.http = http;
        this._mailcounts = { inboxmailcount: 0, sentmailcount: 0, draftmailcount: 0, outboxmailcount: 0, starredmailcount: 0, trashmailcount: 0 };
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._data._mailcounts.subscribe(function (res) { return _this._mailcounts = res; });
        this._data.changeinboxcounts(this._mailcounts);
        var _GlobalMasterID = Number(sessionStorage.getItem('currentUser'));
        if (isNaN(_GlobalMasterID))
            _GlobalMasterID = 0;
        this.http.get('https://localhost:44365/api/values/GetTabCount/' + _GlobalMasterID).subscribe(function (data) {
            _this._mailcounts.inboxmailcount = data[0].inboxCount;
            _this._mailcounts.sentmailcount = data[0].sentCount;
            _this._mailcounts.draftmailcount = data[0].draftCount;
            _this._mailcounts.outboxmailcount = data[0].outboxCount;
            _this._mailcounts.starredmailcount = data[0].starredCount;
            _this._mailcounts.trashmailcount = data[0].trashCount;
        });
    };
    SidebarComponent = __decorate([
        Component({
            selector: 'app-sidebar',
            templateUrl: './sidebar.component.html',
            styleUrls: ['./sidebar.component.css']
        }),
        __metadata("design:paramtypes", [DataService, HttpClient])
    ], SidebarComponent);
    return SidebarComponent;
}());
export { SidebarComponent };
//# sourceMappingURL=sidebar.component.js.map