﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AngularEmailTool.Models;
using S22.Imap;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Xml;

namespace AngularEmailTool.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        //// GET api/values
        //[HttpGet]
        //public JsonResult Get()
        //{
        //    EmailToolContext db = new EmailToolContext();
        //    // MailData EmailData = new MailData();
        //    List<MailData> EmailData = new List<MailData>();
        //    var record = db.EmailConfig.FirstOrDefault();
        //    string resp = "";
        //    StringBuilder ss = new StringBuilder();
        //    using (S22.Pop3.Pop3Client popclient = new S22.Pop3.Pop3Client("pop.asia.secureserver.net", 995, "mail@kreyonmedia.com", "Kreyon@123", S22.Pop3.AuthMethod.Login, true, null))
        //    {


        //        int counter = 0;
        //        int mailcount = 0;
        //        int pagecount = 0;
        //        int skip = 0;
        //        string search = "";


        //        var total = popclient.GetMessageNumbers().Count();
        //        if (total > 0)
        //        {

        //            var RecordSkip = (skip - 1) * 10;
        //            uint[] mylist;
        //            System.Net.Mail.MailMessage[] messages = null;
        //            //S22.Pop3.MessageInfo[] emailint = popclient.GetStatus();
        //            if (!search.Equals(""))
        //            {
        //                mylist = popclient.GetMessageNumbers().OrderByDescending(c => c).ToArray();
        //                messages = popclient.GetMessages(mylist);
        //                messages = messages.Where(s => ((string.IsNullOrEmpty(s.From.User) ? "" : s.From.User).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.Subject) ? "" : s.Subject).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.From.DisplayName) ? "" : s.From.DisplayName).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.From.Address) ? "" : s.From.Address).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.Body) ? "" : s.Body).ToLower().Trim().Contains(search.ToLower().Trim()))).ToArray();
        //                pagecount = messages.Count();
        //            }
        //            else
        //            {
        //                mylist = popclient.GetMessageNumbers().OrderByDescending(c => c).Skip(RecordSkip).Take(10).ToArray();
        //                messages = popclient.GetMessages(mylist);
        //                pagecount = total;
        //            }


        //            System.Text.StringBuilder str = new System.Text.StringBuilder();
        //            List<string> maildatecollection = new List<string>();


        //            if (messages.Count() != 0)
        //            {
        //                foreach (var item in messages)
        //                {

        //                    //  if (!delid.Contains(item.Headers["Date"]))
        //                    //  {



        //                    item.IsBodyHtml = true;
        //                    string tos = "";
        //                    string ccmails = "";
        //                    string bccmails = "";
        //                    string tomails = "";
        //                    tos += item.From.Address;
        //                    if (item.To.Count > 0)
        //                    {
        //                        foreach (var ts in item.To)
        //                        {
        //                            if (!record.Username.Contains(ts.Address))
        //                            {
        //                                tos += ts.Address + ",";
        //                            }
        //                            tomails += ts.Address + ",";

        //                        }
        //                    }

        //                    if (item.CC.Count > 0)
        //                    {
        //                        foreach (var ts in item.CC)
        //                        {
        //                            if (!record.Username.Contains(ts.Address))
        //                            {
        //                                tos += ts.Address + ",";
        //                            }
        //                            ccmails += ts.Address + ",";
        //                        }
        //                    }

        //                    if (item.Bcc.Count > 0)
        //                    {
        //                        foreach (var ts in item.Bcc)
        //                        {

        //                            bccmails += ts.Address + ",";
        //                        }
        //                    }





        //                    List<string> attch = new List<string>();
        //                    List<string> attchname = new List<string>();
        //                    byte[] allBytes = null;
        //                    if (item.Attachments.Count > 0)
        //                    {
        //                        foreach (var at in item.Attachments)
        //                        {
        //                            int size = 1;
        //                            try
        //                            {
        //                                allBytes = new byte[at.ContentStream.Length];
        //                                int bytesRead = at.ContentStream.Read(allBytes, 0, (int)at.ContentStream.Length);

        //                                var result = Convert.ToBase64String(allBytes);
        //                                //size = (int)at.ContentStream.ReadByte();
        //                                attch.Add(result);
        //                                attchname.Add(at.Name);


        //                            }
        //                            catch { size = 1; }





        //                            //  string destinationFile = @"C:\\Path\\" + at.Name;
        //                            // BinaryWriter writer = new BinaryWriter(new FileStream(destinationFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None));
        //                            // writer.Write(allBytes);
        //                            //writer.Close();
        //                        }
        //                    }
        //                    //str.Append("<div id='attach' style='display:none;'>" + attch + "</div>");

        //                    string subjects = "";
        //                    subjects = (string.IsNullOrEmpty(item.Subject) ? "(no subject)" : item.Subject);
        //                    string displayname = "";
        //                    try
        //                    {
        //                        if (item.From != null)
        //                        {
        //                            displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : (item.From.DisplayName).Replace("<notification+", ""));
        //                        }
        //                        else { displayname = subjects; }
        //                    }
        //                    catch { displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : item.From.DisplayName); }
        //                    string depart = "";
        //                    string fromadd = "";
        //                    try
        //                    {
        //                        if (item.From != null)
        //                        {
        //                            depart = (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) + " (" + (string.IsNullOrEmpty(item.From.Address) ? "-" : (item.From.Address)) + ")";
        //                            fromadd = item.From.Address;

        //                        }
        //                        else { depart = "-"; }
        //                    }
        //                    catch { depart = subjects; }


        //                    if (counter == 0)
        //                    {
        //                        ss.Append("<div class='nwinb_read' id='first'  (click)='GetInboxEmaildetails()'>");
        //                    }
        //                    else
        //                    {
        //                        ss.Append("<div class='nwinb_read'  (click)='GetInboxEmaildetails()'>");
        //                    }
        //                    string[] splitdate = null;
        //                    string dat = item.Headers["Date"];

        //                    if (dat.Contains("+"))
        //                    {
        //                        splitdate = dat.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
        //                    }
        //                    else
        //                    {
        //                        splitdate = dat.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
        //                    }
        //                    var Date = splitdate[0].Split(',');
        //                    DateTime TodayDate = DateTime.UtcNow;
        //                    DateTime testdate;
        //                    if (Date.Length == 1)
        //                    {
        //                        testdate = Convert.ToDateTime(Date[0]);
        //                    }
        //                    else
        //                    {
        //                        testdate = Convert.ToDateTime(Date[1]);
        //                    }


        //                    string HowManyHours = Math.Round(TodayDate.Subtract(testdate).TotalHours, 2).ToString();
        //                    string HowManyMinutes = Math.Round(TodayDate.Subtract(testdate).TotalMinutes, 2).ToString();
        //                    string[] dt = HowManyHours.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
        //                    string[] dt1 = HowManyMinutes.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
        //                    string FinalDate = (Convert.ToDateTime(testdate).Date == DateTime.UtcNow.Date ? (dt[0] != "0" ? dt[0] + " hours ago" : dt1[0] + " minutes ago") : (Convert.ToDateTime(testdate)).Date == DateTime.UtcNow.AddDays(-1).Date ? "Yesterday " + Convert.ToDateTime(testdate).ToString("dd MMM yyyy hh : mm tt") : Convert.ToDateTime(testdate).ToString("dd MMM yyyy hh : mm tt"));
        //                    var bodyhtml = item.Body;
        //                    bodyhtml = bodyhtml.Replace("\r\n", "<br>");



        //                    EmailData.Add(new MailData
        //                    {
        //                        Id = item.Headers["Date"],
        //                        Date = FinalDate,
        //                        Body = item.Body,
        //                        Cc = ccmails,
        //                        Bcc = bccmails,
        //                        Attachment_Name = attchname.ToArray(),
        //                        // Attachment = attch.ToArray(),
        //                        Subject = subjects,
        //                        To = tomails,
        //                        Reply_All_Mails = tos,
        //                        Depart_Name = depart,
        //                        Display_Name = displayname,
        //                        From = fromadd
        //                    });


        //                    //ss.Append("<div class='nwinb_chkbox'>");

        //                    //ss.Append("<div class='cbdiv'></div><div class='thisemailbody ng-hide'  style='display:none;'>" + (string.IsNullOrEmpty(bodyhtml) ? "-" : (bodyhtml)) + "</div>");
        //                    //ss.Append("<div class='mysub ng-hide'  style='display:none;'>" + subjects + "</div>");
        //                    //ss.Append("<div class='attach ng-hide'   style='display:none;'>" + attch + "</div>");
        //                    //ss.Append("<div class='attachname ng-hide'   style='display:none;'>" + attchname + "</div>");
        //                    //ss.Append("<div class='uid ng-hide'  style='display:none;'>" + item.Headers["Date"] + "</div>");
        //                    //ss.Append("<div class='fromad ng-hide'  style='display:none;'>" + fromadd + "</div>");
        //                    //ss.Append("<div class='replyallmails ng-hide'  style='display:none;'>" + tos + "</div>");
        //                    //ss.Append("<div class='ccmails ng-hide'  style='display:none;'>" + ccmails + "</div>");
        //                    //ss.Append("<div class='bccmails ng-hide'  style='display:none;'>" + bccmails + "</div>");
        //                    //ss.Append("<div class='tomails ng-hide'  style='display:none;'>" + tomails + "</div>");
        //                    //ss.Append("<div class='maildate ng-hide'  style='display:none;'>" + FinalDate + "</div>");
        //                    //ss.Append("<div class='mytos ng-hide'  style='display:none;'>" + depart + "</div></div>");


        //                    //ss.Append("<div class='mail-list'>");

        //                    //ss.Append("<div class='form-check check_inptblock chk_inbox'>");
        //                    //ss.Append("     <div class='checkbox'>                                                                                                     ");
        //                    //ss.Append("         <label>                                                                                                                ");
        //                    //ss.Append("             <input type='checkbox' name='inboxcheck' value='" + item.Headers["Date"] + "' /><span class='cr'><i class='cr-icon mdi mdi-check'></i></span></label>      ");
        //                    //ss.Append("     </div>                                                                                                                     ");
        //                    //ss.Append(" </div>                                                                                                                         ");

        //                    //ss.Append(" <div class='details star-inptbox' onclick=\"SaveStar(this,'inbox','" + item.Headers["Date"] + "')\">                                                                                             ");
        //                    //ss.Append("     <i class='mdi mdi-star-outline'></i>                                                                                       ");
        //                    //ss.Append(" </div>                                                                                                                         ");

        //                    //ss.Append(" <div class='content'>                                                                                                          ");
        //                    //ss.Append("     <p class='sender-name' >" + displayname + "</p>                                        ");
        //                    //ss.Append("     <p class='message_text nwinb_mailcont'>" + subjects + "</p>                                                                         ");
        //                    //ss.Append(" </div>                                                                                                                         ");
        //                    //ss.Append("                                                                                                                                ");
        //                    //ss.Append(" </div> </div>");
        //                    maildatecollection.Add(item.Headers["Date"]);
        //                    counter++;

        //                    mailcount++;


        //                    // if (item.Attachments.Count() > 0)
        //                    //  {
        //                    //     str.Append("<div class='nwinb_mailattach'></div>");
        //                    //  }
        //                    //string tempdate = item.Headers["Date"].ToString();
        //                    //var dtt = Convert.ToDateTime(tempdate.Substring(0, tempdate.LastIndexOf(':') + 3).Trim());
        //                    //if (DateTime.UtcNow.Date == dtt.Date)
        //                    //{ tempdate = String.Format("{0:t}", dtt.Date); }
        //                    //else
        //                    //{
        //                    //    tempdate = String.Format("{MMM dd}", dtt.Date);
        //                    //}
        //                    //str.Append("<div class='nwinb_mailtime'>" + tempdate + "</div></div>");
        //                    //  }
        //                }


        //            }
        //        }
        //    }

        //    return new JsonResult(EmailData);

        //    // return new string[] { JsonConvert.SerializeObject(EmailData) };
        //}

        // GET api/values/5

        [HttpGet("{skip}/{search}/{masterid}")]
        public JsonResult GetInboxRecords(int skip, string search, int masterid)
        {
            int MasterID = masterid;

            List<MailData> EmailData = new List<MailData>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        // HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

            }

            try
            {

                EmailToolContext db = new EmailToolContext();

                var record = (from rec in db.EmailConfig.AsEnumerable()
                              where rec.Id == MasterID && !(rec.IsDeleted ?? false)
                              select rec).LastOrDefault();

                if (record == null)
                {
                    return new JsonResult("nocre");
                }

                // HttpContext.Current.Session["ConfigId"] = record.Id;

                var delid = (from re in db.TrashMailsTable.AsEnumerable()
                             where re.UserId == record.Id && !string.IsNullOrEmpty(re.InboxMailThreadId)
                             select re.InboxMailThreadId).ToArray();

                //var StarId = (from re in db.StarredEmailTable.AsEnumerable()
                //              where re.UserId == record.Id && re.IsInbox == true
                //              select re.EmailId).ToArray();

                var StarredMailRecords = (from rec in db.StarredEmail
                                          where !string.IsNullOrEmpty(rec.InboxId)
                                          select rec).Select(a => new { TableID = a.Id, InboxID = a.InboxId });

                // THIS IS ENCRYPTING PASSWORD
                String Decryptpassword = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);


                string pass = record.Password;

                string resp = "";

                int counter = 0;
                int mailcount = 0;
                int pagecount = 0;

                StringBuilder ss = new StringBuilder();
                double SystemTimezone = 330;// Convert.ToDouble(TimeZoneInfo.GetSystemTimeZones());
                // for imap configuretion
                if (record.Access == "imap")
                {
                    using (ImapClient Iclient = new ImapClient(record.Host, Convert.ToInt32(record.Port), record.Username, Decryptpassword, S22.Imap.AuthMethod.Login, true, null))
                    {

                        IEnumerable<uint> uids = Iclient.Search(S22.Imap.SearchCondition.All(), "INBOX");


                        var total = uids.Count();
                        if (uids.Count() > 0)
                        {
                            IEnumerable<System.Net.Mail.MailMessage> Iunseenmessages = null;

                            var RecordSkip = (skip - 1) * 10;


                            try
                            {
                                if (!search.Equals("blank"))
                                {
                                    Iunseenmessages = Iclient.GetMessages(uids.OrderByDescending(u => u), false, "INBOX");
                                    Iunseenmessages = Iunseenmessages.Where(s => ((string.IsNullOrEmpty(s.From.User) ? "" : s.From.User).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.Subject) ? "" : s.Subject).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.From.DisplayName) ? "" : s.From.DisplayName).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.From.Address) ? "" : s.From.Address).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.Body) ? "" : s.Body).ToLower().Trim().Contains(search.ToLower().Trim())));
                                    pagecount = Iunseenmessages.Count();
                                }
                                else
                                {

                                    Iunseenmessages = Iclient.GetMessages(uids.OrderByDescending(u => u).Skip(((int)RecordSkip)).Take(10), false, "INBOX");
                                    pagecount = total;
                                }


                            }
                            catch { }
                            var Imessages = Iunseenmessages.OrderByDescending(d => Convert.ToDateTime(d.Headers["date"].Substring(0, d.Headers["date"].LastIndexOf(':') + 3).Trim()));
                            int cnt = Imessages.Count();
                            int RecordShow = 10;
                            if (cnt > 0)
                            {

                                List<string> maildatecollection = new List<string>();
                                System.Text.StringBuilder str = new System.Text.StringBuilder();

                                foreach (var item in Imessages)
                                {

                                    if (!delid.Contains(item.Headers["Date"]))
                                    {
                                        List<string> ComibinedReplyAll = new List<string>();
                                        List<string> ComibinedCc = new List<string>();
                                        List<string> ComibinedBcc = new List<string>();
                                        List<string> ComibinedTo = new List<string>();

                                        ComibinedReplyAll.Add(item.From.Address);

                                        if (item.To.Count > 0)
                                        {
                                            foreach (var ts in item.To)
                                            {
                                                if (!record.Username.Contains(ts.Address))
                                                    ComibinedReplyAll.Add(ts.Address);

                                                ComibinedTo.Add(ts.Address);
                                            }
                                        }

                                        if (item.CC.Count > 0)
                                        {
                                            foreach (var ts in item.CC)
                                            {
                                                if (!record.Username.Contains(ts.Address))
                                                    ComibinedReplyAll.Add(ts.Address);

                                                ComibinedCc.Add(ts.Address);
                                            }
                                        }

                                        if (item.Bcc.Count > 0)
                                        {
                                            foreach (var ts in item.Bcc)
                                            {
                                                ComibinedReplyAll.Add(ts.Address);
                                                ComibinedBcc.Add(ts.Address);
                                            }
                                        }

                                        List<AttachmentRecord> ATT = new List<AttachmentRecord>();

                                        byte[] allBytes = null;
                                        if (item.Attachments.Count > 0)
                                        {
                                            foreach (var at in item.Attachments)
                                            {
                                                int size = 1;
                                                try
                                                {
                                                    allBytes = new byte[at.ContentStream.Length];
                                                    int bytesRead = at.ContentStream.Read(allBytes, 0, (int)at.ContentStream.Length);

                                                    var result = Convert.ToBase64String(allBytes);


                                                    ATT.Add(new AttachmentRecord { Name = at.Name, Value = result });


                                                }
                                                catch { size = 1; }

                                            }
                                        }


                                        string subjects = "";
                                        subjects = (string.IsNullOrEmpty(item.Subject) ? "(no subject)" : item.Subject);
                                        string displayname = "";
                                        try
                                        {
                                            if (item.From != null)
                                            {
                                                displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : (item.From.DisplayName).Replace("<notification+", ""));
                                            }
                                            else { displayname = subjects; }
                                        }
                                        catch { displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : item.From.DisplayName); }
                                        string depart = "";
                                        string fromadd = "";
                                        try
                                        {
                                            if (item.From != null)
                                            {

                                                depart = (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) + " (" + (string.IsNullOrEmpty(item.From.Address) ? "-" : (item.From.Address)) + ")";
                                                fromadd = item.From.Address;
                                            }
                                            else { depart = "-"; }
                                        }
                                        catch { depart = subjects; }




                                        string[] splitdate = null;
                                        string dat = item.Headers["Date"];

                                        if (dat.Contains("+"))
                                        {
                                            splitdate = dat.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
                                        }
                                        else
                                        {
                                            splitdate = dat.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                        }
                                        var Date = splitdate[0].Split(',');
                                        DateTime TodayDate = DateTime.UtcNow.AddMinutes(SystemTimezone);
                                        DateTime testdate;
                                        if (Date.Length == 1)
                                        {
                                            testdate = Convert.ToDateTime(Date[0]);
                                        }
                                        else
                                        {
                                            testdate = Convert.ToDateTime(Date[1]);
                                        }
                                        string HowManyHours = Math.Round(TodayDate.Subtract(testdate).TotalHours, 2).ToString();
                                        string HowManyMinutes = Math.Round(TodayDate.Subtract(testdate).TotalMinutes, 2).ToString();
                                        string[] dt = HowManyHours.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                        string[] dt1 = HowManyMinutes.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                        string FinalDate = (Convert.ToDateTime(testdate).Date == DateTime.UtcNow.Date ? (dt[0] != "0" ? dt[0] + " hours ago" : dt1[0] + " minutes ago") : (Convert.ToDateTime(testdate)).Date == DateTime.UtcNow.AddDays(-1).Date ? "Yesterday " + Convert.ToDateTime(testdate).ToString("hh:mm tt") : Convert.ToDateTime(testdate).ToString("dd MMM yyyy hh:mm tt"));
                                        var bodyhtml = item.Body;
                                        bodyhtml = bodyhtml.Replace("\r\n", "<br>");
                                        var PageLinks = total % RecordShow == 0 ? (total / RecordShow) : (total / RecordShow) + 1;

                                        var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.InboxID.ToLower().Trim().Equals(item.Headers["Date"].ToLower().Trim()));

                                        EmailData.Add(new MailData
                                        {
                                            Id = item.Headers["Date"],
                                            Date = FinalDate,
                                            Body = bodyhtml,
                                            Cc = string.Join(",", ComibinedCc.Select(s => s)), //ccmails,
                                            Bcc = string.Join(",", ComibinedBcc.Select(s => s)), //bccmails,
                                            _AttachmentRecord = ATT,
                                            Subject = subjects,
                                            To = string.Join(",", ComibinedTo.Select(s => s)), //tomails,
                                            Reply_All_Mails = string.Join(",", ComibinedReplyAll.Select(s => s)), //tos,
                                            Depart_Name = depart,
                                            Display_Name = displayname,
                                            From = fromadd,
                                            skipnum = PageLinks,
                                            searchval = search,
                                            starid = StarIdRecord == null ? 0 : StarIdRecord.TableID
                                        });

                                        maildatecollection.Add(item.Headers["Date"]);
                                        counter++;

                                        mailcount++;


                                    }
                                }
                                // if mail count is less then 10

                                if (search.Equals("blank"))
                                {
                                    if (mailcount != 10)
                                    {


                                        IEnumerable<System.Net.Mail.MailMessage> Iunseenmessages2 = null;

                                        try
                                        {

                                            int DeletedRecordsCountPlusTenMore = delid.Count() + 10;

                                            Iunseenmessages2 = Iclient.GetMessages(uids.OrderByDescending(u => u).Skip(((int)RecordSkip)).Take(DeletedRecordsCountPlusTenMore), false, "INBOX");
                                        }
                                        catch { }


                                        var Imessages2 = Iunseenmessages.OrderByDescending(d => Convert.ToDateTime(d.Headers["date"].Substring(0, d.Headers["date"].LastIndexOf(':') + 3).Trim()));
                                        int cnt2 = Imessages.Count();
                                        if (cnt > 0)
                                        {

                                            foreach (var item in Imessages2)
                                            {

                                                if (!delid.Contains(item.Headers["Date"]))
                                                {
                                                    if (!maildatecollection.Contains(item.Headers["Date"]))
                                                    {
                                                        List<string> ComibinedReplyAll = new List<string>();
                                                        List<string> ComibinedCc = new List<string>();
                                                        List<string> ComibinedBcc = new List<string>();
                                                        List<string> ComibinedTo = new List<string>();

                                                        ComibinedReplyAll.Add(item.From.Address);

                                                        if (item.To.Count > 0)
                                                        {
                                                            foreach (var ts in item.To)
                                                            {
                                                                if (!record.Username.Contains(ts.Address))
                                                                    ComibinedReplyAll.Add(ts.Address);

                                                                ComibinedTo.Add(ts.Address);
                                                            }
                                                        }

                                                        if (item.CC.Count > 0)
                                                        {
                                                            foreach (var ts in item.CC)
                                                            {
                                                                if (!record.Username.Contains(ts.Address))
                                                                    ComibinedReplyAll.Add(ts.Address);

                                                                ComibinedCc.Add(ts.Address);
                                                            }
                                                        }

                                                        if (item.Bcc.Count > 0)
                                                        {
                                                            foreach (var ts in item.Bcc)
                                                            {
                                                                ComibinedReplyAll.Add(ts.Address);
                                                                ComibinedBcc.Add(ts.Address);
                                                            }
                                                        }

                                                        List<AttachmentRecord> ATT = new List<AttachmentRecord>();
                                                        byte[] allBytes = null;
                                                        if (item.Attachments.Count > 0)
                                                        {
                                                            foreach (var at in item.Attachments)
                                                            {
                                                                int size = 1;
                                                                try
                                                                {
                                                                    allBytes = new byte[at.ContentStream.Length];
                                                                    int bytesRead = at.ContentStream.Read(allBytes, 0, (int)at.ContentStream.Length);

                                                                    var result = Convert.ToBase64String(allBytes);

                                                                    ATT.Add(new AttachmentRecord { Name = at.Name, Value = result });

                                                                }
                                                                catch { size = 1; }
                                                            }
                                                        }


                                                        string subjects = "";
                                                        subjects = (string.IsNullOrEmpty(item.Subject) ? "(no subject)" : item.Subject);
                                                        string displayname = "";
                                                        try
                                                        {
                                                            if (item.From != null)
                                                            {
                                                                displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : (item.From.DisplayName).Replace("<notification+", ""));
                                                            }
                                                            else { displayname = subjects; }
                                                        }
                                                        catch { displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : item.From.DisplayName); }
                                                        string depart = "";
                                                        string fromadd = "";
                                                        try
                                                        {
                                                            if (item.From != null)
                                                            {

                                                                depart = (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) + " (" + (string.IsNullOrEmpty(item.From.Address) ? "-" : (item.From.Address)) + ")";
                                                                fromadd = item.From.Address;
                                                            }
                                                            else { depart = "-"; }
                                                        }
                                                        catch { depart = subjects; }




                                                        string[] splitdate = null;
                                                        string dat = item.Headers["Date"];

                                                        if (dat.Contains("+"))
                                                        {
                                                            splitdate = dat.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
                                                        }
                                                        else
                                                        {
                                                            splitdate = dat.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                                        }
                                                        var Date = splitdate[0].Split(',');
                                                        DateTime TodayDate = DateTime.UtcNow.AddMinutes(SystemTimezone);
                                                        DateTime testdate;
                                                        if (Date.Length == 1)
                                                        {
                                                            testdate = Convert.ToDateTime(Date[0]);
                                                        }
                                                        else
                                                        {
                                                            testdate = Convert.ToDateTime(Date[1]);
                                                        }
                                                        string HowManyHours = Math.Round(TodayDate.Subtract(testdate).TotalHours, 2).ToString();
                                                        string HowManyMinutes = Math.Round(TodayDate.Subtract(testdate).TotalMinutes, 2).ToString();
                                                        string[] dt = HowManyHours.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                                        string[] dt1 = HowManyMinutes.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                                        string FinalDate = (Convert.ToDateTime(testdate).Date == DateTime.UtcNow.Date ? (dt[0] != "0" ? dt[0] + " hours ago" : dt1[0] + " minutes ago") : (Convert.ToDateTime(testdate)).Date == DateTime.UtcNow.AddDays(-1).Date ? "Yesterday " + Convert.ToDateTime(testdate).ToString("hh:mm tt") : Convert.ToDateTime(testdate).ToString("dd MMM yyyy hh:mm tt"));
                                                        var bodyhtml = item.Body;
                                                        bodyhtml = bodyhtml.Replace("\r\n", "<br>");
                                                        var PageLinks = total % RecordShow == 0 ? (total / RecordShow) : (total / RecordShow) + 1;

                                                        var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.InboxID.ToLower().Trim().Equals(item.Headers["Date"].ToLower().Trim()));

                                                        EmailData.Add(new MailData
                                                        {
                                                            Id = item.Headers["Date"],
                                                            Date = FinalDate,
                                                            Body = bodyhtml,
                                                            Cc = string.Join(",", ComibinedCc.Select(s => s)), //ccmails,
                                                            Bcc = string.Join(",", ComibinedBcc.Select(s => s)), //bccmails,
                                                            _AttachmentRecord = ATT,
                                                            Subject = subjects,
                                                            To = string.Join(",", ComibinedTo.Select(s => s)), //tomails,
                                                            Reply_All_Mails = string.Join(",", ComibinedReplyAll.Select(s => s)), //tos,
                                                            Depart_Name = depart,
                                                            Display_Name = displayname,
                                                            From = fromadd,
                                                            skipnum = PageLinks,
                                                            searchval = search,
                                                            starid = StarIdRecord == null ? 0 : StarIdRecord.TableID

                                                        });
                                                        maildatecollection.Add(item.Headers["Date"]);
                                                        counter++;

                                                        mailcount++;
                                                        if (mailcount == 10)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }



                                return new JsonResult(EmailData);
                            }
                            else { return new JsonResult("nomessage"); }
                        }
                        else { return new JsonResult("nomessage"); }

                    }
                }
                // for pop configuration
                else if (record.Access == "pop")
                {
                    using (S22.Pop3.Pop3Client popclient = new S22.Pop3.Pop3Client(record.Host, Convert.ToInt32(record.Port), record.Username, Decryptpassword, S22.Pop3.AuthMethod.Login, true, null))
                    {

                        var total = popclient.GetMessageNumbers().Count();
                        if (total > 0)
                        {

                            var RecordSkip = (skip - 1) * 10;
                            uint[] mylist;
                            System.Net.Mail.MailMessage[] messages = null;

                            if (!search.Equals("blank"))
                            {
                                mylist = popclient.GetMessageNumbers().OrderByDescending(c => c).ToArray();
                                messages = popclient.GetMessages(mylist);
                                messages = messages.Where(s => ((string.IsNullOrEmpty(s.From.User) ? "" : s.From.User).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.Subject) ? "" : s.Subject).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.From.DisplayName) ? "" : s.From.DisplayName).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.From.Address) ? "" : s.From.Address).ToLower().Trim().Contains(search.ToLower().Trim())) || ((string.IsNullOrEmpty(s.Body) ? "" : s.Body).ToLower().Trim().Contains(search.ToLower().Trim()))).ToArray();
                                pagecount = messages.Count();
                            }
                            else
                            {
                                mylist = popclient.GetMessageNumbers().OrderByDescending(c => c).Skip(RecordSkip).Take(10).ToArray();
                                messages = popclient.GetMessages(mylist);
                                pagecount = total;
                            }

                            int RecordShow = 10;
                            var PageLinks = pagecount % RecordShow == 0 ? (pagecount / RecordShow) : (pagecount / RecordShow) + 1;
                            System.Text.StringBuilder str = new System.Text.StringBuilder();
                            List<string> maildatecollection = new List<string>();
                            if (messages.Count() != 0)
                            {
                                foreach (var item in messages)
                                {

                                    if (!delid.Contains(item.Headers["Date"]))
                                    {
                                        item.IsBodyHtml = true;

                                        List<string> ComibinedReplyAll = new List<string>();
                                        List<string> ComibinedCc = new List<string>();
                                        List<string> ComibinedBcc = new List<string>();
                                        List<string> ComibinedTo = new List<string>();

                                        ComibinedReplyAll.Add(item.From.Address);

                                        if (item.To.Count > 0)
                                        {
                                            foreach (var ts in item.To)
                                            {
                                                if (!record.Username.Contains(ts.Address))
                                                    ComibinedReplyAll.Add(ts.Address);

                                                ComibinedTo.Add(ts.Address);
                                            }
                                        }

                                        if (item.CC.Count > 0)
                                        {
                                            foreach (var ts in item.CC)
                                            {
                                                if (!record.Username.Contains(ts.Address))
                                                    ComibinedReplyAll.Add(ts.Address);

                                                ComibinedCc.Add(ts.Address);
                                            }
                                        }

                                        if (item.Bcc.Count > 0)
                                        {
                                            foreach (var ts in item.Bcc)
                                            {
                                                ComibinedReplyAll.Add(ts.Address);
                                                ComibinedBcc.Add(ts.Address);
                                            }
                                        }

                                        List<AttachmentRecord> ATT = new List<AttachmentRecord>();

                                        byte[] allBytes = null;
                                        if (item.Attachments.Count > 0)
                                        {
                                            foreach (var at in item.Attachments)
                                            {
                                                int size = 1;
                                                try
                                                {
                                                    allBytes = new byte[at.ContentStream.Length];
                                                    int bytesRead = at.ContentStream.Read(allBytes, 0, (int)at.ContentStream.Length);

                                                    var result = Convert.ToBase64String(allBytes);

                                                    ATT.Add(new AttachmentRecord { Name = at.Name, Value = result });


                                                }
                                                catch { size = 1; }






                                            }
                                        }


                                        string subjects = "";
                                        subjects = (string.IsNullOrEmpty(item.Subject) ? "(no subject)" : item.Subject);
                                        string displayname = "";
                                        try
                                        {
                                            if (item.From != null)
                                            {
                                                displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : (item.From.DisplayName).Replace("<notification+", ""));
                                            }
                                            else { displayname = subjects; }
                                        }
                                        catch { displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : item.From.DisplayName); }
                                        string depart = "";
                                        string fromadd = "";
                                        try
                                        {
                                            if (item.From != null)
                                            {
                                                depart = (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) + " (" + (string.IsNullOrEmpty(item.From.Address) ? "-" : (item.From.Address)) + ")";
                                                fromadd = item.From.Address;

                                            }
                                            else { depart = "-"; }
                                        }
                                        catch { depart = subjects; }



                                        string[] splitdate = null;
                                        string dat = item.Headers["Date"];

                                        if (dat.Contains("+"))
                                        {
                                            splitdate = dat.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
                                        }
                                        else
                                        {
                                            splitdate = dat.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                        }
                                        var Date = splitdate[0].Split(',');
                                        DateTime TodayDate = DateTime.UtcNow.AddMinutes(SystemTimezone);
                                        DateTime testdate;
                                        if (Date.Length == 1)
                                        {
                                            testdate = Convert.ToDateTime(Date[0]);
                                        }
                                        else
                                        {
                                            testdate = Convert.ToDateTime(Date[1]);
                                        }


                                        string HowManyHours = Math.Round(TodayDate.Subtract(testdate).TotalHours, 2).ToString();
                                        string HowManyMinutes = Math.Round(TodayDate.Subtract(testdate).TotalMinutes, 2).ToString();
                                        string[] dt = HowManyHours.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                        string[] dt1 = HowManyMinutes.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                        string FinalDate = (Convert.ToDateTime(testdate).Date == DateTime.UtcNow.Date ? (dt[0] != "0" ? dt[0] + " hours ago" : dt1[0] + " minutes ago") : (Convert.ToDateTime(testdate)).Date == DateTime.UtcNow.AddDays(-1).Date ? "Yesterday " + Convert.ToDateTime(testdate).ToString("hh:mm tt") : Convert.ToDateTime(testdate).ToString("dd MMM yyyy hh:mm tt"));
                                        var bodyhtml = item.Body;
                                        bodyhtml = bodyhtml.Replace("\r\n", "<br>");

                                        var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.InboxID.ToLower().Trim().Equals(item.Headers["Date"].ToLower().Trim()));

                                        EmailData.Add(new MailData
                                        {
                                            Id = item.Headers["Date"],
                                            Date = FinalDate,
                                            Body = bodyhtml,
                                            Cc = string.Join(",", ComibinedCc.Select(s => s)), //ccmails,
                                            Bcc = string.Join(",", ComibinedBcc.Select(s => s)), //bccmails,
                                            _AttachmentRecord = ATT,
                                            Subject = subjects,
                                            To = string.Join(",", ComibinedTo.Select(s => s)), //tomails,
                                            Reply_All_Mails = string.Join(",", ComibinedReplyAll.Select(s => s)), //tos,
                                            Depart_Name = depart,
                                            Display_Name = displayname,
                                            From = fromadd,
                                            skipnum = PageLinks,
                                            searchval = search,
                                            starid = StarIdRecord == null ? 0 : StarIdRecord.TableID
                                        });
                                        maildatecollection.Add(item.Headers["Date"]);
                                        counter++;

                                        mailcount++;



                                    }
                                }
                                if (search.Equals("blank"))
                                {
                                    if (mailcount != 10)
                                    {

                                        int DeletedRecordsCountPlusTenMore = delid.Count() + 10;

                                        uint[] mylist2 = popclient.GetMessageNumbers().OrderByDescending(c => c).Skip(RecordSkip).Take(DeletedRecordsCountPlusTenMore).ToArray();

                                        System.Net.Mail.MailMessage[] messages2 = popclient.GetMessages(mylist2);
                                        if (!search.Equals("blank"))
                                        {
                                            messages2 = messages2.Where(s => (s.From.User.ToLower().Trim().Contains(search.ToLower().Trim())) || (s.Subject.ToLower().Trim().Contains(search.ToLower().Trim()))).ToArray();

                                        }
                                        foreach (var item in messages2)
                                        {

                                            if (!delid.Contains(item.Headers["Date"]))
                                            {
                                                if (!maildatecollection.Contains(item.Headers["Date"]))
                                                {
                                                    item.IsBodyHtml = true;
                                                    List<string> ComibinedReplyAll = new List<string>();
                                                    List<string> ComibinedCc = new List<string>();
                                                    List<string> ComibinedBcc = new List<string>();
                                                    List<string> ComibinedTo = new List<string>();

                                                    ComibinedReplyAll.Add(item.From.Address);

                                                    if (item.To.Count > 0)
                                                    {
                                                        foreach (var ts in item.To)
                                                        {
                                                            if (!record.Username.Contains(ts.Address))
                                                                ComibinedReplyAll.Add(ts.Address);

                                                            ComibinedTo.Add(ts.Address);
                                                        }
                                                    }

                                                    if (item.CC.Count > 0)
                                                    {
                                                        foreach (var ts in item.CC)
                                                        {
                                                            if (!record.Username.Contains(ts.Address))
                                                                ComibinedReplyAll.Add(ts.Address);

                                                            ComibinedCc.Add(ts.Address);
                                                        }
                                                    }

                                                    if (item.Bcc.Count > 0)
                                                    {
                                                        foreach (var ts in item.Bcc)
                                                        {
                                                            ComibinedReplyAll.Add(ts.Address);
                                                            ComibinedBcc.Add(ts.Address);
                                                        }
                                                    }

                                                    List<AttachmentRecord> ATT = new List<AttachmentRecord>();
                                                    byte[] allBytes = null;
                                                    if (item.Attachments.Count > 0)
                                                    {
                                                        foreach (var at in item.Attachments)
                                                        {
                                                            int size = 1;
                                                            try
                                                            {
                                                                allBytes = new byte[at.ContentStream.Length];
                                                                int bytesRead = at.ContentStream.Read(allBytes, 0, (int)at.ContentStream.Length);

                                                                var result = Convert.ToBase64String(allBytes);

                                                                ATT.Add(new AttachmentRecord { Name = at.Name, Value = result });


                                                            }
                                                            catch { size = 1; }
                                                        }
                                                    }


                                                    string subjects = "";
                                                    subjects = (string.IsNullOrEmpty(item.Subject) ? "(no subject)" : item.Subject);
                                                    string displayname = "";
                                                    try
                                                    {
                                                        if (item.From != null)
                                                        {
                                                            displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : (item.From.DisplayName).Replace("<notification+", ""));
                                                        }
                                                        else { displayname = subjects; }
                                                    }
                                                    catch { displayname = (string.IsNullOrEmpty(item.From.DisplayName) ? (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) : item.From.DisplayName); }
                                                    string depart = "";
                                                    string fromadd = "";
                                                    try
                                                    {
                                                        if (item.From != null)
                                                        {
                                                            depart = (string.IsNullOrEmpty(item.From.User) ? "-" : (item.From.User)) + " (" + (string.IsNullOrEmpty(item.From.Address) ? "-" : (item.From.Address)) + ")";
                                                            fromadd = item.From.Address;
                                                        }
                                                        else { depart = "-"; }
                                                    }
                                                    catch { depart = subjects; }




                                                    string[] splitdate = null;
                                                    string dat = item.Headers["Date"];

                                                    if (dat.Contains("+"))
                                                    {
                                                        splitdate = dat.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
                                                    }
                                                    else
                                                    {
                                                        splitdate = dat.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                                    }
                                                    var Date = splitdate[0].Split(',');
                                                    DateTime TodayDate = DateTime.UtcNow.AddMinutes(SystemTimezone);
                                                    DateTime testdate;
                                                    if (Date.Length == 1)
                                                    {
                                                        testdate = Convert.ToDateTime(Date[0]);
                                                    }
                                                    else
                                                    {
                                                        testdate = Convert.ToDateTime(Date[1]);
                                                    }
                                                    string HowManyHours = Math.Round(TodayDate.Subtract(testdate).TotalHours, 2).ToString();
                                                    string HowManyMinutes = Math.Round(TodayDate.Subtract(testdate).TotalMinutes, 2).ToString();
                                                    string[] dt = HowManyHours.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                                    string[] dt1 = HowManyMinutes.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                                                    string FinalDate = (Convert.ToDateTime(testdate).Date == DateTime.UtcNow.Date ? (dt[0] != "0" ? dt[0] + " hours ago" : dt1[0] + " minutes ago") : (Convert.ToDateTime(testdate)).Date == DateTime.UtcNow.AddDays(-1).Date ? "Yesterday " + Convert.ToDateTime(testdate).ToString("hh:mm tt") : Convert.ToDateTime(testdate).ToString("dd MMM yyyy hh:mm tt"));

                                                    var bodyhtml = item.Body;
                                                    bodyhtml = bodyhtml.Replace("\r\n", "<br>");

                                                    var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.InboxID.ToLower().Trim().Equals(item.Headers["Date"].ToLower().Trim()));

                                                    EmailData.Add(new MailData
                                                    {
                                                        Id = item.Headers["Date"],
                                                        Date = FinalDate,
                                                        Body = bodyhtml,
                                                        Cc = string.Join(",", ComibinedCc.Select(s => s)), //ccmails,
                                                        Bcc = string.Join(",", ComibinedBcc.Select(s => s)), //bccmails,
                                                        _AttachmentRecord = ATT,
                                                        Subject = subjects,
                                                        To = string.Join(",", ComibinedTo.Select(s => s)), //tomails,
                                                        Reply_All_Mails = string.Join(",", ComibinedReplyAll.Select(s => s)), //tos,
                                                        Depart_Name = depart,
                                                        Display_Name = displayname,
                                                        From = fromadd,
                                                        skipnum = PageLinks,
                                                        searchval = search,
                                                        starid = StarIdRecord == null ? 0 : StarIdRecord.TableID
                                                    });
                                                    maildatecollection.Add(item.Headers["Date"]);
                                                    counter++;

                                                    mailcount++;

                                                    if (mailcount == 10)
                                                    { break; }



                                                }
                                            }
                                        }

                                    }
                                }


                                return new JsonResult(EmailData);
                            }
                            else
                            {
                                return new JsonResult("nomessage");
                            }
                        }
                        else { return new JsonResult("nomessage"); }
                    }
                }
                else { return new JsonResult("error"); }

            }
            catch (Exception ex)
            {
                // var Path = ("~/tempSaveFile.txt");

                //System.IO.StreamWriter dynamicSw = new System.IO.StreamWriter(Path, true);
                //dynamicSw.WriteLine(ex.Message);
                //dynamicSw.Close(); dynamicSw.Dispose();

                return new JsonResult("error");
            }
        }

        // GET api/values
        [HttpGet("{MasterID}")]
        public JsonResult GetDefaultConfigDetails(int MasterID)
        {
            //int sent = 0;
            //int draft = 0;
            //int outbox = 0;
            //int starred = 0;
            //int trash = 0;
            List<ConfigurationDetails> CD = new List<ConfigurationDetails>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;

                        String Decryptpassword = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

                        CD.Add(new ConfigurationDetails
                        {
                            MasterID = MasterID,
                            AccessWith = record.Access,
                            Compose = record.IsCompose,
                            EmailID = record.Username,
                            Forward = record.IsForward,
                            IncomingHost = record.Host,
                            IncomingPort = record.Port,
                            Password = Decryptpassword,
                            Reply = record.IsReply,
                            ReplyAll = record.IsReplyAll,
                            Signature = record.Signature,
                            SmtpHost = record.SmtpServer,
                            SmtpPort = record.SmtpPort
                        });

                        //trash = record.TrashMailsTables.Where(a => a.IsDeleted == false).Count();
                        //sent = record.SendedMails.Count();
                        //draft = record.DraftEmailTables.Count(); ;
                        //outbox = record.OutBoxMails.Count();
                        //starred = record.StarredEmailTables.Count();

                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET

                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;

                        String Decryptpassword = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

                        CD.Add(new ConfigurationDetails
                        {
                            MasterID = MasterID,
                            AccessWith = record.Access,
                            Compose = record.IsCompose,
                            EmailID = record.Username,
                            Forward = record.IsForward,
                            IncomingHost = record.Host,
                            IncomingPort = record.Port,
                            Password = Decryptpassword,
                            Reply = record.IsReply,
                            ReplyAll = record.IsReplyAll,
                            Signature = record.Signature,
                            SmtpHost = record.SmtpServer,
                            SmtpPort = record.SmtpPort
                        });

                        //trash = record.TrashMailsTables.Count();
                        //sent = record.SendedMails.Count();
                        //draft = record.DraftEmailTables.Count(); ;
                        //outbox = record.OutBoxMails.Count();
                        //starred = record.StarredEmailTables.Count();
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
            }

            //trashcount.InnerHtml = trash.ToString();
            //sendcount.InnerHtml = sent.ToString();
            //draftcount.InnerHtml = draft.ToString();
            //outboxcount.InnerHtml = outbox.ToString();
            //starredcount.InnerHtml = starredcount.ToString();

            //starredcount.InnerHtml = starred.ToString();

            return new JsonResult(CD);
        }

        // CONFIG LIGHTBOX SUBMIT BUTTON
        // POST api/values
        [HttpPost]
        public JsonResult PostConfigurationDetails(ConfigurationDetails ConfigDetails)
        {
            try
            {
                int MasterUserID = 0;

                string access = ConfigDetails.AccessWith;

                string IncomingHost = ConfigDetails.IncomingHost;
                string IncomingPort = ConfigDetails.IncomingPort;

                string SmtpHost = ConfigDetails.SmtpHost;
                string SmtpPort = ConfigDetails.SmtpPort;

                string Username = ConfigDetails.EmailID;
                string Password = ConfigDetails.Password;


                string signature = ConfigDetails.Signature;
                Boolean? reply = ConfigDetails.Reply;
                Boolean? replyall = ConfigDetails.ReplyAll;
                Boolean? forward = ConfigDetails.Forward;
                Boolean? compose = ConfigDetails.Compose;

                EmailToolContext dc = new EmailToolContext();

                // LOGIC :: HERE WE HAVE TO SHOW LAST INSERTED USER
                // BUT WE ARE NOT DELETING EXISTING USERS
                // FORCEFULLY MAKING "IsDeleted = true" TO ALL PREVIOUS INSERTED CREDENTIALS
                var MasterTableDeleteAllRecords = dc.EmailConfig;

                foreach (var item in MasterTableDeleteAllRecords)
                {
                    item.IsDeleted = true;
                }

                dc.SaveChanges();
                //END

                // HERE WE ARE CHECKING IF "USERNAME AKA EMAILID" ALREADY EXIST THEN NO NEED TO CREATE NEW RECORDS
                // ONLY UPDATE THE RECORDS EXCEPT "USERNAME AKA EMAILID"
                var MasterTable = dc.EmailConfig.FirstOrDefault(a => a.Username.ToLower().Trim().Equals(Username.ToLower().Trim()));

                if (MasterTable != null)
                {
                    // THIS IS ENCRYPTING PASSWORD
                    String encryptedText = EncryptAlgo.EncryptText(Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

                    MasterTable.Access = access;
                    MasterTable.SmtpServer = SmtpHost;
                    MasterTable.SmtpPort = SmtpPort;
                    MasterTable.Password = encryptedText;
                    MasterTable.Host = IncomingHost;
                    MasterTable.Port = IncomingPort;
                    MasterTable.UserId = 0;
                    MasterTable.IsDeleted = false;
                    MasterTable.Signature = signature;
                    MasterTable.IsReply = (reply == true ? true : reply == false ? false : false);
                    MasterTable.IsReplyAll = (replyall == true ? true : false);
                    MasterTable.IsCompose = (compose == true ? true : false);
                    MasterTable.IsForward = (forward == true ? true : false);

                    dc.SaveChanges();

                    // SAVING VALUES IN SESSION
                    MasterUserID = MasterTable.Id;
                }
                else
                {
                    EmailConfig MasterTableObj = new EmailConfig();

                    // THIS IS ENCRYPTING PASSWORD
                    String encryptedText = EncryptAlgo.EncryptText(Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

                    MasterTableObj.Access = access;
                    MasterTableObj.SmtpServer = SmtpHost;
                    MasterTableObj.SmtpPort = SmtpPort;
                    MasterTableObj.Username = Username;
                    MasterTableObj.Password = encryptedText;
                    MasterTableObj.Host = IncomingHost;
                    MasterTableObj.Port = IncomingPort;
                    MasterTableObj.UserId = 0;
                    MasterTableObj.IsDeleted = false;
                    MasterTableObj.Signature = signature;
                    MasterTableObj.IsReply = (reply == true ? true : false);
                    MasterTableObj.IsReplyAll = (replyall == true ? true : false);
                    MasterTableObj.IsCompose = (compose == true ? true : false);
                    MasterTableObj.IsForward = (forward == true ? true : false);

                    dc.EmailConfig.Add(MasterTableObj);
                    dc.SaveChanges();


                    // SAVING VALUES IN SESSION
                    MasterUserID = MasterTableObj.Id;
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Configuration details reset successfully", anyid = MasterUserID, anystring = "" };

                return new JsonResult(_ReturnResponse);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Can not reset this time due to " + ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }

        // RESET CONFIG DETAILS
        // GET api/values/5
        [HttpGet("{MasterID}")]
        public JsonResult GetResetSubmit(int MasterID)
        {
            try
            {
                using (EmailToolContext dc = new EmailToolContext())
                {
                    var MasterTable = dc.EmailConfig.FirstOrDefault(a => a.Id == MasterID);

                    if (MasterTable != null)
                    {
                        MasterTable.IsDeleted = true;
                        dc.SaveChanges();
                    }

                    // HttpContext.Current.Session["ConfigId"] = null;
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Configuration details reset successfully", anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Can not reset this time due to " + ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }

        /* COMPOSE MAIL SEND */
        [HttpPost]
        public JsonResult PostComposeMailDetails(ComposeMailData ComposeMailData)
        {
            EmailToolContext db = new EmailToolContext();

            int MasterID = ComposeMailData.MasterID;

            if (MasterID == 0)
                return new JsonResult(new { action = "error", id = 0, error = "Please configure your email account first." });

            var record = (from rec in db.EmailConfig
                          where rec.Id == MasterID
                          select rec).AsEnumerable().LastOrDefault();

            if (record == null)
                return new JsonResult(new { action = "error", id = 0, error = "Please configure your email account first." });

            string success = "";
            string wrongemail = "";
            string sucessemail = "";
            string sucessto = "";
            string sucessCc = "";
            string sucessBcc = "";

            string pass = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);

            System.Net.Mail.MailMessage oMail = new System.Net.Mail.MailMessage();
            System.Net.Mail.Attachment attachment1;
            oMail.From = new System.Net.Mail.MailAddress(record.Username);

            // to send emails
            if (ComposeMailData.To != "")
            {
                string[] splitemail = ComposeMailData.To.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (var i = 0; i < splitemail.Length; i++)
                {
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(splitemail[i]);
                    if (match.Success)
                    {
                        oMail.To.Add(new System.Net.Mail.MailAddress(splitemail[i]));
                        sucessemail += splitemail[i];
                        sucessto += splitemail[i] + ",";
                    }
                    else
                    {
                        wrongemail += splitemail[i] + ",";
                    }

                }

            }
            if (ComposeMailData.Bcc != "")
            {
                string[] splitemail = ComposeMailData.Bcc.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                for (var i = 0; i < splitemail.Length; i++)
                {
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(splitemail[i]);
                    if (match.Success)
                    {
                        oMail.Bcc.Add(new System.Net.Mail.MailAddress(splitemail[i]));
                        sucessemail += splitemail[i] + ",";
                        sucessBcc += splitemail[i] + ",";
                    }
                    else
                    {
                        wrongemail += splitemail[i] + ",";
                    }
                }
            }
            if (ComposeMailData.Cc != "")
            {
                string[] splitemail = ComposeMailData.Cc.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                for (var i = 0; i < splitemail.Length; i++)
                {
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(splitemail[i]);
                    if (match.Success)
                    {
                        oMail.CC.Add(new System.Net.Mail.MailAddress(splitemail[i]));
                        sucessemail += splitemail[i];
                        sucessCc += splitemail[i] + ",";
                    }
                    else
                    {
                        wrongemail += splitemail[i] + ",";
                    }
                }
            }

            if (ComposeMailData.Attachment != "")
            {
                string[] splitattach = ComposeMailData.Attachment.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (var i = 0; i < splitattach.Length; i++)
                {
                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmailAttacements");

                    if (!System.IO.Directory.Exists(Epath))
                    {
                        System.IO.Directory.CreateDirectory(Epath);
                    }

                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmailAttacements\\", splitattach[i]);

                    //string file = HttpContext.Current.Server.MapPath("EmailAttacements" + "/" + splitattach[i]);

                    attachment1 = new System.Net.Mail.Attachment(path);
                    oMail.Attachments.Add(attachment1);
                }
            }

            try
            {

                oMail.ReplyTo = new System.Net.Mail.MailAddress(record.Username);
                oMail.Subject = ComposeMailData.Subject;
                oMail.IsBodyHtml = true; // enumeration
                oMail.Priority = System.Net.Mail.MailPriority.High; // enumeration
                oMail.Body = ComposeMailData.Body;

                System.Net.NetworkCredential nc = new System.Net.NetworkCredential(record.Username, pass);
                System.Net.Mail.SmtpClient sC = new System.Net.Mail.SmtpClient();
                int port = Convert.ToInt32(record.SmtpPort);



                string SERVER = record.SmtpServer;
                sC.Host = SERVER;
                sC.Port = port;

                // 3

                sC.Credentials = nc;
                sC.EnableSsl = false;

                sC.Send(oMail);

                success = "done";

            }
            catch (Exception ex)
            {
                // if failure mail store in outbox
                if (!ex.Equals("A recipient must be specified.") && wrongemail == "")
                {
                    OutBoxMail ob = new OutBoxMail();

                    var unique = DateTime.UtcNow.Ticks;
                    //int sid = Convert.ToInt32(HttpContext.Current.Session["ConfigId"]);

                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Outbox_Email\\" + MasterID);

                    if (!System.IO.Directory.Exists(Epath))
                    {
                        System.IO.Directory.CreateDirectory(Epath);
                    }

                    string path = "Outbox_Email/" + MasterID + "/" + unique + ".xml";

                    ob.UserId = ComposeMailData.MasterID; //Convert.ToInt32(HttpContext.Current.Session["ConfigId"]);
                    ob.OutboxMailPath = path;

                    db.OutBoxMail.Add(ob);
                    db.SaveChanges();

                    System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + path), System.Text.Encoding.UTF8);

                    writer.WriteStartDocument(true);
                    writer.Formatting = System.Xml.Formatting.Indented;
                    writer.Indentation = 2;
                    writer.WriteStartElement(StaticKeywordClass.Outbox);

                    HelperClass.createNode(writer, "id", ob.Id.ToString());
                    HelperClass.createNode(writer, "to", sucessto);
                    HelperClass.createNode(writer, "cc", sucessCc);
                    HelperClass.createNode(writer, "bcc", sucessBcc);
                    HelperClass.createNode(writer, "subject", ComposeMailData.Subject);
                    HelperClass.createNode(writer, "body", ComposeMailData.Body);
                    HelperClass.createNode(writer, "Attachement", ComposeMailData.Attachment);
                    HelperClass.createNode(writer, "date", DateTime.UtcNow.AddMinutes(330).ToString("MMM-dd-yyyy hh:mm tt"));
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Something went wrong mail saved in outbox" + "[!]" + sucessemail + "[!]" + wrongemail, anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }

            try
            {
                // for save mail in send box
                SendedMail sm = new SendedMail();

                var unique = DateTime.UtcNow.Ticks;

                var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Sended_Email\\" + MasterID);

                if (!System.IO.Directory.Exists(Epath))
                {
                    System.IO.Directory.CreateDirectory(Epath);
                }

                string path = "Sended_Email/" + MasterID + "/" + unique + ".xml";

                sm.UserId = ComposeMailData.MasterID;  //Convert.ToInt32(HttpContext.Current.Session["ConfigId"]);
                sm.SendMailPath = path;

                db.SendedMail.Add(sm);
                db.SaveChanges();

                System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + path), System.Text.Encoding.UTF8);

                writer.WriteStartDocument(true);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement(StaticKeywordClass.Sent);
                HelperClass.createNode(writer, "id", sm.Id.ToString());
                HelperClass.createNode(writer, "to", sucessto);
                HelperClass.createNode(writer, "cc", sucessCc);
                HelperClass.createNode(writer, "bcc", sucessBcc);
                HelperClass.createNode(writer, "subject", ComposeMailData.Subject);
                HelperClass.createNode(writer, "body", ComposeMailData.Body);
                HelperClass.createNode(writer, "Attachement", ComposeMailData.Attachment);
                HelperClass.createNode(writer, "date", DateTime.UtcNow.AddMinutes(330).ToString("MMM-dd-yyyy hh:mm tt"));
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = success + "[!]" + sucessemail + "[!]" + wrongemail, anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);

            }
            catch (Exception ex)
            {
                //string Path = System.Web.HttpContext.Current.Server.MapPath("~/tempSaveFile.txt");

                //System.IO.StreamWriter dynamicSw = new System.IO.StreamWriter(Path, true);
                //dynamicSw.WriteLine(ex.Message);
                //dynamicSw.Close(); dynamicSw.Dispose();
                //return "fail";

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not sent recipient(s) due to " + ex.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);

            }
        }


        /* COMPOSE MAIL Draft */
        [HttpPost]
        public JsonResult PostDraftMailDetails(ComposeMailData ComposeMailData)
        {
            try
            {
                EmailToolContext db = new EmailToolContext();

                DraftEmailTable de = new DraftEmailTable();

                int MasterID = ComposeMailData.MasterID;

                if (MasterID == 0)
                    return new JsonResult(new { action = "error", id = 0, error = "Please configure your email account first." });

                var unique = DateTime.UtcNow.Ticks;

                var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Draft_Email\\" + MasterID);

                if (!System.IO.Directory.Exists(Epath))
                {
                    System.IO.Directory.CreateDirectory(Epath);
                }

                string path = "Draft_Email/" + MasterID + "/" + unique + ".xml";

                de.UserId = ComposeMailData.MasterID;  //Convert.ToInt32(HttpContext.Current.Session["ConfigId"]);
                de.Draftpath = path;

                db.DraftEmailTable.Add(de);
                db.SaveChanges();

                System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + path), System.Text.Encoding.UTF8);

                writer.WriteStartDocument(true);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement(StaticKeywordClass.Draft);

                HelperClass.createNode(writer, "id", de.Id.ToString());
                HelperClass.createNode(writer, "to", ComposeMailData.To);
                HelperClass.createNode(writer, "cc", ComposeMailData.Cc);
                HelperClass.createNode(writer, "bcc", ComposeMailData.Bcc);
                HelperClass.createNode(writer, "subject", ComposeMailData.Subject);
                HelperClass.createNode(writer, "body", ComposeMailData.Body);
                HelperClass.createNode(writer, "Attachement", ComposeMailData.Attachment);
                HelperClass.createNode(writer, "date", DateTime.UtcNow.AddMinutes(330).ToString("MMM-dd-yyyy hh:mm tt"));
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully", anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Attachment not deleted successfully due to " + ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }

        /* COMPOSE MAIL FILE UPLOAD ACTION */
        [HttpPost]
        public ActionResult ComposeFileUpload()
        {
            try
            {

                long size = 0;
                var files = Request.Form.Files;

                string NewlyCreatedFileName = "";

                foreach (var file in files)
                {
                    //var filename = ContentDispositionHeaderValue
                    //                .Parse(file.ContentDisposition)
                    //                .FileName
                    //                .Trim('"');

                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmailAttacements");

                    if (!System.IO.Directory.Exists(Epath))
                    {
                        System.IO.Directory.CreateDirectory(Epath);
                    }

                    string filename = DateTime.UtcNow.Ticks.ToString();
                    string fname = file.FileName;
                    string ext = Path.GetExtension(fname);

                    NewlyCreatedFileName = filename + ext;

                    filename = Epath + $@"\" + NewlyCreatedFileName;
                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(filename))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                }

                //string message = $"{files.Count} file(s) / { size}bytes uploaded successfully!";
                // return new JsonResult(new { filename = NewlyCreatedFileName });
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "", anystring = NewlyCreatedFileName };

                return new JsonResult(_ReturnResponse);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Attachment can not add this time due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }
        }

        // DELETE COMPOSE MAIL TEMPORARY ATTACHMENTS
        // GET api/values/5
        [HttpGet("{file}")]
        public JsonResult GetDeleteAttachmentSubmit(string file)
        {
            try
            {

                var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmailAttacements\\" + file);

                if (System.IO.File.Exists(Epath))
                {
                    System.IO.File.SetAttributes(Epath, System.IO.FileAttributes.Normal);

                    System.IO.File.Delete(Epath);
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully", anystring = file };

                return new JsonResult(_ReturnResponse);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Attachment not deleted successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }
        }

        /*********************** SENT MAIL LISTING CODE **************************/
        [HttpGet("{skip}/{search}/{masterid}")]
        public JsonResult GetSentRecords(int skip, string search, int masterid)
        {
            int MasterID = masterid;

            List<MailData> EmailData = new List<MailData>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        // HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

            }

            try
            {

                EmailToolContext db = new EmailToolContext();

                var record = from rec in db.SendedMail
                             where rec.UserId == MasterID
                             select rec;

                //var StarId = (from re in db.StarredEmailTable.AsEnumerable()
                //              where re.UserId == MasterID && re.IsInbox == false
                //              select re.EmailId).ToArray();

                var StarredMailRecords = (from rec in db.StarredEmail
                                          where rec.SentId.HasValue
                                          select rec).Select(a => new { TableID = a.Id, ReferID = a.SentId });

                record = record.OrderByDescending(a => a.Id);

                if (record.Count() == 0)
                {
                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "no sent mail found", anyid = 0, anystring = "" };

                    return new JsonResult(_ReturnResponse);
                }


                StringBuilder ss = new StringBuilder();
                foreach (var item in record)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                    try
                    {
                        doc.Load(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + item.SendMailPath));
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    System.Xml.XmlNode id = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/id");
                    System.Xml.XmlNode to = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/to");
                    System.Xml.XmlNode sub = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/subject");
                    System.Xml.XmlNode body = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/body");
                    System.Xml.XmlNode cc = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/cc");
                    System.Xml.XmlNode bcc = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/bcc");
                    System.Xml.XmlNode date = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/date");
                    System.Xml.XmlNode Attachement = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/Attachement");
                    var tos = "";
                    string[] spltto = to.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] spltcc = cc.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] attach = Attachement.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < spltto.Length; i++)
                    {
                        if (spltto[i] != "")
                        {
                            tos += spltto[i] + ",";
                        }
                    }
                    for (int i = 0; i < spltcc.Length; i++)
                    {
                        if (spltcc[i] != "")
                        {
                            tos += spltcc[i] + ",";
                        }
                    }

                    var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.ReferID == item.Id);

                    EmailData.Add(new MailData
                    {
                        Id = id.InnerText,
                        To = to.InnerText,
                        Subject = (string.IsNullOrEmpty(sub.InnerText) ? "" : sub.InnerText),
                        Body = (string.IsNullOrEmpty(body.InnerText) ? "" : body.InnerText),
                        Cc = (string.IsNullOrEmpty(cc.InnerText) ? "" : cc.InnerText),
                        Bcc = (string.IsNullOrEmpty(bcc.InnerText) ? "" : bcc.InnerText),
                        Date = date.InnerText,
                        Attachment_Name = attach,
                        Reply_All_Mails = tos,
                        starid = StarIdRecord == null ? 0 : StarIdRecord.TableID
                    });
                }

                if (!search.Equals("blank"))
                {
                    EmailData = EmailData.Where(s => s.To.ToLower().Trim().Contains(search.ToLower().Trim())
                                                || s.Subject.ToLower().Trim().Contains(search.ToLower().Trim())).ToList();
                }

                int count = EmailData.Count();
                var RecordSkip = (skip - 1) * 10;
                EmailData = EmailData.Skip(RecordSkip).Take(10).ToList();

                var PageLinks = count % 10 == 0 ? (count / 10) : (count / 10) + 1;

                List<FinalJsonCollection> fdata = new List<FinalJsonCollection>();

                fdata.Add(new FinalJsonCollection { _MailData = EmailData, TotalPages = PageLinks });

                return new JsonResult(fdata);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }

        //TO DOWNLOAD SENTBOX ATTACHMENT
        [HttpGet("{name}")]
        public FileResult DownloadAttachment(string name)
        {
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmailAttacements\\" + name);

            FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes(filePath), "application/octet-stream")
            {
                FileDownloadName = name
            };

            return result;

        }

        // Draft mail
        [HttpGet("{skip}/{search}/{masterid}")]
        public JsonResult GetDraftRecords(int skip, string search, int masterid)
        {
            int MasterID = masterid;

            List<MailData> EmailData = new List<MailData>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        // HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

            }

            try
            {

                EmailToolContext db = new EmailToolContext();

                var record = from rec in db.DraftEmailTable
                             where rec.UserId == MasterID
                             select rec;

                //var StarId = (from re in db.StarredEmailTable.AsEnumerable()
                //              where re.UserId == MasterID && re.IsInbox == false
                //              select re.EmailId).ToArray();

                var StarredMailRecords = (from rec in db.StarredEmail
                                          where rec.DraftId.HasValue
                                          select rec).Select(a => new { TableID = a.Id, ReferID = a.DraftId });

                record = record.OrderByDescending(a => a.Id);

                if (record.Count() == 0)
                {
                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "no draft mail found", anyid = 0, anystring = "" };

                    return new JsonResult(_ReturnResponse);
                }


                StringBuilder ss = new StringBuilder();
                foreach (var item in record)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                    try
                    {
                        doc.Load(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + item.Draftpath));
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    System.Xml.XmlNode id = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/id");
                    System.Xml.XmlNode to = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/to");
                    System.Xml.XmlNode sub = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/subject");
                    System.Xml.XmlNode body = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/body");
                    System.Xml.XmlNode cc = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/cc");
                    System.Xml.XmlNode bcc = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/bcc");
                    System.Xml.XmlNode date = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/date");
                    System.Xml.XmlNode Attachement = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/Attachement");
                    var tos = "";
                    string[] spltto = to.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] spltcc = cc.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] attach = Attachement.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < spltto.Length; i++)
                    {
                        if (spltto[i] != "")
                        {
                            tos += spltto[i] + ",";
                        }
                    }
                    for (int i = 0; i < spltcc.Length; i++)
                    {
                        if (spltcc[i] != "")
                        {
                            tos += spltcc[i] + ",";
                        }
                    }

                    var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.ReferID == item.Id);

                    EmailData.Add(new MailData
                    {
                        Id = id.InnerText,
                        To = (string.IsNullOrEmpty(to.InnerText) ? "(no recipient)" : to.InnerText),
                        Subject = (string.IsNullOrEmpty(sub.InnerText) ? "(no subject)" : sub.InnerText),
                        Body = (string.IsNullOrEmpty(body.InnerText) ? "" : body.InnerText),
                        Cc = (string.IsNullOrEmpty(cc.InnerText) ? "" : cc.InnerText),
                        Bcc = (string.IsNullOrEmpty(bcc.InnerText) ? "" : bcc.InnerText),
                        Date = date.InnerText,
                        Attachment_Name = attach,
                        Reply_All_Mails = tos,
                        starid = StarIdRecord == null ? 0 : StarIdRecord.TableID
                    });
                }

                if (!search.Equals("blank"))
                {
                    EmailData = EmailData.Where(s => s.To.ToLower().Trim().Contains(search.ToLower().Trim())
                                                || s.Subject.ToLower().Trim().Contains(search.ToLower().Trim())).ToList();
                }

                int count = EmailData.Count();
                var RecordSkip = (skip - 1) * 10;
                EmailData = EmailData.Skip(RecordSkip).Take(10).ToList();

                var PageLinks = count % 10 == 0 ? (count / 10) : (count / 10) + 1;

                List<FinalJsonCollection> fdata = new List<FinalJsonCollection>();

                fdata.Add(new FinalJsonCollection { _MailData = EmailData, TotalPages = PageLinks });

                return new JsonResult(fdata);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }


        [HttpGet("{skip}/{search}/{masterid}")]
        public JsonResult GetTrashRecords(int skip, string search, int masterid)
        {
            int MasterID = masterid;

            List<MailData> EmailData = new List<MailData>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        // HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

            }

            try
            {

                EmailToolContext db = new EmailToolContext();

                var record = from rec in db.TrashMailsTable
                             where rec.UserId == MasterID && rec.IsDeleted == false
                             select rec;

                record = record.OrderByDescending(a => a.Id);

                if (record.Count() == 0)
                {
                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "no mail found in trash", anyid = 0, anystring = "" };

                    return new JsonResult(_ReturnResponse);
                }


                StringBuilder ss = new StringBuilder();
                foreach (var item in record)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                    try
                    {
                        doc.Load(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + item.Xmlpath));
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    System.Xml.XmlNode id = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/id");

                    string from = "";
                    try
                    {
                        System.Xml.XmlNode _from = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/from");
                        from = (string.IsNullOrEmpty(_from.InnerText) ? "" : _from.InnerText);
                    }
                    catch (Exception)
                    { from = ""; }

                    System.Xml.XmlNode to = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/to");
                    System.Xml.XmlNode sub = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/subject");
                    System.Xml.XmlNode body = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/body");
                    System.Xml.XmlNode cc = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/cc");
                    System.Xml.XmlNode bcc = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/bcc");
                    System.Xml.XmlNode date = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/date");
                    System.Xml.XmlNode Attachement = doc.DocumentElement.SelectSingleNode("/" + item.EmailType + "/Attachement");
                    var tos = "";
                    string[] spltto = to.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] spltcc = cc.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] attach = Attachement.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < spltto.Length; i++)
                    {
                        if (spltto[i] != "")
                        {
                            if (tos != "")
                                tos += ",";

                            tos += spltto[i];
                        }
                    }

                    for (int i = 0; i < spltcc.Length; i++)
                    {
                        if (spltcc[i] != "")
                        {
                            if (tos != "")
                                tos += ",";

                            tos += spltcc[i];
                        }
                    }


                    EmailData.Add(new MailData
                    {
                        Id = id.InnerText,
                        From = from,
                        To = (string.IsNullOrEmpty(to.InnerText) ? "(no recipient)" : to.InnerText),
                        Subject = (string.IsNullOrEmpty(sub.InnerText) ? "(no subject)" : sub.InnerText),
                        Body = (string.IsNullOrEmpty(body.InnerText) ? "" : body.InnerText),
                        Cc = (string.IsNullOrEmpty(cc.InnerText) ? "" : cc.InnerText),
                        Bcc = (string.IsNullOrEmpty(bcc.InnerText) ? "" : bcc.InnerText),
                        Date = date.InnerText,
                        Attachment_Name = attach,
                        Reply_All_Mails = tos
                    });
                }

                if (!search.Equals("blank"))
                {
                    EmailData = EmailData.Where(s => s.To.ToLower().Trim().Contains(search.ToLower().Trim())
                                                || s.Subject.ToLower().Trim().Contains(search.ToLower().Trim())).ToList();
                }

                int count = EmailData.Count();
                var RecordSkip = (skip - 1) * 10;
                EmailData = EmailData.Skip(RecordSkip).Take(10).ToList();

                var PageLinks = count % 10 == 0 ? (count / 10) : (count / 10) + 1;

                List<FinalJsonCollection> fdata = new List<FinalJsonCollection>();

                fdata.Add(new FinalJsonCollection { _MailData = EmailData, TotalPages = PageLinks });

                return new JsonResult(fdata);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }


        // MAIL TAB COUNTS
        [HttpGet("{MasterID}")]
        public JsonResult GetTabCount(int MasterID)
        {
            List<EmailTabCounts> EmailCount = new List<EmailTabCounts>();

            using (EmailToolContext db = new EmailToolContext())
            {
                if (MasterID == 0)
                {
                    var masteridrecord = (from rec in db.EmailConfig.AsEnumerable()
                                          where !(rec.IsDeleted ?? false)
                                          select rec).LastOrDefault();

                    if (masteridrecord != null)
                    {
                        MasterID = masteridrecord.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

                var record = (from rec in db.EmailConfig.Include(a => a.SendedMail).Include(b => b.DraftEmailTable).Include(c => c.OutBoxMail).Include(d => d.StarredEmail).Include(e => e.TrashMailsTable)
                              where rec.Id == MasterID
                              select rec).FirstOrDefault();

                int inboxcount = 0;
                String Decryptpassword = EncryptAlgo.DecryptText(record.Password, EncryptAlgo.theKey, EncryptAlgo.theIV);
                if (record.Access == "imap")
                {

                    using (ImapClient Iclient = new ImapClient(record.Host, Convert.ToInt32(record.Port), record.Username, Decryptpassword, S22.Imap.AuthMethod.Login, true, null))
                    {

                        IEnumerable<uint> uids = Iclient.Search(S22.Imap.SearchCondition.All(), "INBOX");
                        inboxcount = uids.Count();
                    }

                }
                else
                {
                    using (S22.Pop3.Pop3Client popclient = new S22.Pop3.Pop3Client(record.Host, Convert.ToInt32(record.Port), record.Username, Decryptpassword, S22.Pop3.AuthMethod.Login, true, null))
                    {

                        var total = popclient.GetMessageNumbers().Count();
                        inboxcount = total;
                    }
                }



                EmailCount.Add(new EmailTabCounts
                {
                    InboxCount = inboxcount,
                    SentCount = record.SendedMail.Count(),
                    DraftCount = record.DraftEmailTable.Count(),
                    OutboxCount = record.OutBoxMail.Count(),
                    StarredCount = record.StarredEmail.Count(),
                    TrashCount = record.TrashMailsTable.Count(),

                });
            }

            return new JsonResult(EmailCount);
        }


        [HttpGet("{skip}/{search}/{masterid}")]
        public JsonResult GetOutboxRecords(int skip, string search, int masterid)
        {
            int MasterID = masterid;

            List<MailData> EmailData = new List<MailData>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        // HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

            }

            try
            {

                EmailToolContext db = new EmailToolContext();

                var record = from rec in db.OutBoxMail
                             where rec.UserId == MasterID
                             select rec;

                //var StarId = (from re in db.StarredEmailTable.AsEnumerable()
                //              where re.UserId == MasterID && re.IsInbox == false
                //              select re.EmailId).ToArray();

                var StarredMailRecords = (from rec in db.StarredEmail
                                          where rec.OutboxId.HasValue
                                          select rec).Select(a => new { TableID = a.Id, ReferID = a.OutboxId });

                record = record.OrderByDescending(a => a.Id);

                if (record.Count() == 0)
                {
                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "no sent mail found", anyid = 0, anystring = "" };

                    return new JsonResult(_ReturnResponse);
                }


                StringBuilder ss = new StringBuilder();
                foreach (var item in record)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                    try
                    {
                        doc.Load(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + item.OutboxMailPath));
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    System.Xml.XmlNode id = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/id");
                    System.Xml.XmlNode to = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/to");
                    System.Xml.XmlNode sub = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/subject");
                    System.Xml.XmlNode body = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/body");
                    System.Xml.XmlNode cc = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/cc");
                    System.Xml.XmlNode bcc = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/bcc");
                    System.Xml.XmlNode date = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/date");
                    System.Xml.XmlNode Attachement = doc.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/Attachement");
                    var tos = "";
                    string[] spltto = to.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] spltcc = cc.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] attach = Attachement.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < spltto.Length; i++)
                    {
                        if (spltto[i] != "")
                        {
                            tos += spltto[i] + ",";
                        }
                    }
                    for (int i = 0; i < spltcc.Length; i++)
                    {
                        if (spltcc[i] != "")
                        {
                            tos += spltcc[i] + ",";
                        }
                    }

                    var StarIdRecord = StarredMailRecords.FirstOrDefault(a => a.ReferID == item.Id);

                    EmailData.Add(new MailData
                    {
                        Id = id.InnerText,
                        To = to.InnerText,
                        Subject = (string.IsNullOrEmpty(sub.InnerText) ? "" : sub.InnerText),
                        Body = (string.IsNullOrEmpty(body.InnerText) ? "" : body.InnerText),
                        Cc = (string.IsNullOrEmpty(cc.InnerText) ? "" : cc.InnerText),
                        Bcc = (string.IsNullOrEmpty(bcc.InnerText) ? "" : bcc.InnerText),
                        Date = date.InnerText,
                        Attachment_Name = attach,
                        Reply_All_Mails = tos,
                        starid = StarIdRecord == null ? 0 : StarIdRecord.TableID
                    });
                }

                if (!search.Equals("blank"))
                {
                    EmailData = EmailData.Where(s => s.To.ToLower().Trim().Contains(search.ToLower().Trim())
                                                || s.Subject.ToLower().Trim().Contains(search.ToLower().Trim())).ToList();
                }

                int count = EmailData.Count();
                var RecordSkip = (skip - 1) * 10;
                EmailData = EmailData.Skip(RecordSkip).Take(10).ToList();

                var PageLinks = count % 10 == 0 ? (count / 10) : (count / 10) + 1;

                List<FinalJsonCollection> fdata = new List<FinalJsonCollection>();

                fdata.Add(new FinalJsonCollection { _MailData = EmailData, TotalPages = PageLinks });

                return new JsonResult(fdata);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }


        // MAKING SAVE STAR INBOX MAIL
        [HttpPost]
        public JsonResult PostSaveStarMail(DeleteInboxMail DeleteInboxMail)
        {
            int IsStarRecordInserted = 0;

            using (EmailToolContext db = new EmailToolContext())
            {
                try
                {
                    StarredEmail ob = new StarredEmail();

                    var unique = DateTime.UtcNow.Ticks;

                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\StarInbox_Email\\" + DeleteInboxMail.MasterID);

                    if (!System.IO.Directory.Exists(Epath))
                    {
                        System.IO.Directory.CreateDirectory(Epath);
                    }

                    string path = "StarInbox_Email/" + DeleteInboxMail.MasterID + "/" + unique + ".xml";

                    ob.InboxId = DeleteInboxMail.DeleteID;
                    ob.UserId = DeleteInboxMail.MasterID;
                    ob.StarredDate = DateTime.UtcNow;
                    ob.XmlPath = path;

                    db.StarredEmail.Add(ob);
                    db.SaveChanges();

                    IsStarRecordInserted = ob.Id;

                    System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + path), System.Text.Encoding.UTF8);

                    writer.WriteStartDocument(true);
                    writer.Formatting = System.Xml.Formatting.Indented;
                    writer.Indentation = 2;
                    writer.WriteStartElement(StaticKeywordClass.Inbox);

                    HelperClass.createNode(writer, "id", ob.Id.ToString());
                    HelperClass.createNode(writer, "from", DeleteInboxMail.From);
                    HelperClass.createNode(writer, "to", DeleteInboxMail.To);
                    HelperClass.createNode(writer, "cc", DeleteInboxMail.Cc);
                    HelperClass.createNode(writer, "bcc", DeleteInboxMail.Bcc);
                    HelperClass.createNode(writer, "subject", DeleteInboxMail.Subject);
                    HelperClass.createNode(writer, "body", DeleteInboxMail.Body);
                    HelperClass.createNode(writer, "Attachement", DeleteInboxMail.Attachment);
                    HelperClass.createNode(writer, "date", DateTime.UtcNow.AddMinutes(330).ToString("MMM-dd-yyyy hh:mm tt"));
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "" };

                    return new JsonResult(_ReturnResponse);
                }
                catch (Exception)
                {
                    if (IsStarRecordInserted > 0)
                    {
                        db.StarredEmail.Remove(db.StarredEmail.FirstOrDefault(a => a.Id == IsStarRecordInserted));
                        db.SaveChanges();
                    }

                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "" };

                    return new JsonResult(_ReturnResponse);
                }
            }

        }

        // MAKING SAVE STAR OTHER TABS MAIL
        [HttpPost]
        public JsonResult PostSaveStarMailOther(DeleteInboxMail DeleteInboxMail)
        {
            int IsStarRecordInserted = 0;

            using (EmailToolContext db = new EmailToolContext())
            {
                try
                {
                    StarredEmail ob = new StarredEmail();

                    ob.UserId = DeleteInboxMail.MasterID;
                    ob.StarredDate = DateTime.UtcNow;

                    if (DeleteInboxMail.labeltxt.Equals(StaticKeywordClass.Sent))
                    {
                        var detail = db.SendedMail.FirstOrDefault(a => a.Id == Convert.ToInt32(DeleteInboxMail.DeleteID));

                        if (detail != null)
                        {
                            ob.SentId = detail.Id;
                            ob.XmlPath = detail.SendMailPath;
                        }
                    }
                    else if (DeleteInboxMail.labeltxt.Equals(StaticKeywordClass.Draft))
                    {
                        var detail = db.DraftEmailTable.FirstOrDefault(a => a.Id == Convert.ToInt32(DeleteInboxMail.DeleteID));

                        if (detail != null)
                        {
                            ob.DraftId = detail.Id;
                            ob.XmlPath = detail.Draftpath;
                        }
                    }
                    else if (DeleteInboxMail.labeltxt.Equals(StaticKeywordClass.Outbox))
                    {
                        var detail = db.OutBoxMail.FirstOrDefault(a => a.Id == Convert.ToInt32(DeleteInboxMail.DeleteID));

                        if (detail != null)
                        {
                            ob.OutboxId = detail.Id;
                            ob.XmlPath = detail.OutboxMailPath;
                        }
                    }
                    else if (DeleteInboxMail.labeltxt.Equals(StaticKeywordClass.Trash))
                    {
                        var detail = db.TrashMailsTable.FirstOrDefault(a => a.Id == Convert.ToInt32(DeleteInboxMail.DeleteID));

                        if (detail != null)
                        {
                            ob.TrashId = detail.Id;
                            ob.XmlPath = detail.Xmlpath;
                        }
                    }

                    db.StarredEmail.Add(ob);
                    db.SaveChanges();

                    IsStarRecordInserted = ob.Id;

                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "" };

                    return new JsonResult(_ReturnResponse);
                }
                catch (Exception)
                {
                    if (IsStarRecordInserted > 0)
                    {
                        db.StarredEmail.Remove(db.StarredEmail.FirstOrDefault(a => a.Id == IsStarRecordInserted));
                        db.SaveChanges();
                    }

                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "" };

                    return new JsonResult(_ReturnResponse);
                }
            }
        }

        [HttpGet("{StarID}")]
        public JsonResult GetRemoveStarEvent(int StarID)
        {

            using (EmailToolContext db = new EmailToolContext())
            {
                var record = (from rec in db.StarredEmail
                              where rec.Id == StarID
                              select rec).FirstOrDefault();

                if (record != null)
                {
                    db.StarredEmail.Remove(record);
                    db.SaveChanges();
                }
            }

            ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "" };

            return new JsonResult(_ReturnResponse);
        }

        [HttpGet("{skip}/{search}/{masterid}")]
        public JsonResult GetStarredRecords(int skip, string search, int masterid)
        {
            int MasterID = masterid;

            List<MailData> EmailData = new List<MailData>();

            using (EmailToolContext dc = new EmailToolContext())
            {
                // 
                if (MasterID > 0)
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where rec.Id == MasterID
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        // HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }
                else
                {
                    var record = (from rec in dc.EmailConfig.AsEnumerable()
                                  where !(rec.IsDeleted ?? false)
                                  select rec).LastOrDefault();

                    if (record != null)
                    {
                        MasterID = record.Id;
                        //HttpContext.Current.Session["ConfigId"] = record.Id;
                    }
                    else
                    {
                        // ERROR : NO EMAIL CREDENTIALS SET
                    }
                }

            }

            try
            {

                EmailToolContext db = new EmailToolContext();

                var record = from rec in db.StarredEmail
                             where rec.UserId == MasterID
                             select rec;

                record = record.OrderByDescending(a => a.Id);

                if (record.Count() == 0)
                {
                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "no starred mail found", anyid = 0, anystring = "" };

                    return new JsonResult(_ReturnResponse);
                }


                StringBuilder ss = new StringBuilder();
                foreach (var item in record)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                    try
                    {
                        doc.Load(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + item.XmlPath));
                    }
                    catch (Exception)
                    {
                        continue;
                    }

                    string EmailType = "";

                    if (item.SentId.HasValue)
                        EmailType = StaticKeywordClass.Sent;
                    else if (item.DraftId.HasValue)
                        EmailType = StaticKeywordClass.Draft;
                    else if (item.OutboxId.HasValue)
                        EmailType = StaticKeywordClass.Outbox;
                    else if (item.TrashId.HasValue)
                        EmailType = StaticKeywordClass.Trash;
                    else if (!string.IsNullOrEmpty(item.InboxId))
                        EmailType = StaticKeywordClass.Inbox;

                    System.Xml.XmlNode id = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/id");

                    string from = "";
                    try
                    {
                        System.Xml.XmlNode _from = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/from");
                        from = (string.IsNullOrEmpty(_from.InnerText) ? "" : _from.InnerText);
                    }
                    catch (Exception)
                    { from = ""; }

                    System.Xml.XmlNode to = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/to");
                    System.Xml.XmlNode sub = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/subject");
                    System.Xml.XmlNode body = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/body");
                    System.Xml.XmlNode cc = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/cc");
                    System.Xml.XmlNode bcc = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/bcc");
                    System.Xml.XmlNode date = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/date");
                    System.Xml.XmlNode Attachement = doc.DocumentElement.SelectSingleNode("/" + EmailType + "/Attachement");
                    var tos = "";
                    string[] spltto = to.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] spltcc = cc.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] attach = Attachement.InnerText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < spltto.Length; i++)
                    {
                        if (spltto[i] != "")
                        {
                            if (tos != "")
                                tos += ",";

                            tos += spltto[i];
                        }
                    }

                    for (int i = 0; i < spltcc.Length; i++)
                    {
                        if (spltcc[i] != "")
                        {
                            if (tos != "")
                                tos += ",";

                            tos += spltcc[i];
                        }
                    }


                    EmailData.Add(new MailData
                    {
                        Id = id.InnerText,
                        From = from,
                        To = (string.IsNullOrEmpty(to.InnerText) ? "(no recipient)" : to.InnerText),
                        Subject = (string.IsNullOrEmpty(sub.InnerText) ? "(no subject)" : sub.InnerText),
                        Body = (string.IsNullOrEmpty(body.InnerText) ? "" : body.InnerText),
                        Cc = (string.IsNullOrEmpty(cc.InnerText) ? "" : cc.InnerText),
                        Bcc = (string.IsNullOrEmpty(bcc.InnerText) ? "" : bcc.InnerText),
                        Date = date.InnerText,
                        Attachment_Name = attach,
                        Reply_All_Mails = tos,
                        starid = item.Id
                    });
                }

                if (!search.Equals("blank"))
                {
                    EmailData = EmailData.Where(s => s.To.ToLower().Trim().Contains(search.ToLower().Trim())
                                                || s.Subject.ToLower().Trim().Contains(search.ToLower().Trim())).ToList();
                }

                int count = EmailData.Count();
                var RecordSkip = (skip - 1) * 10;
                EmailData = EmailData.Skip(RecordSkip).Take(10).ToList();

                var PageLinks = count % 10 == 0 ? (count / 10) : (count / 10) + 1;

                List<FinalJsonCollection> fdata = new List<FinalJsonCollection>();

                fdata.Add(new FinalJsonCollection { _MailData = EmailData, TotalPages = PageLinks });

                return new JsonResult(fdata);

            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = ex.Message.ToString(), anyid = 0, anystring = "" };

                return new JsonResult(_ReturnResponse);
            }
        }

        // DELETE MULTIPLE DRAFT MAIL
        [HttpPost("{recordids}")]
        public JsonResult PostDraftBulkDeleteMail(string recordids)
        {
            try
            {
                List<string> splitValues = recordids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                using (EmailToolContext dc = new EmailToolContext())
                {
                    var Records = dc.DraftEmailTable.Where(a => splitValues.Contains(a.Id.ToString())).ToList();

                    foreach (var item in Records)
                    {
                        int UserID = item.UserId;
                        string path = item.Draftpath;

                        TrashMailsTable TMT = new TrashMailsTable();

                        TMT.UserId = UserID;
                        TMT.Xmlpath = path;
                        TMT.EmailType = StaticKeywordClass.Draft;
                        TMT.IsDeleted = false;

                        dc.TrashMailsTable.Add(TMT);
                        dc.SaveChanges();

                        var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Xmlpath);

                        if (System.IO.File.Exists(Epath))
                        {
                            XmlDocument xml = new System.Xml.XmlDocument();
                            xml.Load(Epath);
                            XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/id");
                            eid.InnerText = TMT.Id.ToString();

                            try
                            {
                                xml.Save(Epath);
                            }
                            catch (Exception)
                            { }

                        }
                    }
                }

                //DELETION
                using (EmailToolContext dc = new EmailToolContext())
                {
                    dc.DraftEmailTable.RemoveRange(dc.DraftEmailTable.Where(a => splitValues.Contains(a.Id.ToString())));
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        /* START MULTIPLE DELETE, RESTORE ACTIONS */
        #region

        // DELETE MULTIPLE INBOX MAIL
        [HttpPost]
        public JsonResult PostInboxBulkDeleteMail(IEnumerable<DeleteInboxMail> _DeleteInboxMail)
        {
            try
            {
                using (EmailToolContext db = new EmailToolContext())
                {
                    foreach (var DeleteInboxMail in _DeleteInboxMail)
                    {
                        TrashMailsTable ob = new TrashMailsTable();

                        var unique = DateTime.UtcNow.Ticks;

                        var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Trash_Email\\" + DeleteInboxMail.MasterID);

                        if (!System.IO.Directory.Exists(Epath))
                        {
                            System.IO.Directory.CreateDirectory(Epath);
                        }

                        string path = "Trash_Email/" + DeleteInboxMail.MasterID + "/" + unique + ".xml";

                        ob.InboxMailThreadId = DeleteInboxMail.DeleteID;
                        ob.UserId = DeleteInboxMail.MasterID;
                        ob.EmailType = StaticKeywordClass.Inbox;
                        ob.Xmlpath = path;
                        ob.IsDeleted = false;

                        db.TrashMailsTable.Add(ob);
                        db.SaveChanges();

                        System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + path), System.Text.Encoding.UTF8);

                        writer.WriteStartDocument(true);
                        writer.Formatting = System.Xml.Formatting.Indented;
                        writer.Indentation = 2;
                        writer.WriteStartElement(StaticKeywordClass.Inbox);

                        HelperClass.createNode(writer, "id", ob.Id.ToString());
                        HelperClass.createNode(writer, "from", DeleteInboxMail.From);
                        HelperClass.createNode(writer, "to", DeleteInboxMail.To);
                        HelperClass.createNode(writer, "cc", DeleteInboxMail.Cc);
                        HelperClass.createNode(writer, "bcc", DeleteInboxMail.Bcc);
                        HelperClass.createNode(writer, "subject", DeleteInboxMail.Subject);
                        HelperClass.createNode(writer, "body", DeleteInboxMail.Body);
                        HelperClass.createNode(writer, "Attachement", DeleteInboxMail.Attachment);
                        HelperClass.createNode(writer, "date", DateTime.UtcNow.AddMinutes(330).ToString("MMM-dd-yyyy hh:mm tt"));
                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                        writer.Close();
                    }
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail(s) moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail(s) did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // DELETE MULTIPLE SENT MAIL
        [HttpPost("{recordids}")]
        public JsonResult PostSentBulkDeleteMail(string recordids)
        {
            try
            {
                List<string> splitValues = recordids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                using (EmailToolContext dc = new EmailToolContext())
                {
                    var Records = dc.SendedMail.Where(a => splitValues.Contains(a.Id.ToString())).ToList();

                    foreach (var item in Records)
                    {
                        int UserID = item.UserId;
                        string path = item.SendMailPath;

                        TrashMailsTable TMT = new TrashMailsTable();

                        TMT.UserId = UserID;
                        TMT.Xmlpath = path;
                        TMT.EmailType = StaticKeywordClass.Sent;
                        TMT.IsDeleted = false;

                        dc.TrashMailsTable.Add(TMT);
                        dc.SaveChanges();

                        var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Xmlpath);

                        if (System.IO.File.Exists(Epath))
                        {
                            XmlDocument xml = new System.Xml.XmlDocument();
                            xml.Load(Epath);
                            XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/id");
                            eid.InnerText = TMT.Id.ToString();

                            try
                            {
                                xml.Save(Epath);
                            }
                            catch (Exception)
                            { }

                        }
                    }
                }

                //DELETION
                using (EmailToolContext dc = new EmailToolContext())
                {
                    dc.SendedMail.RemoveRange(dc.SendedMail.Where(a => splitValues.Contains(a.Id.ToString())));
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // DELETE MULTIPLE OUTBOX MAIL
        [HttpPost("{recordids}")]
        public JsonResult PostOutboxBulkDeleteMail(string recordids)
        {
            try
            {
                List<string> splitValues = recordids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                using (EmailToolContext dc = new EmailToolContext())
                {
                    var Records = dc.OutBoxMail.Where(a => splitValues.Contains(a.Id.ToString())).ToList();

                    foreach (var item in Records)
                    {
                        int UserID = item.UserId;
                        string path = item.OutboxMailPath;

                        TrashMailsTable TMT = new TrashMailsTable();

                        TMT.UserId = UserID;
                        TMT.Xmlpath = path;
                        TMT.EmailType = StaticKeywordClass.Outbox;
                        TMT.IsDeleted = false;

                        dc.TrashMailsTable.Add(TMT);
                        dc.SaveChanges();

                        var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Xmlpath);

                        if (System.IO.File.Exists(Epath))
                        {
                            XmlDocument xml = new System.Xml.XmlDocument();
                            xml.Load(Epath);
                            XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/id");
                            eid.InnerText = TMT.Id.ToString();

                            try
                            {
                                xml.Save(Epath);
                            }
                            catch (Exception)
                            { }

                        }
                    }
                }

                //DELETION
                using (EmailToolContext dc = new EmailToolContext())
                {
                    dc.OutBoxMail.RemoveRange(dc.OutBoxMail.Where(a => splitValues.Contains(a.Id.ToString())));
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // DELETE MULTIPLE TRASH MAIL
        [HttpPost("{recordids}")]
        public JsonResult PostTrashBulkDeleteMail(string recordids)
        {
            try
            {
                List<string> splitValues = recordids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                using (EmailToolContext dc = new EmailToolContext())
                {
                    var Records = dc.TrashMailsTable.Where(a => splitValues.Contains(a.Id.ToString())).ToList();

                    foreach (var item in Records)
                    {
                        item.IsDeleted = true;
                    }

                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail deleted successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // RESTORE MULTIPLE TRASH MAIL
        [HttpPost("{recordids}")]
        public JsonResult PostTrashBulkRestoreMail(string recordids)
        {
            try
            {
                List<string> splitValues = recordids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                using (EmailToolContext dc = new EmailToolContext())
                {
                    var Records = dc.TrashMailsTable.Where(a => splitValues.Contains(a.Id.ToString())).ToList();

                    foreach (var TrashTable in Records)
                    {
                        string _EmailType = TrashTable.EmailType;
                        int MasterID = TrashTable.UserId;
                        string path = TrashTable.Xmlpath;

                        if (_EmailType == StaticKeywordClass.Draft)
                        {
                            DraftEmailTable TMT = new DraftEmailTable();

                            TMT.Draftpath = path;
                            TMT.UserId = MasterID;

                            dc.DraftEmailTable.Add(TMT);
                            dc.SaveChanges();

                            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Draftpath);

                            if (System.IO.File.Exists(Epath))
                            {
                                XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(Epath);
                                XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/id");
                                eid.InnerText = TMT.Id.ToString();

                                try
                                {
                                    xml.Save(Epath);
                                }
                                catch (Exception)
                                { }
                            }
                        }
                        else if (_EmailType == StaticKeywordClass.Outbox)
                        {
                            OutBoxMail TMT = new OutBoxMail();

                            TMT.OutboxMailPath = path;
                            TMT.UserId = MasterID;

                            dc.OutBoxMail.Add(TMT);
                            dc.SaveChanges();

                            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.OutboxMailPath);

                            if (System.IO.File.Exists(Epath))
                            {
                                XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(Epath);
                                XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/id");
                                eid.InnerText = TMT.Id.ToString();

                                try
                                {
                                    xml.Save(Epath);
                                }
                                catch (Exception)
                                { }
                            }

                        }
                        else if (_EmailType == StaticKeywordClass.Sent)
                        {

                            SendedMail TMT = new SendedMail();

                            TMT.SendMailPath = path;
                            TMT.UserId = MasterID;

                            dc.SendedMail.Add(TMT);
                            dc.SaveChanges();

                            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.SendMailPath);

                            if (System.IO.File.Exists(Epath))
                            {
                                XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(Epath);
                                XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/id");
                                eid.InnerText = TMT.Id.ToString();

                                try
                                {
                                    xml.Save(Epath);
                                }
                                catch (Exception)
                                { }
                            }
                        }
                    }
                }

                //DELETION
                using (EmailToolContext dc = new EmailToolContext())
                {
                    dc.TrashMailsTable.RemoveRange(dc.TrashMailsTable.Where(a => splitValues.Contains(a.Id.ToString())));
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail restored successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not restore this time due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }
        }
        #endregion

        /* START SINGLE DELETE, RESTORE ACTIONS */
        #region

        // DELETE INBOX MAIL
        [HttpPost]
        public JsonResult PostDeleteInboxMail(DeleteInboxMail DeleteInboxMail)
        {
            try
            {
                EmailToolContext db = new EmailToolContext();

                TrashMailsTable ob = new TrashMailsTable();

                var unique = DateTime.UtcNow.Ticks;

                var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Trash_Email\\" + DeleteInboxMail.MasterID);

                if (!System.IO.Directory.Exists(Epath))
                {
                    System.IO.Directory.CreateDirectory(Epath);
                }

                string path = "Trash_Email/" + DeleteInboxMail.MasterID + "/" + unique + ".xml";

                ob.InboxMailThreadId = DeleteInboxMail.DeleteID;
                ob.UserId = DeleteInboxMail.MasterID;
                ob.EmailType = StaticKeywordClass.Inbox;
                ob.Xmlpath = path;
                ob.IsDeleted = false;

                db.TrashMailsTable.Add(ob);
                db.SaveChanges();

                System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + path), System.Text.Encoding.UTF8);

                writer.WriteStartDocument(true);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement(StaticKeywordClass.Inbox);

                HelperClass.createNode(writer, "id", ob.Id.ToString());
                HelperClass.createNode(writer, "from", DeleteInboxMail.From);
                HelperClass.createNode(writer, "to", DeleteInboxMail.To);
                HelperClass.createNode(writer, "cc", DeleteInboxMail.Cc);
                HelperClass.createNode(writer, "bcc", DeleteInboxMail.Bcc);
                HelperClass.createNode(writer, "subject", DeleteInboxMail.Subject);
                HelperClass.createNode(writer, "body", DeleteInboxMail.Body);
                HelperClass.createNode(writer, "Attachement", DeleteInboxMail.Attachment);
                HelperClass.createNode(writer, "date", DateTime.UtcNow.AddMinutes(330).ToString("MMM-dd-yyyy hh:mm tt"));
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // DELETE SENT MAIL
        [HttpPost]
        public JsonResult PostDeleteSentMail(DeleteMailRelatedFields DeleteMailRelatedFields)
        {
            try
            {
                EmailToolContext dc = new EmailToolContext();

                //////////////
                var SendedMails = dc.SendedMail.FirstOrDefault(a => a.Id == DeleteMailRelatedFields.DeleteID);

                if (SendedMails != null)
                {
                    TrashMailsTable TMT = new TrashMailsTable();

                    TMT.UserId = SendedMails.UserId;
                    TMT.Xmlpath = SendedMails.SendMailPath;
                    TMT.EmailType = StaticKeywordClass.Sent;
                    TMT.IsDeleted = false;

                    dc.TrashMailsTable.Add(TMT);
                    dc.SaveChanges();

                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Xmlpath);

                    if (System.IO.File.Exists(Epath))
                    {
                        XmlDocument xml = new System.Xml.XmlDocument();
                        xml.Load(Epath);
                        XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/id");
                        eid.InnerText = TMT.Id.ToString();

                        try
                        {
                            xml.Save(Epath);
                        }
                        catch (Exception)
                        { }

                    }

                    dc.SendedMail.Remove(SendedMails);
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        [HttpPost]
        public JsonResult PostDeleteDraftMail(DeleteMailRelatedFields DeleteMailRelatedFields)
        {
            try
            {
                EmailToolContext dc = new EmailToolContext();

                //////////////
                var DraftedMails = dc.DraftEmailTable.FirstOrDefault(a => a.Id == DeleteMailRelatedFields.DeleteID);

                if (DraftedMails != null)
                {
                    TrashMailsTable TMT = new TrashMailsTable();

                    TMT.UserId = DraftedMails.UserId;
                    TMT.Xmlpath = DraftedMails.Draftpath;
                    TMT.EmailType = StaticKeywordClass.Draft;
                    TMT.IsDeleted = false;

                    dc.TrashMailsTable.Add(TMT);
                    dc.SaveChanges();

                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Xmlpath);

                    if (System.IO.File.Exists(Epath))
                    {
                        XmlDocument xml = new System.Xml.XmlDocument();
                        xml.Load(Epath);
                        XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/id");
                        eid.InnerText = TMT.Id.ToString();

                        try
                        {
                            xml.Save(Epath);
                        }
                        catch (Exception)
                        { }

                    }

                    dc.DraftEmailTable.Remove(DraftedMails);
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // DELETE TRASH MAILS
        [HttpPost]
        public JsonResult PostDeleteTrashMail(DeleteMailRelatedFields DeleteMailRelatedFields)
        {
            try
            {
                EmailToolContext dc = new EmailToolContext();

                //////////////
                if (DeleteMailRelatedFields.MasterID > 0)
                {
                    var record = (from rec in dc.TrashMailsTable
                                  where rec.Id == DeleteMailRelatedFields.DeleteID
                                  select rec).FirstOrDefault();

                    if (record != null)
                    {
                        record.IsDeleted = true;
                        dc.SaveChanges();
                    }

                }


                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail deleted successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }

        // RESTORE TRASH MAILS
        [HttpPost]
        public JsonResult PostRestoreTrashMail(DeleteMailRelatedFields DeleteMailRelatedFields)
        {
            try
            {

                int MasterID = DeleteMailRelatedFields.MasterID;

                using (EmailToolContext dc = new EmailToolContext())
                {

                    var TrashTable = dc.TrashMailsTable.FirstOrDefault(a => a.Id == DeleteMailRelatedFields.DeleteID);

                    if (TrashTable != null)
                    {
                        if (TrashTable.EmailType == StaticKeywordClass.Draft)
                        {
                            DraftEmailTable TMT = new DraftEmailTable();

                            TMT.Draftpath = TrashTable.Xmlpath;
                            TMT.UserId = MasterID;

                            dc.DraftEmailTable.Add(TMT);
                            dc.SaveChanges();

                            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Draftpath);

                            if (System.IO.File.Exists(Epath))
                            {
                                XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(Epath);
                                XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Draft + "/id");
                                eid.InnerText = TMT.Id.ToString();

                                try
                                {
                                    xml.Save(Epath);
                                }
                                catch (Exception)
                                { }
                            }
                        }
                        else if (TrashTable.EmailType == StaticKeywordClass.Outbox)
                        {
                            OutBoxMail TMT = new OutBoxMail();

                            TMT.OutboxMailPath = TrashTable.Xmlpath;
                            TMT.UserId = MasterID;

                            dc.OutBoxMail.Add(TMT);
                            dc.SaveChanges();

                            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.OutboxMailPath);

                            if (System.IO.File.Exists(Epath))
                            {
                                XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(Epath);
                                XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/id");
                                eid.InnerText = TMT.Id.ToString();

                                try
                                {
                                    xml.Save(Epath);
                                }
                                catch (Exception)
                                { }
                            }

                        }
                        else if (TrashTable.EmailType == StaticKeywordClass.Sent)
                        {

                            SendedMail TMT = new SendedMail();

                            TMT.SendMailPath = TrashTable.Xmlpath;
                            TMT.UserId = MasterID;

                            dc.SendedMail.Add(TMT);
                            dc.SaveChanges();

                            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.SendMailPath);

                            if (System.IO.File.Exists(Epath))
                            {
                                XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(Epath);
                                XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Sent + "/id");
                                eid.InnerText = TMT.Id.ToString();

                                try
                                {
                                    xml.Save(Epath);
                                }
                                catch (Exception)
                                { }
                            }
                        }

                        dc.TrashMailsTable.Remove(TrashTable);
                        dc.SaveChanges();
                    }

                    ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail restored successfully" };

                    return new JsonResult(_ReturnResponse);
                }
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not restore this time due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }
        }

        [HttpPost]
        public JsonResult PostDeleteOutboxMail(DeleteMailRelatedFields DeleteMailRelatedFields)
        {
            try
            {
                EmailToolContext dc = new EmailToolContext();

                //////////////
                var OutboxMails = dc.OutBoxMail.FirstOrDefault(a => a.Id == DeleteMailRelatedFields.DeleteID);

                if (OutboxMails != null)
                {
                    TrashMailsTable TMT = new TrashMailsTable();

                    TMT.UserId = OutboxMails.UserId;
                    TMT.Xmlpath = OutboxMails.OutboxMailPath;
                    TMT.EmailType = StaticKeywordClass.Outbox;
                    TMT.IsDeleted = false;

                    dc.TrashMailsTable.Add(TMT);
                    dc.SaveChanges();

                    var Epath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\" + TMT.Xmlpath);

                    if (System.IO.File.Exists(Epath))
                    {
                        XmlDocument xml = new System.Xml.XmlDocument();
                        xml.Load(Epath);
                        XmlNode eid = xml.DocumentElement.SelectSingleNode("/" + StaticKeywordClass.Outbox + "/id");
                        eid.InnerText = TMT.Id.ToString();

                        try
                        {
                            xml.Save(Epath);
                        }
                        catch (Exception)
                        { }

                    }

                    dc.OutBoxMail.Remove(OutboxMails);
                    dc.SaveChanges();
                }

                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "Mail moved into trash successfully" };

                return new JsonResult(_ReturnResponse);
            }
            catch (Exception ex)
            {
                ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "error", msg = "Mail did not delete successfully due to " + ex.Message.ToString() };

                return new JsonResult(_ReturnResponse);
            }

        }
        #endregion
        //  SIGNATURE FILE UPLOAD
        [HttpPost]
        public ActionResult SignatureFileUpload()
        {
            long size = 0;
            var files = Request.Form.Files;

            string NewlyCreatedFileName = "";
            var Epath = Path.Combine(Directory.GetCurrentDirectory(), "SignatureAttacements");

            foreach (var file in files)
            {
                //var filename = ContentDispositionHeaderValue
                //                .Parse(file.ContentDisposition)
                //                .FileName
                //                .Trim('"');



                if (!System.IO.Directory.Exists(Epath))
                {
                    System.IO.Directory.CreateDirectory(Epath);
                }

                string filename = DateTime.UtcNow.Ticks.ToString();
                string fname = file.FileName;
                string ext = Path.GetExtension(fname);

                NewlyCreatedFileName = filename + ext;

                filename = Epath + $@"\" + NewlyCreatedFileName;
                size += file.Length;
                using (FileStream fs = System.IO.File.Create(filename))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
            }

            ReturnResponseClass _ReturnResponse = new ReturnResponseClass() { type = "success", msg = "", anystring = NewlyCreatedFileName };

            return new JsonResult(_ReturnResponse);
        }


        //[HttpGet("{masterid}")]
        //public JsonResult GetSignatureDetails( int masterid)
        //{

        //    try
        //    {
        //        EmailToolContext dc = new EmailToolContext();

        //        var record = (from rec in dc.EmailConfig
        //                      where rec.Id == masterid
        //                      select rec).FirstOrDefault();

        //    }
        //    catch(Exception ex)
        //    {

        //    }



        //}
    }
}
